// 以下是业务服务器API地址

 // 本机开发API地址
// var WxApiRoot = 'http://localhost:8070/wx/';
// 测试环境部署api地址

// var WxApiRoot = 'http://192.168.0.57:6080/demo/wx/';
// var WxApiRoot_1='http://192.168.0.57:8383/reservation/';

// var WxApiRoot = 'http://192.168.0.103:6080/demo/wx/';
// var WxApiRoot_1='http://192.168.0.103:8383/reservation/';

// var WxApiRoot = 'http://192.168.0.243:6080/demo/wx/';
// var WxApiRoot_1='http://192.168.0.243:8383/reservation/';

var WxApiRoot = 'https://jczh.jiachang8.com/jczh/wx/wx/';
var WxApiRoot_1='https://jczh.jiachang8.com/reservation/';

// 线上云平台api地址
// var WxApiRoot = 'https://www.dtsshop.com/wx/';

module.exports = {
  IndexUrl: WxApiRoot + 'home/index', //首页数据接口
  CatalogList: WxApiRoot + 'catalog/index', //分类目录全部分类数据接口
  CatalogCurrent: WxApiRoot + 'catalog/current', //分类目录当前分类数据接口

  AuthLoginByWeixin: WxApiRoot + 'auth/login_by_weixin', //微信登录
  AuthLoginByAccount: WxApiRoot + 'auth/login', //账号登录
  AuthLogout: WxApiRoot + 'auth/logout', //账号登出
  AuthRegister: WxApiRoot + 'auth/register', //账号注册
  AuthReset: WxApiRoot + 'auth/reset', //账号密码重置
  AuthRegisterCaptcha: WxApiRoot + 'auth/regCaptcha', //验证码
  AuthBindPhone: WxApiRoot + 'auth/bindPhone', //绑定微信手机号

  GoodsCount: WxApiRoot + 'goods/count', //统计商品总数
  GoodsList: WxApiRoot + 'goods/list', //获得商品列表
  GoodsCategory: WxApiRoot + 'goods/category', //获得分类数据
  GoodsDetail: WxApiRoot + 'goods/detail', //获得商品的详情
  GoodsRelated: WxApiRoot + 'goods/related', //商品详情页的关联商品（大家都在看）

  BrandList: WxApiRoot + 'brand/list', //品牌列表
  BrandDetail: WxApiRoot + 'brand/detail', //品牌详情

  CartList: WxApiRoot + 'cart/index', //获取购物车的数据
  CartAdd: WxApiRoot + 'cart/add', // 添加商品到购物车
  CartFastAdd: WxApiRoot + 'cart/fastadd', // 立即购买商品
  CartUpdate: WxApiRoot + 'cart/update', // 更新购物车的商品
  CartDelete: WxApiRoot + 'cart/delete', // 删除购物车的商品
  CartChecked: WxApiRoot + 'cart/checked', // 选择或取消选择商品
  CartGoodsCount: WxApiRoot + 'cart/goodscount', // 获取购物车商品件数
  CartCheckout: WxApiRoot + 'cart/checkout', // 下单前信息确认

  CollectList: WxApiRoot + 'collect/list', //收藏列表
  CollectAddOrDelete: WxApiRoot + 'collect/addordelete', //添加或取消收藏

  CommentList: WxApiRoot + 'comment/list', //评论列表
  CommentCount: WxApiRoot + 'comment/count', //评论总数
  CommentPost: WxApiRoot + 'comment/post', //发表评论

  TopicList: WxApiRoot + 'topic/list', //专题列表
  TopicDetail: WxApiRoot + 'topic/detail', //专题详情
  TopicRelated: WxApiRoot + 'topic/related', //相关专题

  SearchIndex: WxApiRoot + 'search/index', //搜索关键字
  SearchResult: WxApiRoot + 'search/result', //搜索结果
  SearchHelper: WxApiRoot + 'search/helper', //搜索帮助
  SearchClearHistory: WxApiRoot + 'search/clearhistory', //搜索历史清楚

  AddressList: WxApiRoot + 'address/list', //收货地址列表
  AddressDetail: WxApiRoot + 'address/detail', //收货地址详情
  AddressSave: WxApiRoot + 'address/save', //保存收货地址
  AddressDelete: WxApiRoot + 'address/delete', //保存收货地址

  ExpressQuery: WxApiRoot + 'express/query', //物流查询
  RegionList: WxApiRoot + 'region/list', //获取区域列表

  OrderSubmit: WxApiRoot + 'order/submit', // 提交订单
  OrderPrepay: WxApiRoot + 'order/prepay', // 订单的预支付会话
  OrderList: WxApiRoot + 'order/list', //订单列表
  OrderDetail: WxApiRoot + 'order/detail', //订单详情
  ExpressTrace: WxApiRoot + 'order/expressTrace', //订单物流
  OrderCancel: WxApiRoot + 'order/cancel', //取消订单
  OrderRefund: WxApiRoot + 'order/refund', //退款取消订单
  OrderDelete: WxApiRoot + 'order/delete', //删除订单
  OrderConfirm: WxApiRoot + 'order/confirm', //确认收货
  OrderGoods: WxApiRoot + 'order/goods', // 代评价商品信息
  OrderComment: WxApiRoot + 'order/comment', // 评价订单商品信息

  FeedbackAdd: WxApiRoot + 'feedback/submit', //添加反馈
  FootprintList: WxApiRoot + 'footprint/list', //足迹列表
  FootprintDelete: WxApiRoot + 'footprint/delete', //删除足迹

  UserFormIdCreate: WxApiRoot + 'formid/create', //用户FromId，用于发送模版消息

  GroupOnList: WxApiRoot + 'groupon/list', //团购列表
  GroupOn: WxApiRoot + 'groupon/query', //团购API-查询
  GroupOnMy: WxApiRoot + 'groupon/my', //团购API-我的团购
  GroupOnDetail: WxApiRoot + 'groupon/detail', //团购API-详情
  GroupOnJoin: WxApiRoot + 'groupon/join', //团购API-详情

  CouponList: WxApiRoot + 'coupon/list', //优惠券列表
  CouponMyList: WxApiRoot + 'coupon/mylist', //我的优惠券列表
  CouponSelectList: WxApiRoot + 'coupon/selectlist', //当前订单可用优惠券列表
  CouponReceive: WxApiRoot + 'coupon/receive', //优惠券领取
  CouponReceiveAll: WxApiRoot + 'coupon/receiveAll', //优惠券领取
  CouponExchange: WxApiRoot + 'coupon/exchange', //优惠券兑换
  GetUserCoupon: WxApiRoot + 'coupon/getUserCoupon',//用户个人可领取优惠券查询

  StorageUpload: WxApiRoot + 'storage/upload', //图片上传,

  UserIndex: WxApiRoot + 'user/index', //个人页面用户相关信息
  BrokerageMain: WxApiRoot + 'brokerage/main',//佣金收益主页面
  SettleOrderList: WxApiRoot + 'brokerage/settleOrderList',//佣金收益主页面
  ExtractList: WxApiRoot + 'brokerage/extractList',//佣金账号提现记录
  ArticleDetail: WxApiRoot + 'article/detail',//公告详情
  ApplyAgency: WxApiRoot + 'user/applyAgency',//代理申请
  GetSharedUrl: WxApiRoot + 'user/getSharedUrl', //获取推广二维码

  pasVerification:WxApiRoot+'order/pasVerification',//验证商品房券支付密码
  checkBalances:WxApiRoot+'user/checkBalances',//查房
  hotelbanner:WxApiRoot +'home/ad',//订房页面轮播
  place:WxApiRoot +'chargeOrder/place',//下单
  prepay:WxApiRoot +'chargeOrder/prepay',//拉起支付
  chargeOrder:WxApiRoot+'chargeOrder/list',
  jichupackage:WxApiRoot_1+'api/package/selectByAll',//查询基础套餐
  hotelContract:WxApiRoot_1+'api/hotelContract/save',//酒店合同
  c_hotelContract:WxApiRoot_1+'api/hotelContract/selectByIdAndAll',//合同查询
  audit:WxApiRoot_1+'api/hotelContract/audit',//合同审核
  c_delete:WxApiRoot_1+'api/hotelContract/delete',//合同删除
  partnerContract:WxApiRoot_1+'api/partnerContract/save',//合伙人合同
  partner_selectByIdAndAll:WxApiRoot_1+'api/partnerContract/selectByIdAndAll',//合伙人合同查询
  partner_audit:WxApiRoot_1+'api/partnerContract/audit',//合伙人合同审批
  partner_delete:WxApiRoot_1+'api/partnerContract/delete',//合伙人合同删除
  supplierContract:WxApiRoot_1+'api/supplierContract/save',//供应商合同
  supplier_selectByIdAndAll:WxApiRoot_1+'api/supplierContract/selectByIdAndAll',//供应商合同查询
  supplier_audit:WxApiRoot_1+'api/supplierContract/audit',//供应审批
  supplier_delete:WxApiRoot_1+'api/supplierContract/delete',//供应商删除  

  avatarUpload: WxApiRoot_1 + 'api/upload/image', //头像上传,
  companyList:WxApiRoot+'user/company',//公司列表
  info_list:WxApiRoot_1+'api/user/info/list',//订房用户信息

  userInfoAdd:WxApiRoot_1+'api/user/info/add', //新增用户信息
  userInfoList:WxApiRoot_1+'api/user/info/list', // 查询用户信息
  userInfoDelete:WxApiRoot_1+'api/user/info/delete', // 查询用户信息
  userInforDetail:WxApiRoot_1+'api/user/info/detail', // 根据id查询用户信息
  userInforUpdate:WxApiRoot_1+'api/user/info/update', // 修改用户信息

  hotel_selectById:WxApiRoot_1+'api/hotel/selectById',//通过酒店ID查询酒店信息

  recommended:WxApiRoot+'user/recommended',//我的团队
  selectByPartner:WxApiRoot+'user/selectByPartner',//查询合伙人等级

  article_list:WxApiRoot+'article/list',//家畅学院
  hotel_selectByMap:WxApiRoot_1+ 'api/hotel/selectByMap',

  goodsInStock: WxApiRoot+'goods/inStock',// goods页面 校验该商品是否有货
  accountBalanceSheet:WxApiRoot+ 'accountBalanceSheet/list',//平台流水
  changePassword:WxApiRoot+  'user/changePassword',//修改登录密码
  statistics:WxApiRoot_1+ 'api/contract/statistics',//合伙人业绩
  evaluate: WxApiRoot_1  + 'api/comment/save', //评论

  home_reservation: WxApiRoot+'home/reservation', //topic顶图
  roomType_query:WxApiRoot_1+ 'api/roomType/query',//查询房型
  
  estimateAll:WxApiRoot_1+'api/comment/selectByMap',//酒店评价
  addressChange:WxApiRoot + 'address/change',//修改收获地址
  whetherToSet:WxApiRoot_1+'api/order/whetherToSet',//判断是否存在支付密码

  Partnerdelete:WxApiRoot +'userLevel/delete',

  selectByMap:WxApiRoot+'user/selectByMap',//查询合伙人按等级

  leaderboard:WxApiRoot_1+'api/leaderboard/like',//排行榜酒店点赞
  leaderboard_spread:WxApiRoot_1+'api/leaderboard/spread',//排行榜酒店点赞

  supplierContract_cx: WxApiRoot_1+'api/supplierContract/selectByMap',//供应商合同搜索
  hotelContract_cx: WxApiRoot_1+'api/hotelContract/selectByMap',//酒店合同搜索
  partnerContract_cx: WxApiRoot_1+'api/partnerContract/selectByMap',//合伙人合同搜索
 
  amount_cx:WxApiRoot+ 'amount/list',//员工查询
  frist_day:WxApiRoot_1+'api/order/unitPriceQuery',//查预定第一天房间价格

  hotelMap:WxApiRoot_1+'api/hotel/hotelMap',//地图查询
  hotelsuggestion:WxApiRoot_1 +'api/hotel/suggestion'//查pio
};