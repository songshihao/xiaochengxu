Component({
  data: {
    selected: 0,
    color: "#7A7E83",
    selectedColor: "#3cc51f",
    list: [
      {
        "pagePath": "pages/index/index",
        "iconPath": "https://jczh.jiachang8.com/images/ic_menu_choice_nor.png",
        "selectedIconPath": "https://jczh.jiachang8.com/images/ic_menu_choice_pressed.png",
        "text": "首页"
      },
      {
        "pagePath": "pages/catalog/catalog",
        "iconPath": "https://jczh.jiachang8.com/images/ic_menu_sort_nor.png",
        "selectedIconPath": "https://jczh.jiachang8.com/images/ic_menu_sort_pressed.png",
        "text": "分类"
      },
      {
        "pagePath": "pages/topic/topic",
        "iconPath": "https://jczh.jiachang8.com/images/ic_menu_topic_nor.png",
        "selectedIconPath": "https://jczh.jiachang8.com/images/ic_menu_topic_pressed.png",
        "text": "订房"
      },
      {
        "pagePath": "pages/cart/cart",
        "iconPath": "https://jczh.jiachang8.com/images/ic_menu_shoping_nor.png",
        "selectedIconPath": "https://jczh.jiachang8.com/images/ic_menu_shoping_pressed.png",
        "text": "购物车"
      },
      {
        "pagePath": "pages/dts_index/index",
        "iconPath": "https://jczh.jiachang8.com/images/ic_menu_me_nor.png",
        "selectedIconPath": "https://jczh.jiachang8.com/images/ic_menu_me_pressed.png",
        "text": "我的"
      }
    ]
  },
  attached() {
  },
  methods: {
    switchTab(e) {
      const data = e.currentTarget.dataset
      const url = data.path
      wx.switchTab({url})
      this.setData({
        selected: data.index
      })
    }
  }
})