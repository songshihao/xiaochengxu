// pages/hotelorder/order/order.js
var util = require('../../../utils/back.js');      //引用外部的js文件
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderFormId:'',
    check_in_time:'',
    departure_time:'',
    price:'',
    hotelName:'',
    usename:'',
    roomnum:'',
    amountPayable:''

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    this.setData({
      orderFormId:options.orderFormId,
      check_in_time:options.check_in_time,
      departure_time:options.departure_time,
      // price:options.price,
      hotelName:options.hotelName,
      usename:options.useName,
      roomnum:options.count,
      amountPayable:options.amountPayable
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  wxzf:function(){
    // wx.requestPayment({
    //   timeStamp: '',
    //   nonceStr: '',
    //   package: '',
    //   signType: 'MD5',
    //   paySign: '',
    //   success (res) { 
    //     console.log(res)
    //   },
    //   fail (res) { 
    //     console.log(res)
    //   }
    // })
    wx.request({
      url: app.globalData.url83 +'api/order/wxPay',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data:{
        order_code:this.data.orderFormId
      },
      success:function(res){
        console.log(res)
        console.log( res.data.payJson.timeStamp)
         wx.requestPayment({
            timeStamp: res.data.payJson.timeStamp,
            nonceStr:  res.data.payJson.nonceStr,
            package:  res.data.payJson.Package,
            signType: 'MD5',
            paySign: res.data.payJson.paySign,
            success (res) { 
              console.log(res)
              console.log(res.errMsg)
              console.log(res.errMsg.requestPayment)
              if(res.errMsg=='requestPayment:ok'){
                wx.navigateTo({
                  url: '/pages/hotelorder/order/zfsuccess/zfsuccess',
                })
              }else  if(res.errMsg!='requestPayment:ok'){
                wx.navigateTo({
                  url: '/pages/hotelorder/cancellation/cancellation',
                })
              }
            },
            fail (res) { 
              console.log(res)
            }
          })
      },
      fail (res) {
        console.log(res)
                
      }

    })
   
  },
  qxdd:function(){
    var orderFormId=this.data.orderFormId
    wx.showModal({
       title: '提示',
        content: '您确定要取消吗？',
        showCancel: true, //是否显示取消按钮-----》false去掉取消按钮
        cancelText: "否", //默认是“取消”
        cancelColor: 'skyblue', //取消文字的颜色
        confirmText: "是", //默认是“确定”
        confirmColor: 'skyblue', //确定文字的颜色
        success: function(res) {
          console.log(res)
             if (res.cancel) {
               //点击取消
                console.log("您点击了取消")
                wx.showToast({
                  title: '您的订单还在',
                  duration:1500
                })
              } else if(res.confirm){
                //点击确定
      //             wx.showToast({
      //               title: '您已经取消了订单',
      //               duration:1500
      //           })
                wx.request({
                  url:app.globalData.url83 + 'api/order/order_deletion',
                  method: "POST",
                  header: {
                    'content-type': "application/x-www-form-urlencoded",
                    'X-Nideshop-Token': wx.getStorageSync('token')
                  },
                  data:{
                    order_code:orderFormId
                  },
                  success:function(res){
                    
                  },
                  fail (res) {
                    console.log(res)
                            
                  }
            
                })
                setTimeout(function(){
                  wx.navigateTo({
                    url: '/pages/hotelorder/cancellation/cancellation',
                  })
                },1000)
               
                }
              }
            })
  },
  onBackTap: function(e) {
    // wx.redirectTo({
    //   url: '../photo',
    // })

    // var pages = getCurrentPages();                       //获取当前页面
    // var prePage = pages[pages.length - 2];               //获取上一页面
    // prePage.setData({
    //   'search.page': 1                                   //给上一页面的变量赋值
    // })
    // wx.navigateBack({                                    //返回上一页面
    //   delta: 1,
    // })
    util.onBackTap()          //调用

  },
  
  chongzhi:function(){
    wx.navigateTo({
      url: '/pages/ucenter/chongzhi/chongzhi?arguments='+'zfzf',
    })
  }
})