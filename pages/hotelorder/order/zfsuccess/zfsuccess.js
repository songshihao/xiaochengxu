// pages/hotelorder/order/order.js
var back = require('../../../../utils/back');      //引用外部的js文件
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hdatahieht:'',
    hdatatop:'',
    hotel_telephone:'',
    dianhua:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
   
    var that=this
    var hdatahieht=wx.getStorageSync('hdatahieht')
    var hdatatop=wx.getStorageSync('hdatatop')
      // console.log(hdatahieht)
      // console.log(hdatatop)
      that.setData({
        hdatahieht:hdatahieht,
        hdatatop:hdatatop,
        hotel_telephone:options.hotel_telephone
      });
  },
  jc_dianhua:function(){
    this.setData({
      dianhua:'4007569888'
    })
    this.open();
  },
  hotel_dianhua:function(){
    this.setData({
      dianhua:this.data.hotel_telephone
    })
    this.open();
  },

  //拨号
  open: function () {
        wx.makePhoneCall({
          phoneNumber:this.data.dianhua, 
          success: function () {
            console.log("拨打电话成功！")
          },
          fail: function () {
            console.log("拨打电话失败！")
          }
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  ljck:function(){
    wx.navigateTo({
      url: '/pages/hotelorder/orderlist/orderlist',
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
 
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  wxzf:function(){
    wx.requestPayment({
      timeStamp: '',
      nonceStr: '',
      package: '',
      signType: 'MD5',
      paySign: '',
      success (res) { 
        console.log(res)
      },
      fail (res) { 
        console.log(res)
      }
    })
    wx.request({
      url: 'url',
      header:{},
      data:{
      },
      success:function(res){
         wx.requestPayment({
            timeStamp: '',
            nonceStr: '',
            package: '',
            signType: 'MD5',
            paySign: '',
            success (res) { 
              
            },
            fail (res) { 
              console.log(res)
            }
          })
      },
      fail (res) {
        console.log(res)               
      }
    })
  },
  onBackTap: function(e) {
    back.onBackTap();
  },
  onBackHome:function(){
    wx.switchTab({
      url: '/pages/topic/topic',
    })
  }
})