// 上传组件 基于https://github.com/Tencent/weui-wxss/tree/master/src/example/uploader
var app = getApp();
var util = require('../../../../utils/util.js');
var api = require('../../../../config/api.js');
Page({
  data: {
    code: '',
    order:{},
    type: 0,
    valueId: 0,
    orderGoods: {},
    content: '',
    stars: [0, 1, 2, 3, 4],
    star: 5,
    starText: '十分满意',
    hasPicture: false,
    picUrls: [],
    files: []
  },
  chooseImage: function(e) {
    if (this.data.files.length >= 5) {
      util.showErrorToast('只能上传五张图片')
      return false;
    }

    var that = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {
        that.setData({
          files: that.data.files.concat(res.tempFilePaths)
        });
        that.upload(res);
      }
    })
  },
  upload: function(res) {
    var that = this;
    const uploadTask = wx.uploadFile({
      url: api.StorageUpload,
      filePath: res.tempFilePaths[0],
      name: 'file',
      success: function(res) {
        console.log(res)
        var _res = JSON.parse(res.data);
        if (_res.errno === 0) {
          var url = _res.data.url
          var picUrls=that.data.picUrls
          picUrls.push(url)
          console.log(picUrls)
          console.log(url)
          // that.data.picUrls.push(url)
          that.setData({
            hasPicture: true,
            picUrls:picUrls
            // picUrls: that.data.picUrls
          })
        }
      },
      fail: function(e) {
        wx.showModal({
          title: '错误',
          content: '上传失败',
          showCancel: false
        })
      },
    })

    uploadTask.onProgressUpdate((res) => {
      console.log('上传进度', res.progress)
      console.log('已经上传的数据长度', res.totalBytesSent)
      console.log('预期需要上传的数据总长度', res.totalBytesExpectedToSend)
    })

  },
  previewImage: function(e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: this.data.files // 需要预览的图片http链接列表
    })
  },
  selectRater: function(e) {
    var star = e.currentTarget.dataset.star + 1;
    var starText;
    if (star == 1) {
      starText = '很差';
    } else if (star == 2) {
      starText = '不太满意';
    } else if (star == 3) {
      starText = '满意';
    } else if (star == 4) {
      starText = '比较满意';
    } else {
      starText = '十分满意'
    }
    this.setData({
      star: star,
      starText: starText
    })

  },
  onLoad: function(options) {
    console.log(options)
    var that = this;
    that.setData({
      code: options.code,
    });
    this.getOrderGoods();
  },
  getOrderGoods: function() {
    let that = this;
    wx.request({
      url: app.globalData.url83 +'api/order/selectByBeanBefore',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data:{
        page: 1,
        pageSize: 1,
        status:''	,
        order_code:that.data.code

      },
      success:function(res){
        console.log(res)
        if(res.data.code==0){
          that.setData({
           order:res.data.data.list[0]
          })
        }
      }
    })
  },
  onClose: function() {
    wx.navigateBack();
  },
  onPost: function() {
    let that = this;

    if (!this.data.content) {
      util.showErrorToast('请填写评论')
      return false;
    }
    console.log(that.data.picUrls)
    var picUrls=JSON.stringify(that.data.picUrls)
    wx.request({
      url: api.evaluate,
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data:{
        orderCode: that.data.code,
        content: that.data.content,
        star: that.data.star,
        picUrls: picUrls
      },
      success:function(res){
        console.log(res)
        if(res.data.errno==404){
          wx.showToast({
            title: res.data.errmsg,
            icon:"none"
          })
        }else if(res.data.code==0){
               wx.showToast({
                  title: '评论成功',
                  complete: function() {
                    setTimeout(function(){
                      wx.switchTab({
                        url: '/pages/dts_index/index'
                      })
                    },500)
                   
                  }
                })
        }else{
          wx.showToast({
            title: '评论异常，请联系管理员',
            complete: function() {
              wx.switchTab({
                url: '/pages/dts_index/index'
              })
            }
          })
        }
   
      }
    })

  },
  bindInputValue(event) {

    let value = event.detail.value;

    //判断是否超过140个字符
    if (value && value.length > 140) {
      return false;
    }

    this.setData({
      content: event.detail.value,
    })
  },
  onReady: function() {

  },
  onShow: function() {
    // 页面显示

  },
  onHide: function() {
    // 页面隐藏

  },
  onUnload: function() {
    // 页面关闭

  }
})