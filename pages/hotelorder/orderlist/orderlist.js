var util = require('../../../utils/util');
var api = require('../../../config/api.js');
var util = require('../../../utils/back.js');      //引用外部的js文件

var app = getApp();
// import cfg from '../../utils/config.js';
// import util from '../../utils/util.js';

Page({
  /**
   * 页面的初始数据
   */
  data: {
    navTab: [
     {id:0,name:'全部'},
    //  {id:1,name:'未付款'},
     {id:1,name:'未确认'},
     {id:2,name:'已确认'},
     {id:3,name:'已完成'},
    //  {id:5,name:'已取消'},
    ],        
    currentTab: 0,
    sendList:[],
    idx:0,
    page:1,
    token:'',
    order_code:'',
    hdatahieht:'',
    hdatatop:'',
    order_status:''
  },
  select: {
    page: 1,
    size: 6,
    isEnd: false,
  
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
    var token = wx.getStorageSync('token');
    if (token == '') {
      wx.navigateTo({
        url: 'pages/auth/login/login',
      })
    }
    wx.getStorage({
      key: 'token',
      success:function(res) {
        // console.log(res.data)
        that.setData({
          token:res.data
        })
      },
      fail:function(res){
        // console.log(res)
        if(res.errMsg=="getStorage:fail data not found"){
             wx.navigateTo({
                url: 'pages/auth/login/login',
              })
        }
      }
    })
    var hdatahieht=wx.getStorageSync('hdatahieht')
    var hdatatop=wx.getStorageSync('hdatatop')
      that.setData({
        hdatahieht:hdatahieht,
        hdatatop:hdatatop
      });
    that.getData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onLoad();
    setTimeout(function(){
      wx.stopPullDownRefresh({
        complete: (res) => {
          wx.showToast({
            title: '刷新成功',
            icon:'none',
          })
        },
      })
    },200)
  },
  dianhua:function(e){
    console.log(e.currentTarget.dataset.dianhua)
    this.setData({
      dianhua:e.currentTarget.dataset.dianhua
    })
    this.open();
  },
  //拨号
  open: function () {
    wx.makePhoneCall({
      phoneNumber:this.data.dianhua, 
      success: function () {
        console.log("拨打电话成功！")
      },
      fail: function () {
        console.log("拨打电话失败！")
      }
});
},
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  currentTab: function (e) {
    if (this.data.currentTab == e.currentTarget.dataset.idx){
      return;
    }
    this.setData({
      currentTab: e.currentTarget.dataset.idx
    })
    this.getData();
  },
  getData:function(){
    var _this=this;
    if(_this.data.currentTab!='0'){
      wx.request({
        url: app.globalData.url83 +'api/order/selectByBeanBefore',
        // url:'http://192.168.0.28:8383/reservation/api/order/selectByBeanBefore',
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
        data:{
          page: 1,
          pageSize: 20,
          status:_this.data.currentTab
          // payment_status :_this.data.currentTab,
          // hotel_id:wx.getStorageSync('hotelId')	
        },
        success:function(res){
          if(res.errmsg=='token失效，请重新登录'){
            wx.showModal({
              title: 'token失效，请重新登录',
              content: '点击确定重新登录',
              success (res) {
              if (res.confirm) {
              wx.navigateTo({
                url: '/pages/auth/login/login',
              })
              } else if (res.cancel) {
              console.log('用户点击取消')
              } 
              } 
            })
          }
          _this.setData({
              sendList:res.data.data.list
            })
        },
        fail:function(res){
  
  
      },
      })
    }else if(_this.data.currentTab=='0'){
      wx.request({
        url: app.globalData.url83 +'api/order/selectByBeanBefore',
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
        data:{
          page:1,
          pageSize: 20,
          status:''	
        },
        success:function(res){
          console.log(res)
          if(res.errmsg=='token失效，请重新登录'){
            wx.showModal({
              title: 'token失效，请重新登录',
              content: '点击确定重新登录',
              success (res) {
              if (res.confirm) {
              wx.navigateTo({
                url: '/pages/auth/login/login',
              })
              } else if (res.cancel) {
              console.log('用户点击取消')
              } 
              } 
            })
          }
          _this.setData({
              sendList:res.data.data.list
            })
        },
        fail:function(res){
  
  
      },
      })
    }
  
  },
  //预定结果消息推送
  subscribeMessage:function(e){
    let that=this
    wx.requestSubscribeMessage({
      "tmplIds":['MWw9LsytviIITAXnu8mBa6p6KYZL9cVZI4Tz7hKuasY'],
      success (res) {
        var status=e.currentTarget.dataset.status
        that.setData({
          order_code: e.currentTarget.dataset.code,
          order_status:e.currentTarget.dataset.status
        })
        if(status!='已完成'){
          wx.showModal({
            title: '提示',
             content: '您确定要取消吗？',
             success: function(res) {
              //  console.log(res)
                  if (res.cancel) {
                    //点击取消
                     wx.showToast({
                       title: '您的订单还在',
                       duration:1500
                     })
                   } else if(res.confirm){
                     //点击确定
                  that.tuikuan();                
                   }
                   }
                 }) 
        }
       }
    })
  },
  order_Refund:function(e){
    var that=this
    var status=e.currentTarget.dataset.status
    that.setData({
      order_code: e.currentTarget.dataset.code,
      order_status:e.currentTarget.dataset.status
    })
    if(status!='已完成'){
      wx.showModal({
        title: '提示',
         content: '您确定要取消吗？',
         success: function(res) {
          //  console.log(res)
              if (res.cancel) {
                //点击取消
                 wx.showToast({
                   title: '您的订单还在',
                   duration:1500
                 })
               } else if(res.confirm){
                 //点击确定
              that.tuikuan();                
               }
               }
             }) 
    }
  },
  tuikuan:function(){
    var that=this
    var order_status=that.data.order_status
    var url=''
    if(order_status=='已确认'){
      url='api/refund/applicationRefund'
    }else {
      url= 'api/order/order_cancel'
    }
    wx.request({
      url: app.globalData.url83 + url,
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data:{
        order_code:this.data.order_code
      },
      success:function(res){
        console.log(res)
        // that.onLoad();
        if(res.data.code==500){
          util.showErrorToast(res.data.msg)
        }
        setTimeout(function(){
          wx.navigateTo({
            url: '/pages/hotelorder/cancellation/cancellation',
          })
        },1000)
      },
      fail:function(res){


    },
    })
  },
  replace:function(e){
    var that=this
    wx.request({
      url: app.globalData.url83 +'api/refund/cancelRefund',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data:{
        order_code:e.currentTarget.dataset.code
        
      },
      success:function(res){
        console.log(res)
        that.onLoad();
      },
      fail:function(res){


    },
    })
  },
  onBackTap: function(e) {
    // util.onBackTap()          //调用
    wx.switchTab({
      url: '/pages/dts_index/index',
    })
  },
  
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var _this = this;
    // console.log(that)
    var list_ALL=_this.data.sendList
    var page = _this.data.page
    page++;
    _this.setData({
      page:page
    })
    // 显示加载图标
    wx.showLoading({
    title: '玩命加载中',
    })
    if(_this.data.currentTab!='0'){
      wx.request({
        url: app.globalData.url83 +'api/order/selectByBeanBefore',
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
        data:{
          page: page,
          pageSize: 20,
          status:_this.data.currentTab
        },
        success:function(res){
          console.log(res)
          var list =res.data.data.list
          if(list.length>0){
            wx.hideLoading();
          }else{
            wx.hideLoading();
            wx.showToast({
              title: '暂无更多',
              icon:'none'
            })
          }
          var lists = list_ALL.concat(list)    
          _this.setData({
              sendList : lists,
            });
        },
        fail:function(res){
          wx.showToast({
            title: res.errmsg,
          })
  
      },
      })
    }else if(_this.data.currentTab=='0'){
      wx.request({
        url: app.globalData.url83 +'api/order/selectByBeanBefore',
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
        data:{
          page: page,
          pageSize: 20,
          status:''	
        },
        success:function(res){
          console.log(res)
          var list =res.data.data.list
          if(list.length>0){
            wx.hideLoading();
          }else{
            wx.hideLoading();
            wx.showToast({
              title: '暂无更多',
              icon:'none'
            })
          }
          var lists = list_ALL.concat(list) 
          _this.setData({
              sendList : lists,
            });
        },
        fail:function(res){
          wx.showToast({
            title: res.errmsg,
          })
      },
      })
    }
 },
 Reservation_again:function(e){
   console.log(e)
   var hotel_name=e.currentTarget.dataset.hotel_name
   wx.navigateTo({
    //  url: '/pages/ucenter/hotelxq/hotelxq?hotel_name='+'hotel_name',
    url: '/pages/hotellist/hotellist',
   })
 },
 details:function(e){
  console.log(e)
  var code=e.currentTarget.dataset.code
  wx.navigateTo({
    url: '/pages/hotelorder/orderlist/hotelorderDetails/hotelorderDetails?code=' + code,
  })
 },
 evaluate:function(e){
   console.log(e)
   var code=e.currentTarget.dataset.code
   wx.navigateTo({
    url: '/pages/hotelorder/orderlist/evaluate/evaluate?code=' + code,
  })
 }
})