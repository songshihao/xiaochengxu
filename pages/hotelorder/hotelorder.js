var back = require('../../utils/back.js'); //引用外部的js文件
var util = require('../../utils/util.js');
var api = require('../../config/api.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mingxi: "none",
    hotelName: '',
    price: '',
    price1: '', //初始价格，单价
    time: '18:00',
    C_password:0,
    roomNum: 1,
    useName: '',
    phone: '',
    departure_time: '',
    check_in_time: '',
    startdate:'',
    startTime: '',
    endTime: '',
    timess: '',
    hotel_id: '',
    room_type_id: '',
    roomType: '',
    roomLabel:'',
    array: ['选择房间数量', 1, 2, 3, 4, 5],
    coupons: 1,
    coupon: 0,
    couponchecked: false,
    payPass: '',
    isRuleTrue: false,
    //用户输入明文、暗文
    showType: 'password', //如果是密码换成'password'
    isFocus: false,
    zfpassword: '',
    //默认四位，手动copy两个json，正常密码框是6位
    dataSource: [{
        initValue: ''
      }, {
        initValue: ''
      }, {
        initValue: ''
      }, {
        initValue: ''
      },
      {
        initValue: ''
      }, {
        initValue: ''
      }
    ],
    roomVoucherNub: '',
    red_envelopes: '',
    incentive_voucher: '',
    token: '',
    ajxtrue: false,
    hdatahieht:'',
    hdatatop:'',
    userList:[],
    isusername:'',
    ismobile:'',
    hotel_telephone:'',
    securityCode:'',
    checkintime:'',
    checkouttime:'',
    z_balance:'',
    userLevelId:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var that = this
    that.setData({
      room_type_id: options.id,
      hotelName: options.hotelName,
      hotel_id: options.hotel_id,
      price: options.price,
      price1: options.price,
      roomType: options.roomType,
      roomLabel:options.roomLabel,
      hotel_telephone:options.hotel_telephone,
      checkintime:options.checkintime,
      checkouttime:options.checkouttime,
      timess:options.dates
    })
    var startTime_a = options.checkintime
    var endTime_a = options.checkouttime
    var startTime_y = startTime_a.replace('年', '-');
    var startTime_m = startTime_y.replace('月', '-');
    console.log(startTime_m)
    var startTime1 = startTime_m.replace('日', '');
    var endTime_y = endTime_a.replace('年', '-');
    var endTime_m = endTime_y.replace('月', '-');
    var endTime1 = endTime_m.replace('日', '');
    that.setData({
      check_in_time: startTime1,
      departure_time: endTime1,
      startTime: startTime_a,
      endTime: endTime_a,
    })
    var hdatahieht=wx.getStorageSync('hdatahieht')
    var hdatatop=wx.getStorageSync('hdatatop')
      that.setData({
        hdatahieht:hdatahieht,
        hdatatop:hdatatop
      });
    that.fanquan();
  },
  fanquan:function(res){
    var _this=this
    util.request(api.checkBalances, null, 'POST').then(function(res) {
      wx.hideLoading();
      let z_balance=res.data.hotelVoucher + res.data.redEnvelopes +  res.data.incentiveVoucher
      console.log('总余额：' + z_balance)
      _this.setData({
                  hotel_voucher:res.data.hotelVoucher,
                  red_envelopes:res.data.redEnvelopes,
                  incentive_voucher:res.data.incentiveVoucher,
                  z_balance:z_balance
                })
    })
  },
  mingxi: function (e) {
    var mingxi = this.data.mingxi
    this.setData({
      mingxi: "block"
    })
    if (mingxi == "block") {
      this.setData({
        mingxi: "none"
      })
    }
  },
  mxtit_off: function () {
    this.setData({
      mingxi: "none"
    })
  },
  checkboxChange: function (e) {
    this.setData({
      coupon: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let userLevelId=wx.getStorageSync('userLevelId')
    let startdate= util.formatTime6(new Date());
    this.setData({
      userLevelId:userLevelId,
      startdate:startdate
    })
    util.request1(api.info_list, {
      page : 1
    },).then(function(res) {
    })
    this.showList();
  },
  showList(){
    var that=this
    util.request1(api.userInfoList,{
      page:1
    },'POST').then(res => {
      if(res.code == 0){
        that.setData({
          userList: res.data.list
        })
        var list =res.data.list
        for(var i=0;i<list.length;i++){
          var isDefault =list[i].isDefault
          if(isDefault==1){
            that.setData({
              useName:list[i].name,
              phone:list[i].mobile
            })
          }
        } 
      }
    })
  },
  //停止下拉刷新
  onPullDownRefresh: function () {
    var that = this
    wx.stopPullDownRefresh({
      success: function (res) {
        that.onLoad()
      }
    })
  },
  // roomNum: function (e) {
  //   this.setData({
  //     roomNum: e.detail.value
  //   })
  // },
  useName: function (e) {
    this.setData({
      useName: e.detail.value
    })
  },
  phone: function (e) {
    var usephone = e.detail.value;
    let that = this
    if (!(/^1[3456789]\d{9}$/.test(usephone))) {
      this.setData({
        ajxtrue: false
      })
      if (usephone.length >= 11) {
        wx.showToast({
          title: '手机号有误',
          icon: 'none',
          duration: 2000
        })
      }
    } else {
      this.setData({
        ajxtrue: true,
        phone: e.detail.value
      })
    }
  },
  roomnum: function (e) {
    this.setData({
      // index: e.detail.value,
      roomNum: e.detail.value,
      price: (e.detail.value) * this.data.price1
    })
  },
  bindTimeChange: function (e) {
    this.setData({
      time: e.detail.value
    })
  },
  bindDateChange: function (e) {
    this.setData({
      check_in_time: e.detail.value
    })
    var value=e.detail.value
    this.price_qq(value);
    var A = this.data.departure_time
    var B = e.detail.value
    var sdate = new Date(B);
    var now = new Date(A);
    var days = now.getTime() - sdate.getTime();
    var day = parseInt(days / (1000 * 60 * 60 * 24));
    console.log(day)
    if (day >= 0) {
      this.setData({
        timess: day,
      })
    }
  },
  price_qq:function(value){
    var that=this
    var roomNum=that.data.roomNum
    wx.request({ 
      url: api.frist_day,
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
     data:{
      roomTypeId:that.data.room_type_id,
      checkInTime:value
     },
     success:function(res){
       console.log(res)
       if(res.data.code==0){
        that.setData({
          price:res.data.price *roomNum
         })
       }else{
         wx.showToast({
           title: '请求异常',
           icon:'none'
         })
       }
       
     },
     fail:function(res){
      console.log(res)
      wx.showToast({
        title: '请求失败',
        icon:'none'
      })
     }
    })
  },
  bindDateChange1: function (e) {
    this.setData({
      departure_time: e.detail.value
    })
    var A = e.detail.value
    var B = this.data.check_in_time
    var sdate = new Date(B);
    var now = new Date(A);
    var days = now.getTime() - sdate.getTime();
    var day = parseInt(days / (1000 * 60 * 60 * 24));
    if (day >= 0) {
      this.setData({
        timess: day
      })
    }
  },
  xiadan: function (e) {
    var that = this;
        //预定结果消息推送
  //  subscribeMessage:function(){
    wx.requestSubscribeMessage({
      "tmplIds":['ssV9uZCD6t3b6nSXHIQSd2rW9zpVNgltvpKPilNk3L8'],
      complete (res) {
        // wx.showToast({
        //   title: '订阅成功',
        // })
    var price = that.data.price
    var coupon = that.data.coupon
    var check_in_time = that.data.check_in_time
    var departure_time = that.data.departure_time
    var hotelName = that.data.hotelName
    var sdate = new Date(check_in_time);
    var now = new Date(departure_time);
    var days = now.getTime() - sdate.getTime();
    var day = parseInt(days / (1000 * 60 * 60 * 24));
    if (day >= 0) {
      that.setData({
        timess: day
      })
    }else  if (day < 0){
      wx.showModal({
        title: '错误信息',
        content: '离开日期要大于入住日期',
        showCancel: false
      })
      return;
    }
    if (that.data.roomNum == "" || that.data.useName == "") {
      wx.showModal({
        title: '错误信息',
        content: '房间数、住客姓名必填',
        showCancel: false
      });
      return false;
    } else if (that.data.phone =="") {
      wx.showToast({
        title: '手机号有误',
        icon: 'none',
        duration: 2000
      })
    } else if (that.data.roomNum != "" || that.data.useName != "" || that.data.phone == " ") {
      // if (that.data.password != that.data.confirmPassword) {
      //   wx.showModal({
      //     title: '错误信息',
      //     content: '确认密码不一致',
      //     showCancel: false
      //   });
      //   return false;
      // }
      if (coupon == 1 || that.data.userLevelId==12) {
        wx.request({ 
          url: api.whetherToSet,
          method: "POST",
          header: {
            'content-type': "application/x-www-form-urlencoded",
            'X-Nideshop-Token': wx.getStorageSync('token')
          },
         data:{},
         success:function(res){
           if(res.data.code==1){
            that.setData({
              isRuleTrue: true,
            })
           }else if(res.data.code==-1){
            wx.showModal({
              title: '提示',
              content: '支付密码未设置，请先设置支付密码',
              success (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                  wx.navigateTo({
                    url: '/pages/ucenter/password/password?newpayPass=1',
                  })
                }
              }
            })
           }else{
             util.showErrorToast('请求异常')
           }
         },
         fail:function(res){
          console.log(res)
         },
        })
      } else if (coupon == ''&&that.data.userLevelId!=12) {
        that.zfzf();
      }
    }
    }
    })
  },
  submitOrder1: function () {
    var that = this
    util.jc_LoadShow("加载中...");
    wx.request({
      url: app.globalData.url83 + 'api/order/pasVerification',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data: {
        payPassword: that.data.payPass
      },
      success: function (res) {
        util.jc_LoadHide();
        that.hideRule();
        if(res.data.errno==401){
          wx.showModal({
            title: res.data.errmsg,
            content: "token失效，请重新登录",
            success (res) {
              wx.navigateTo({
                url: '/pages/auth/login/login',
              })
            }
          })
        }
        if (res.data.code == 0) {
          console.log('密码验证成功')
          that.setData({
            securityCode: res.data.key,
            isRuleTrue: false,
          })
          that.zfzf();
        } else if (res.data.code == '-1') {
          wx.showModal({
            title: res.data.msg,
            content: '密码错误'
          })
        } else if (res.data.code == '-2') {
          wx.showModal({
            title: res.data.msg,
            success(res) {
              console.log(res)
              if (res.confirm) {
                wx.navigateTo({
                  url: '/pages/ucenter/password/password?newpayPass=1',
                })
              } else if (res.cancel) {
                console.log('您取消了下单取消')
              }
            }
          })
        }
      },
      fail: function (res) {
        util.hideLoading()
        console.log(res)
      }
    })
  },
  zfzf: function () {
    var that = this
    var price = that.data.price
    var coupon = that.data.coupon
    var check_in_time = that.data.check_in_time
    var departure_time = that.data.departure_time
    var hotelName = that.data.hotelName
    var useName = that.data.useName
    var count = that.data.roomNum
    var hotel_telephone=that.data.hotel_telephone
    wx.request({
      url: app.globalData.url83 + 'api/order/checkIn',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        // 'content-type': "application/json",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data: {
        hotel_id: that.data.hotel_id,
        room_type_id: that.data.room_type_id,
        // room_type_id:'undefined',
        check_in_time: that.data.check_in_time,
        departure_time: that.data.departure_time,
        securityCode: that.data.securityCode,
        // price:this.data.price,
        // time:this.data.time,
        count: that.data.roomNum,
        name: that.data.useName,
        phone: that.data.phone,
        arrive_time: that.data.time
      },
      success: function (res) {
        // console.log(res)
        var that = this
        if (res.data.code == 1) {
          // that.setData({
          //   amountPayable:res.data.orderForm.amountPayable
          //  })
          wx.navigateTo({
            url: '/pages/hotelorder/order/zfsuccess/zfsuccess?amountPayable=' + res.data.orderForm.amountPayable+'&hotel_telephone='+ hotel_telephone,
          })
        } else if (res.data.code == 0) {
          var orderFormId = res.data.orderForm.orderCode
          var amountPayable = res.data.orderForm.amountPayable
          if (orderFormId != undefined) {
            wx.navigateTo({
              url: '/pages/hotelorder/order/order?orderFormId=' + orderFormId + "&price=" + price + "&check_in_time=" + check_in_time +
                "&departure_time=" + departure_time + "&hotelName=" + hotelName + "&useName=" + useName + "&count=" + count + "&amountPayable=" + amountPayable,
            })
          } else if (orderFormId == undefined) {
            wx.showModal({
              title: '下单失败',
              // content: '',
              showCancel: false
            });
          }
        } else if (res.data.code == 500) {
          wx.showToast({
            title: '服务器请求异常',
            icon: 'none',
            duration: 500
          })
          return
        }else if(res.data.errno !=0 || res.data.errno!=1) {
          console.log(res.data.errmsg)
          wx.showModal({
            title: '提示',
            content:res.data.errmsg ,
            success (res) {
              setTimeout(function(){
                back.onBackTap() //调用
              },1000)
            }
          })
        }


      },
      fail: function (res) {
        console.log(res)
      }

    })



  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  open: function () {
    wx.showActionSheet({
      itemList: ['400-756-9888'],
      success: function (res) {
        console.log(res) //当点击400-900-2250就相当于点击了
        wx.makePhoneCall({
          phoneNumber: '400-756-9888', //此号码并非真实电话号码，仅用于测试  
          success: function () {
            console.log("拨打电话成功！")
          },
          fail: function () {
            console.log("拨打电话失败！")
          }
        })
        if (!res.cancel) {
          console.log(res.tapIndex) //console出了下标
        }
      }
    });
  },
  onBackTap: function (e) {
    back.onBackTap() //调用
  },
  onTapFocus: function () {
    this.setData({
      isFocus: true
    });
  },
  //打开输入弹窗
  showRule: function () {
    // var isRuleTrue =_this.data.isRuleTrue
    this.setData({
      isRuleTrue: true
    })
  },

  //关闭输入弹窗
  hideRule: function () {
    // var isRuleTrue =_this.data.isRuleTrue
    this.setData({
      isRuleTrue: false,
      payPass: ''
    })
  },
  clearInput: function (e) {
    this.setData({
      isRuleTrue: false
    })
    console.log(e)
    switch (e.currentTarget.id) {
      case 'clear-username':
        this.setData({
          username: ''
        });
        break;
      case 'clear-password':
        this.setData({
          password: ''
        });
        break;
      case 'clear-confirm-password':
        this.setData({
          confirmPassword: ''
        });
        break;
      case 'clear-code':
        this.setData({
          code: ''
        });
        break;
    }
  },
  payPass: function (e) {
    this.setData({
      payPass: e.detail.value
    }, 100);
    if (e.detail.cursor > 6) {
      wx.showModal({
        title: '密码必须六位数',
        showCancel: false
      })
    }
  },
  mobileInput: function (e) {
    console.log(e)
    let dataSource = this.data.dataSource;
    let curInpArr = e.detail.value.split('');
    let curInpArrLength = curInpArr.length;
    console.log(dataSource)
    console.log(curInpArr)
    console.log(curInpArrLength)
    // if (curInpArr.length != this.data.dataSource.length){
    for (let i = 0; i < dataSource.length - curInpArrLength; i++) {
      curInpArr.push('');
    }
    // }
    for (let i = 0; i < this.data.dataSource.length; i++) {
      let initValue = 'dataSource[' + i + '].initValue';
      1
      this.setData({
        [initValue]: curInpArr[i]
      });
    }

    this.setData({
      payPass: e.detail.value
    }, 100);

    console.log(e.detail.value)
  },
  C_password:function(e){
    console.log(e)
    var that=this
    var type=e.currentTarget.dataset.password
    if(type==0){
      that.setData({
        C_password:1
      })
    }else if(type==1){
      that.setData({
        C_password:0
      })
    }
  
  },
})