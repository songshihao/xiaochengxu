// pages/ranklsit/ranklsit.js
let City = require('../../utils/allcity');
var util = require('../../utils/util.js');
var api = require('../../config/api.js');
var back=require('../../utils/back')
const app=getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    mode:'widthFix',
    city:'上海',
    goTop:app.globalData.goTop,
    floorstatus:'',
    zan:100,
    zan_active:'',
    cai:99,
    cai_active:'',
    status:'',
    spread_status:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    if(options.city){
      this.setData({
        city:options.city
      })
    }
 
  },
    // 获取滚动条当前位置
    onPageScroll: function (e) {
      if (e.scrollTop > 100) {
        this.setData({
          floorstatus: true
        });
      } else {
        this.setData({
          floorstatus: false
        });
      }
    },
    zan:function(){
      console.log('123')
      let that=this
      let zan=that.data.zan
      let zan_active=that.data.zan_active
      let cai_active=that.data.cai_active
      if(zan_active!=1&&cai_active!=1){
        zan++;
        that.setData({
          zan:zan,
          zan_active:1,
          status:1
        })
        that.zanrequest();
      }else if(zan_active==1&&cai_active!=1){
        zan--;
        that.setData({
          zan:zan,
          zan_active:2,
          status:0
        })
        that.zanrequest();
      }
    },
    zanrequest:function(){
      let that=this
      wx.request({
        url: api.leaderboard,
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
       data:{
        hotelId:'1',
        status:that.data.status
       },
       success:function(res){
        console.log(res)
       },
       fail:function(res){
         console.log(res)
       }
        
      })
    },
    leaderboard_spread:function(){
      let that=this
      wx.request({
        url: api.leaderboard_spread,
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
       data:{
        hotelId:'1',
        status:that.data.spread_status
       },
       success:function(res){
        console.log(res)
       },
       fail:function(res){
         console.log(res)
       }
        
      })
    },
    cai:function(){
      let that=this
      let cai=that.data.cai
      let cai_active=that.data.cai_active
      let zan_active=that.data.zan_active
      if(cai_active!=1&&zan_active!=1){
        cai++;
        that.setData({
          cai:cai,
          cai_active:1,
          zan_active:2,
          spread_status:1
        })
        this.leaderboard_spread();
      }else if(cai_active==1&&zan_active!=1){
        cai--;
        that.setData({
          cai:cai,
          cai_active:2,
          spread_status:0
        })
        this.leaderboard_spread();
      }
    },
  //回到顶部
  goTop: function (e) {  // 一键回到顶部
    if (wx.pageScrollTo) {
      wx.pageScrollTo({
        scrollTop: 0
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  },
  city:function(){
    wx.navigateTo({
      url: '/pages/ucenter/ceshi/ceshi?type=rank',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onLoad()
    setTimeout(function(){
        wx.stopPullDownRefresh({
        complete: (res) => {
          wx.showToast({
            title: '刷新成功',
            icon:'none'
          })
        },
      })
    },1000)
  },
  //返回上一页
  onBackTap: function (e) {
    back.onBackTap() //调用
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})