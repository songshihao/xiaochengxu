var util = require('../../utils/util.js');
const user = require('../../services/user.js');
var api = require('../../config/api.js');
import qqmap from '../../utils/map.js';
var QQMapWX = require('../../lib/qqmap-wx-jssdk.min');
 
// 实例化API核心类
var qqmapsdk = new QQMapWX({
    key: 'GVOBZ-APK6P-BOZDT-V5ZWX-MRI7T-6GF5H' // 必填
});
const chooseLocation = requirePlugin('chooseLocation');
var flag = 0;
var classCatch = ['current', 'next', 'next', 'next'];
var touch = [0, 0];
//获取应用实例
const app = getApp()
Page({
    data: {
      banner:[],
      startTime: '',
      endTime:'',
      startTime_a:'',
      endTime_a:'',
      startTime_b:'',
      endTime_b:'',
      startTime1:'',

      choose_year: '',
      dates:'1',
      username:'',
      queryCondition:'',
      starLevel:'',
      token:'',
      name:'家畅酒店' ,
      address:'',
      mode: 'aspectFill',
      referrer:'',
      canshu:'',
      mingxi:'none',
      floorstatus:'',
      bg_price1:'',
      end_price1:'',
      pres: [
        { id: '0', value: '经济型' }, 
        { id: '1',value:'舒适型'},
        { id: '2', value: '豪华型' }, 
        { id: '3',value:'其他'},
      ],
      id:'0',
      weizhi:'',
      goTop:app.globalData.goTop,
      imgUrl:''
    },
    onLoad: function (options) {
      //调用app中的函数
      var that=this
      that.setData({
        referrer:"<"+options.nickName+">"
      })
      wx.setStorageSync('username1', options.username);
      if(that.data.referrer!=''&&options.nickName!=undefined){
        var usename=wx.getStorageSync('username')
        if(usename==''){
          that.setData({
            isRuleTrue: true
          })
          setTimeout(function() {
            that.setData({
              isRuleTrue: false
            })
          }, 9000);
        }
      }
      if(that.data.weizhi!=''){
        wx.clearStorageSync('locatecity')
      }
      that.getLocate()
        // 页面初始化 options为页面跳转所带来的参数
      that.getIndexData();
      that.getLocate();
    wx.setNavigationBarTitle({ title:'家畅智能酒店置换平台——人人都可以免费住酒店点击领取100元全额抵用红包券！'}) //页面标题为路由参数 
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
    },
      //打开输入弹窗
      showRule: function () {
        this.setData({
          isRuleTrue: true
        })
      },
      //关闭输入弹窗
      hideRule: function () {
        this.setData({
          isRuleTrue: false
        })
      },
    cha:function(){
      var that=this
      var timedata=[
        {
          startTime :that.data.startTime_b,
          endTime:that.data.endTime_b,
          startTimeq:that.data.startTime,
          startTimeaa:that.data.startTime,
          endTimeq:that.data.endTime,
          timess:that.data.dates,
        }
      ]
      wx.setStorage({
        data: timedata,
        key: 'timedata',
      })
      var tokon = wx.getStorageSync('token');
      that.setData({
        token:tokon
      })
      if (tokon!='') {
        if(that.data.dates>0){
          wx.navigateTo({
            url: '/pages/hotellist/hotellist?page=1'
          })
        }else
         if(that.data.dates<=0){
          wx.showModal({
            title:"错误提示",
            content:"结束时间不能小于开始时间，请重新选择"
          })
        }
      } else {
        wx.navigateTo({
          url: "/pages/auth/login/login?page=1"
        });
      }
    },
    onReady: function () {
        // 页面渲染完成
    },
    onShow: function (options) {
        // 页面显示
    //获取用户的登录信息
    var that=this
    util.request(api.home_reservation,null,).then(function(res){
      if(res.errno==0){
        that.setData({
          imgUrl:res.data.imgUrl
        })
      }
    })
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    if(getDate){
      that.setData({
        startTime_b: getDate.checkInDate,
        endTime_b: getDate.checkOutDate,
      })
      var A = getDate.checkInDate
      var B = getDate.checkOutDate
      var sdate = new Date(A);
      var now = new Date(B);
      var days = now.getTime() - sdate.getTime();
      var day = parseInt(days / (1000 * 60 * 60 * 24));
      if (day >= 0) {
        that.setData({
          dates: day,
        })
      }
    }else{ //获取当前时间
            var TIME = util.formatTime1(new Date());
            var TIME2 = util.formatTime3(new Date());
            var startTime1 = TIME[0];
            var endTime1 = TIME2[0];

            var A = startTime1
            var B = endTime1
            var sdate = new Date(A);
            var now = new Date(B);
            var days = now.getTime() - sdate.getTime();
            var day = parseInt(days / (1000 * 60 * 60 * 24));
            if (day >= 0) {
              that.setData({
                dates: day,
              })
            }
            that.setData({
                startTime: TIME[0],
                endTime: TIME2[0],
                startTime_a: TIME[0],
                endTime_a: TIME2[0],
                startTime_b:startTime1,
                endTime_b:endTime1,
              });
          }
          that.getLocate();
    },
    onHide: function () {
        // 页面隐藏
    },
    onUnload: function () {
        // 页面关闭
    },
    getIndexData: function() {
      let that = this;
      util.request(api.hotelbanner,{
        position:2
      }).then(function(res) {
        if (res.errno === 0) {
          that.setData({
            banner: res.data.items,
          });
        }
      });
      util.request(api.GoodsCount).then(function (res) {
        that.setData({
          goodsCount: res.data.goodsCount
        });
      });
    },
  // 获取滚动条当前位置
  onPageScroll: function (e) {
    if (e.scrollTop > 100) {
      this.setData({
        floorstatus: true
      });
    } else {
      this.setData({
        floorstatus: false
      });
    }
  },

  //回到顶部
  goTop: function (e) {  // 一键回到顶部
    if (wx.pageScrollTo) {
      wx.pageScrollTo({
        scrollTop: 0
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  },
  intoMap:function(){
    wx.navigateTo({
      url: '/pageA/pages/map/map',
    })
  },
    //调用定位
    getLocate() {
      let that = this;
      new qqmap().getLocateInfo().then(function (val) {//这个方法在另一个文件里，下面有贴出代码
        that.setData({
          address:val
        })
        if (val.indexOf('市') !== -1) {//这里是去掉“市”这个字
          val = val.slice(0, val.indexOf('市'));
        }
        that.setData({
          locateCity: val
        });
        //把获取的定位和获取的时间放到本地存储
        wx.setStorageSync('locatecity', { city: val, time: new Date().getTime() });
      });
      // this.intoMap()
    },
      //停止下拉刷新
  onPullDownRefresh: function () {
    setTimeout(function(){
      wx.stopPullDownRefresh({
        })
    },500)  
  },
  onShareAppMessage: function (res) {
    wx.getStorage({
      key: 'userInfo',
      success:function(res){
        nickName=res.data.nickName
      }
    })
    if (res.from === 'button') {
      // 通过按钮触发
      var data = res.target.dataset
      return {
        title: '家畅——人人都可以免费住酒店点击领取100元全额抵用红包券！',
        desc: '家畅智能酒店置换商城平台通过置换酒店空置房，来帮助酒店免费升级为智慧酒店，并免费提供酒店所需的物资和服务，致力于打造中国最大的智慧酒店',
       path: '/pages/topic/topic?username=' + wx.getStorageSync('username') + '&nickName=' + wx.getStorageSync('nickName'),
        imageUrl: 'https://jiachang8.com/mpimage/share_img4.jpg',
        success: function (res) {
          // 转发成功
          console.log('转发成功')
        },
        fail: function (res) {
          // 转发失败
          console.log('转发失败')
        }
      }
    }
    //通过右上角菜单触发
    return {
      title: '家畅——人人都可以免费住酒店点击领取100元全额抵用红包券！',
      desc: '家畅智能酒店置换商城平台通过置换酒店空置房，来帮助酒店免费升级为智慧酒店，并免费提供酒店所需的物资和服务，致力于打造中国最大的智慧酒店',
      // path: "/pages/researches/index",
     path: '/pages/topic/topic?username=' + wx.getStorageSync('username') + '&nickName=' + wx.getStorageSync('nickName'),
      imageUrl: 'https://jiachang8.com/mpimage/share_img4.jpg',
    };
  },
  mingxi:function(e){
    var mingxi=this.data.mingxi
    this.setData({
      mingxi:"block"
    })
    if(mingxi=="block"){
      this.setData({
        mingxi:"none"
      })
    }
  },
  mxtit_off:function(){
    this.setData({
      mingxi:"none"
    })
    var that=this
    setTimeout(function(){
      if(that.data.hoteladdressinput!=''){
        wx.clearStorageSync('locatecity')
      }
    },1000)
  },
  name_chaxun:function(res){
    // this.shanxuan();
    var that=this
    var timedata=[
      {
        startTime :that.data.startTime_b,
        endTime:that.data.endTime_b,
        startTimeq:that.data.startTime,
        startTimeaa:that.data.startTime,
        // endTimeq:that.data.endTime_a,
        endTimeq:that.data.endTime,
        timess:that.data.dates,
        hoteladdressinput:that.data.hoteladdressinput,//酒店位置
        hotelnameinput:that.data.hotelnameinput,//酒店名称
        bg_price:that.data.bg_price1,
        end_price:that.data.end_price1
      }
    ]
    wx.setStorage({
      data: timedata,
      key: 'timedata',
    })
    wx.navigateTo({
      url: '/pages/hotellist/search/search',
    })
  },
  changeColor: function(e){
    var _this = this
    var id=_this.data.id;
    _this.setData({
     id:e.currentTarget.dataset.id,
     orderAmount:e.currentTarget.dataset.value,
    roomVoucherNub :e.currentTarget.dataset.value
    }); 
    },
    rank_more:function(){
      wx.navigateTo({
        url: '/pages/ranklsit/ranklsit',
      })
    },
    pricedate:function(){
      var checkInDate=this.data.startTime
      var checkOutDate=this.data.endTime
     wx.navigateTo({
       url: '/pages/hotellist/pricedate/index?checkInDate='+ checkInDate+'&checkOutDate='+checkOutDate,
     })
    },
})