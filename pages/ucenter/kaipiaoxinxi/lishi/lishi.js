// pages/wode/kaipiaoxinxi/lishi/lishi.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nr: "",
    ts: "",
    tsshow: false,
    msg: '',
    msgshow: false,
  },
  fapiaotaitou: function(e) {

    var fptt = "nr.invoice"
    this.setData({
      [fptt]: e.detail.value
    })
  },
  nashuiren: function(e) {

    var fptt = "nr.identifier"
    this.setData({
      [fptt]: e.detail.value
    })
  },
  fapiaoneirong: function(e) {
    var fptt = "nr.content"
    this.setData({
      [fptt]: e.detail.value
    })
  },
  yh: function(e) {
    var fptt = "nr.bank";
    this.setData({
      [fptt]: e.detail.value
    })
  },
  zh:function(e){
    var fptt = "nr.cardNo";
    this.setData({
      [fptt]: e.detail.value
    })
  },
  dz: function(e) {
    var fptt = "nr.address"
    this.setData({
      [fptt]: e.detail.value
    })
  },
  dh: function(e) {
    var fptt = "nr.phone"
    this.setData({
      [fptt]: e.detail.value
    })
  },
  checkBankCard: function(cardNo) {
    var tmp = true,
      total = 0;
    for (var i = cardNo.length; i > 0; i--) {
      var num = cardNo.substring(i, i - 1);
      if (tmp = !tmp, tmp) num = num * 2;
      var gw = num % 10;
      total += (gw + (num - gw) / 10);
    }
    return total % 10 == 0;
  },
  bc: function() {
    var _this = this
    if (!_this.data.nr.invoice||!_this.data.nr.identifier) {
      wx.showToast({
        title: '请输入发票抬头和纳税人识别号',
      })
      return
    }
    var phone = this.data.nr.phone
    if (!(/^1[3456789]\d{9}$/.test(phone)) && !(/0\d{2,3}-\d{7,8}/.test(phone)) && phone != "") {
      wx.showToast({
        title: '请填写正确的电话号',
      })
      return

    }
    if (this.data.nr.cardNo != "" && this.data.nr.bank == "") {
      wx.showToast({
        title: '填写账号请先填写开户行',
      })
      return;
    }
    var aa = this.checkBankCard(this.data.nr.cardNo);
    if (!aa) {
      wx.showToast({
        title: '账号填写错误',
      })
      return;
    }


    wx.request({
      url: 'https://www.jiachang8.com/wxmp/client/updateInvoice',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'Cookie': wx.getStorageSync("JSESSIONID") //读取cookie
      },
      data: {
        customerId: _this.data.nr.customerId,
        invoice: _this.data.nr.invoice,
        identifier: _this.data.nr.identifier,
        content: _this.data.nr.content,
        bank: _this.data.nr.bank,
        cardNo: _this.data.nr.cardNo,
        address: _this.data.nr.address,
        phone: _this.data.nr.phone,
        id: _this.data.nr.id,
      },
      success: function(e) {

        if (e.data.status) {
          wx.navigateTo({
            url: '/pages/wode/wode',
          })
          wx.showToast({
            title: '更改成功',
            icon: "success",
            duration: 2000,
          })
        } else {

          _this.setData({
            msg: e.data.msg,
            tsshow: false,
            msgshow: true,
          })
          setTimeout(function() {
            _this.setData({
              msgshow: false,
            })
          }, 2000)
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    this.setData({
      nr: options
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})