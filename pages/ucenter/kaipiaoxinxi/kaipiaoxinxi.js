// pages/wode/kaipiaoxinxi/kaipiaoxinxi.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bd: [],
    bdcd: 0,
    msg: "",
    msgshow: false,
    formId:'',
  },
  kaifapiao: function(e) {
    var _this = this;
    // if (!app.globalData.isInRoom) {
    //   wx.showModal({
    //     title: '您还没有注册',
    //     content: '是否去注册？',
    //     cancelText: "否",
    //     confirmText: "是",
    //     confirmColor: "#00a8ff",
    //     success: function (res) {
    //       if (res.confirm) {
    //         wx.navigateTo({
    //           url:'/pages/zhuce/zhuce',
    //         })
    //       }
    //     }
    //   })
    //   return;
    // }

    var id = this.data.bd[e.currentTarget.dataset.id].id


    var s = new Date();

    var a = s.getFullYear(); //年
    var b = s.getMonth() + 1; //月
    var c = s.getDate(); //日
    var d = s.getHours(); //时
    var e = s.getMinutes(); //分
    var f = s.getSeconds(); //秒
    if (b < 10) {
      b = "0" + b;
    };
    if (c < 10) {
      c = "0" + c;
    };
    if (d < 10) {
      d = "0" + d;
    };
    if (e < 10) {
      e = "0" + e;
    };
    if (f < 10) {
      f = "0" + f;
    }


    var shijian = a + "-" + b + "-" + c + " " + d + ":" + e + ":" + f // 获取到的时间 


    wx.request({
      url: app.globalData.url83+'api/invoice/selectByMap',
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        'X-Nideshop-Token':wx.getStorageSync('token')
      },
      data: {
        page:1,
        pageSize:10
        // customerId: app.globalData.userId, //用户编号  
        // hotelId: app.globalData.hotelId, //  酒店id     
        // roomNo: app.globalData.roomNo, // 房间编号         
        // serviceType: 7, //服务类型  
        // timeType: id, //id
        // dataTime: shijian, //时间
        // formId: _this.data.formId,
      },
      success: function(e) {
        console.log(e)
        if (e.data.status) {

          _this.setData({
            msg: "已通知前台开发票"
          })
        } else {

          _this.setData({
            msg: e.data.msg
          })
        }
        _this.setData({
          msgshow: true
        })
        setTimeout(function() {
          _this.setData({
            msgshow: false
          })
        }, 1000)
      }
    })
  },
  getFormID: function (e) {
    this.setData({
      formId: e.detail.formId
    });
    this.kaifapiao(e)
  },
  kong:function(){

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var _this = this;
    //获取历史发票信息
    wx.request({
      url: app.globalData.url83+'api/invoice/selectByMap',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token':wx.getStorageSync('token')
      },
      data: {
        page: 1,
        pageSize: 10
      },
      success: function(e) {

        console.log(e)
        //获取 e的  信息 及长度  把数据添加到 bd 中
        if (e.data.code==0) {
          _this.setData({
            bd: e.data.data.list
          })
        }

      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})