// pages/wode/kaipiaoxinxi/gongsifapiaoxinxi/kaihuhangjizhanghao/kaihuhangjizhanghao.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    kaihuhang: "",
    zhanghao: "",
    tskshow: false,
    tsk: "",
  },
  kaihuhang: function(t) {

    this.setData({
      kaihuhang: t.detail.value,
    })

  },
  zhanghao: function(t) {
    this.setData({
      zhanghao: t.detail.value
    })
  },
  // 
  checkBankCard: function(cardNo) {
    var tmp = true,
      total = 0;
    for (var i = cardNo.length; i > 0; i--) {
      var num = cardNo.substring(i, i - 1);
      if (tmp = !tmp, tmp) num = num * 2;
      var gw = num % 10;
      total += (gw + (num - gw) / 10);
    }
    return total % 10 == 0;
  },
  // 
  bc: function() {
    var _this = this;

    if (this.data.kaihuhang == "" || this.data.zhanghao == "") {
      this.setData({
        tsk: "请填写开户行及账号",
        tskshow: true
      });
      setTimeout(function() {
        _this.setData({
          tskshow: false
        });
      }, 1600)
      return;
    }
    var aa = this.checkBankCard(this.data.zhanghao);


    if (!aa) {
      this.setData({
        tsk: "账号填写错误",
        tskshow: true
      });
      setTimeout(function() {
        _this.setData({
          tskshow: false
        });
      }, 1600)
      return;
    }
    app.globalData.xinfapiao.kaihuhang = this.data.kaihuhang;
    app.globalData.xinfapiao.zhanghao = this.data.zhanghao;
    wx.navigateTo({
      url: "/pages/wode/kaipiaoxinxi/gongsifapiaoxinxi/gongsifapiaoxinxi"
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      kaihuhang: app.globalData.xinfapiao.kaihuhang,
      zhanghao: app.globalData.xinfapiao.zhanghao,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})