// pages/wode/kaipiaoxinxi/gongsifapiaoxinxi/gongsifapiaoxinxi.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fapiaotaitou: "",
    anshuirenshibiehao: "",
    fapiaoneirong: "",
    kaihuhang: "",
    zhanghao: "",
    dizhi: "",
    dianhua: "",
    tishikuang: '',
    show: false,
    msg: "",
    msgshow: false,
    //发票
    type: '',
    title: '',
    taxNumber: '',
    companyAddress: '',
    telephone: '',
    bankName: '',
    bankAccount: '',

  },
  fapiaotaitou: function(t) {

    this.setData({
      fapiaotaitou: t.detail.value,

    })
    app.globalData.xinfapiao.fapiaotaitou = this.data.fapiaotaitou;

  },
  nashuiren: function(t) {
    this.setData({
      anshuirenshibiehao: t.detail.value,

    });
    app.globalData.xinfapiao.anshuirenshibiehao = this.data.anshuirenshibiehao;
  },
  fapiaoneirong: function(t) {
    this.setData({
      fapiaoneirong: t.detail.value
    });
    app.globalData.xinfapiao.fapiaoneirong = this.data.fapiaoneirong;
  },
  kaihuhang: function(t) {
    this.setData({
      kaihuhang: t.detail.value
    });
    app.globalData.xinfapiao.kaihuhang = this.data.kaihuhang;
  },
  zhanghao: function(t) {
    this.setData({
      zhanghao: t.detail.value
    });
    app.globalData.xinfapiao.zhanghao = this.data.zhanghao;
  },
  dizhi: function(t) {
    this.setData({
      dizhi: t.detail.value
    });
    app.globalData.xinfapiao.dizhi = this.data.dizhi;
  },
  dianhua: function(t) {
    this.setData({
      dianhua: t.detail.value
    });
    app.globalData.xinfapiao.dianhua = this.data.dianhua;
  },



  bc: function() {
    var _this = this;
    var fpcd = this.data.fapiaotaitou.length;
    var sbcd = this.data.anshuirenshibiehao.length;


    if (fpcd == "" || fpcd == null || fpcd == undefined || fpcd == 0 || sbcd == "" || sbcd == null || sbcd == undefined || sbcd == 0) {

      _this.setData({
        tishikuang: "请填写发票抬头或纳税人识别号",
        show: true,
      });
      setTimeout(function() {
        _this.setData({
          show: false,
        })
      }, 2000)
      return;
    }

    if (this.data.fapiaoneirong == "") {
      this.setData({
        fapiaoneirong: "住宿服务费"
      })
    }
    // 下面是后台传值 如果保存成功就把app.js中的xinkaipiao中的属性的属性值清空
    wx.request({
      url:app.globalData.url83 + 'api/invoice/save',
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'X-Nideshop-Token':wx.getStorageSync('token')
      },
      data: {
        address: _this.data.dizhi,
        bank: _this.data.kaihuhang,
        cardNo: _this.data.zhanghao,
        content: _this.data.fapiaoneirong,
        // userId: app.globalData.userId, //用户编号
        identifier: _this.data.anshuirenshibiehao,
        invoice: _this.data.fapiaotaitou,
        phone: _this.data.dianhua,
        //   开户行
        //  卡号cardNo 
        //  发票内容 content  
        //  用户编号customerId 
        //  id/id
        //  标识号identifier  
        //  发票抬头invoice 
        //  电话phone-->
      },
      success: function(e) {
        console.log(e)
   

        if (e.data.code==0) {

          _this.setData({
            tishikuang: "提交成功",
            show: true,

          })


          //app.js中的xinkaipiao中的属性的属性值清空

          app.globalData.xinfapiao.anshuirenshibiehao = "";
          app.globalData.xinfapiao.dianhua = "";
          app.globalData.xinfapiao.dizhi = "";
          app.globalData.xinfapiao.fapiaoneirong = '';
          app.globalData.xinfapiao.fapiaotaitou = '';
          app.globalData.xinfapiao.kaihuhang = '';
          app.globalData.xinfapiao.zhanghao = '';
          _this.setData({
            fapiaotaitou: "",
            anshuirenshibiehao: "",
            fapiaoneirong: "",
            kaihuhang: "",
            zhanghao: "",
            dizhi: "",
            dianhua: "",
          })


          setTimeout(function() {
            _this.setData({
              show: false,
            })
          }, 1000)

          wx.redirectTo({

            url: '../kaipiaoxinxi',
          })

        } else {

          _this.setData({
            msg: e.data.msg,
            show: false,
            msgshow: true,
          })
          setTimeout(function() {
            _this.setData({
              msgshow: false,
            })
          }, 1000)
        }
      },
    })


  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    this.setData({
      fapiaotaitou: app.globalData.xinfapiao.fapiaotaitou,
      anshuirenshibiehao: app.globalData.xinfapiao.anshuirenshibiehao,
      fapiaoneirong: app.globalData.xinfapiao.fapiaoneirong,
      kaihuhang: app.globalData.xinfapiao.kaihuhang,
      zhanghao: app.globalData.xinfapiao.zhanghao,
      dizhi: app.globalData.xinfapiao.dizhi,
      dianhua: app.globalData.xinfapiao.dianhua,
    })

    if (options.kaihuhang) {
      kaihuhang: options.kaihuhang
    };
    if (options.zhanghao) {
      this.setData({
        zhanghao: options.zhanghao
      })
    };
    if (options.dizhi) {
      this.setData({
        dizhi: options.dizhi
      })
    };
    if (options.dianhua) {
      this.setData({
        dianhua: options.dianhua
      })
    };

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
 
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  chooseInvoiceTitle() {
    wx.chooseInvoiceTitle({
      success: (res) => {
        console.log(res)
        this.setData({
          type: res.type,
          fapiaotaitou: res.title,
          anshuirenshibiehao: res.taxNumber,
          dizhi: res.companyAddress,
          dianhua: res.telephone,
          kaihuhang: res.bankName,
          zhanghao: res.bankAccount
        })
      },
      fail: (err) => {
        console.error(err)
      }
    })
  }
})