// pages/wode/kaipiaoxinxi/gongsifapiaoxinxi/dizhihuodianhua/dizhihuodianhua.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    dizhi: "",
    dianhua: '',
    tskshow: false,
    tsk: "",

  },
  dizhi: function(t) {

    this.setData({
      dizhi: t.detail.value
    })
  },
  dianhua: function(t) {

    this.setData({
      dianhua: t.detail.value
    })
  },
  bc: function() {
    var _this = this;
    var phone = this.data.dianhua
    if (!(/^1[3456789]\d{9}$/.test(phone)) && !(/0\d{2,3}-\d{7,8}/.test(phone))) {
      this.setData({
        tsk: "请填写正确的电话号",
        tskshow: true
      });
      setTimeout(function() {
        _this.setData({
          tskshow: false
        });
      }, 1600)
      return

    }

    app.globalData.xinfapiao.dizhi = this.data.dizhi;
    app.globalData.xinfapiao.dianhua = this.data.dianhua;
    wx.navigateTo({
      url: "/pages/wode/kaipiaoxinxi/gongsifapiaoxinxi/gongsifapiaoxinxi"
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    this.setData({
      dizhi: app.globalData.xinfapiao.dizhi,
      dianhua: app.globalData.xinfapiao.dianhua,
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})