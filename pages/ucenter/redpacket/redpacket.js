// pages/ucenter/redpacket/redpacket.js

var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    greeting:'恭喜发财 大吉大利',
    amount:'',
    redCount:'',
    encryption:'',
    token:'',
    avatarUrl:'',
    nickName:''
  },
  greeting(e){

   this.setData({
    greeting:e.detail.value
   })
  },
  amount(e){

   this.setData({
    amount:e.detail.value
   })
  },
  redCount(e){

   this.setData({
    redCount:e.detail.value
   })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function () {
    var that=this
    wx.getStorage({
      key: 'userInfo',
      success:function(res){
        console.log(res)
        that.setData({
          avatarUrl:res.data.avatarUrl,
          nickName:res.data.nickName
        })
      }
    })
    var token = wx.getStorageSync('token');
    if (token== '') {
      wx.navigateTo({
        url: '/pages/auth/login/login',
      })
    }
    wx.getStorage({
      key: 'token',
      success:function(res) {
        // console.log(res.data)
        that.setData({
          token:res.data
        })
      },
      fail:function(res){
        // console.log(res)
        if(res.errMsg=="getStorage:fail data not found"){
             wx.navigateTo({
                url: '/pages/auth/login/login',
              })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
   onLoad: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
     //打开规则提示
     showRule: function () {
      this.setData({
        isRuleTrue: 1
      })
  },
  //关闭规则提示
  hideRule: function () {
    this.setData({
      isRuleTrue: 0
    })
    wx.redirectTo({
      url: '/pages/dts_index/index',
    })
  },
  shengcheng:function (res) {
    var that=this
    var amount=that.data.amount
    var redCount=that.data.redCount
    var limiting=(amount/redCount)
    if(limiting<0.1){
      wx.showToast({
        title: '单个红包不能小于0.1元券！',
        icon:'none'
      },200)
    }else if(redCount<= 0){
      wx.showToast({
        title: '红包个数不能为0！',
        icon:'none'
      },200)
    }else if(limiting>=0.1){
    wx.request({
      // url:'http://192.168.0.243:8383/reservation/api/user/share/red_envelope',
      url: app.globalData.url83 + 'api/user/share/red_envelope',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token':wx.getStorageSync('token')
      },
      data: {
        amount:that.data.amount, //分享的红包金额
        redCount:that.data.redCount //红包个数
      },
      success:function (res) {
        if(res.data.errno == 404)
        {
          wx.showToast({
            title: '红包个数不能小于零或余额不足！',
            icon:'none'
          },200)
        }else if(res.data.errno == 401){
          // wx.showToast({
          //   title: 'token失效，请重新登录！',
          //   icon:'none'
          // },200)
          wx.showModal({
            title: 'token失效，请重新登录',
            content: '确定重新登录,取消返回首页',
            success (res) {
            if (res.confirm) {
            wx.navigateTo({
              url: '/pages/auth/login/login',
            })
            } else if (res.cancel) {
              wx.navigateTo({
                url: '/pages/hotel/hotel',
              })
            }
          }
          })
        }
        else {
        that.setData({
          encryption:res.data.encryption,  
          isRuleTrue: 1
        })
      }
    },
      fail:function (res){
        // console.log(res)
        console.log("出错啦")
      }
    })
          
  }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    console.log(res)
    // 通过按钮触发
    var that=this
    if(that.data.encryption==''||that.data.encryption==undefined){
      wx.showToast({
        title: '红包生成失败！',
      })
    }
    that.setData({
      isRuleTrue:0
    })
    if (res.from === 'button') {
      return {
        title: that.data.greeting,//祝福语
        // desc: '家畅智能酒店置换商城平台通过置换酒店空置房，来帮助酒店免费升级为智慧酒店，并免费提供酒店所需的物资和服务，致力于打造中国最大的智慧酒店',
        path: 'pages/ucenter/receivehb/receivehb?encryption='+  that.data.encryption + '&username=' +
         wx.getStorageSync('username')+ '&greeting=' + that.data.greeting+ '&avatarUrl=' + that.data.avatarUrl+ '&nickName=' + that.data.nickName,

        imageUrl: 'https://jiachang8.com/mpimage/images/hb.png?x=1',//封面 
        success: function (res) {
          console.log(res)
          // 转发成功
          console.log('转发成功')
          wx.switchTab({
            url: '/pages/dts_index/index',
          })
        
        },
        fail: function (res) {
          // 转发失败
          console.log('转发失败')
        }
      }
      

    }
       

  }
})