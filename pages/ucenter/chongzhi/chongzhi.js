// pages/ucenter/chongzhi/chongzhi.js
const util = require('../../../utils/util');
const api = require('../../../config/api');
const AUTH_MODE = 'fingerPrint'
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pres: [{
        id: '0',
        value: '充500元送100 (83折)',
        roomvouchernub: '500'
      },
      {
        id: '1',
        value: '充800元送200元（8折）',
        roomvouchernub: '800'
      },
      {
        id: '2',
        value: '充1000元送300 (77折)',
        roomvouchernub: '1000'
      },
      {
        id: '3',
        value: '充5000元送2000 (71折)',
        roomvouchernub: '5000'
      },
      {
        id: '4',
        value: '充10000元送5000 (66折) ',
        roomvouchernub: '10000'
      },
      // { id: '5',value:'自定义'}
    ],
    id: '2',
    idx: '2',
    orderAmount: '', //金额
    roomVoucherNub: '', //房券
    // bonus:'500',
    bonuslist: {
      0: '600',
      1: '1000',
      2: '1300',
      3: '7000',
      4: '15000'
    },
    arguments:''
  },

  changeColor: function (e) {
    console.log(e)
    var _this = this
    console.log(e.currentTarget.dataset.id)
    var id = _this.data.id;
    var roomVoucherNub = _this.data.roomVoucherNub;
    _this.setData({
      id: e.currentTarget.dataset.id,
      idx: e.currentTarget.dataset.id,
      orderAmount: e.currentTarget.dataset.value,
      //  roomVoucherNub :e.currentTarget.dataset.value * .8
      roomVoucherNub: e.currentTarget.dataset.roomvouchernub

    });
  },

  setDefal: function (e) {
    var _this = this
    var id = _this.data.id;
    var roomVoucherNub = _this.data.pres[id].roomvouchernub;
    console.log(roomVoucherNub);
    _this.setData({
      idx: id,
      roomVoucherNub: roomVoucherNub
    });
  },
  shoushu(e) {
    var _this = this
    console.log(e)
    console.log(e.detail.value)
    var orderAmount = _this.data.orderAmount

    _this.setData({
      orderAmount: e.detail.value,
      // roomVoucherNub: e.detail.value * .8
      roomVoucherNub: e.detail.value

    })
  },
  clear: function (e) {
    var _this = this
    var orderAmount = _this.data.orderAmount
    console.log(e)

    _this.setData({
      roomVoucherNub: '充值的房券数量'
    })


  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var that=this
    if(options.arguments){
      that.setData({
        arguments:options.arguments
      })
    }
   
    that.setDefal()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  zf: function () {

    console.log('111')
    wx.checkIsSoterEnrolledInDevice({
      checkAuthMode: 'fingerPrint',
      success(res) {
        console.log(res.isEnrolled)
      }
    })
  },
  startAuth() {
    var _this = this
    wx.showLoading({
      // title: 'title',
    })
    var _this=this
    util.request(api.place, { 
      amount: _this.data.roomVoucherNub
    }, 'POST').then(function(res) {
    
      console.log(res)
      if(res.errno==0){
        util.request(api.prepay,
          {  orderId: res.data.orderId}, 'POST').then(function(res) {
         console.log(res)
         wx.hideLoading();
         wx.requestPayment({
          'timeStamp': res.data.timeStamp,
          'nonceStr': res.data.nonceStr,
          'package': res.data.packageValue,
          'signType': res.data.signType,
          'paySign': res.data.paySign,
          'success': function(res) {
            console.log("支付过程成功");
            console.log(res)
            if (res.errMsg == 'requestPayment:ok') {
              // console.log('支付成功--前端')
              wx.showModal({
                title: '充值成功',
                // content: '支付成功',
                showCancel: false,
                icon: 'none'
              })
              if(_this.data.arguments=='zfzf'){
                var pages = getCurrentPages();                       //获取当前页面
                var prePage = pages[pages.length - 2];               //获取上一页面
                prePage.setData({
                  'search.page': 1                                   //给上一页面的变量赋值
                })
                setTimeout(function(){
                  wx.navigateBack({                                    //返回上一页面
                    delta: 1,
                  })
                },2000)
              }
            } else if (res.errMsg != 'requestPayment:ok') {
              wx.showModal({
                title: '充值失败',
                // content: '支付成功',
                showCancel: false,
                icon: 'none'
              })
            }
          },
          fail(res) {
            console.log(res)
          }
        })
      })

   }
})
},
})