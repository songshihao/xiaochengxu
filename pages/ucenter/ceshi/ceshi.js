// pages/demo/demo.js
let City = require('../../../utils/allcity');
const QQMapWX = require('../../../lib/qqmap-wx-jssdk.min.js');
// 实例化API核心类
var qqmapsdk = new QQMapWX({
  key: 'GVOBZ-APK6P-BOZDT-V5ZWX-MRI7T-6GF5H' // 必填
});
Page({

  data: {
    city: [],
    config: {
      horizontal: true, // 第一个选项是否横排显示（一般第一个数据选项为 热门城市，常用城市之类 ，开启看需求）
      animation: true, // 过渡动画是否开启
      search: true, // 是否开启搜索
      searchHeight: 45, // 搜索条高度
      suctionTop: true, // 是否开启标题吸顶
    },
    rank:false,
    hotlelist:false
  },
  onLoad(options) {
    console.log(options)
    let that=this
    if(options.type){
      that.setData({
        rank:true,
      })
    }
    if(options.hotlelist==1){
      that.setData({
        hotlelist:true,
      })
    }
    wx.showLoading({
      title: '加载数据中...',
    })
    // 模拟服务器请求异步加载数据
    setTimeout(()=>{
      that.setData({
      city: City
    })
      wx.hideLoading()
    },1000)
  
  },
  bindtap(e) {
    var that=this
    var city=e.detail.name
    var rank=that.data.rank
    var hotlelist=that.data.hotlelist
    // that.getsuggest(city);
    if(rank){
      wx.navigateTo({
        url: '/pages/ranklsit/ranklsit?city=' + city,
      })
      return
    }else if(hotlelist){
      wx.setStorageSync('locatecity', city)
      wx.navigateTo({
        url: '/pages/hotellist/hotellist'
      })
      return
    }
    else {
        wx.setStorageSync('weizhi', city)
        setTimeout(function(){ 
          // wx.switchTab({
          //   url: '/pages/topic/topic?city='+ city ,
          // })
        },1000)
      }
  },
  //在Page({})中使用下列代码
//数据回填方法
backfill: function (e) {
  var id = e.currentTarget.id;
  for (var i = 0; i < this.data.suggestion.length;i++){
    if(i == id){
      this.setData({
        backfill: this.data.suggestion[i].title
      });
    }  
  }
},

//触发关键词输入提示事件
getsuggest: function(city) {
  var _this = this;
  //调用关键词提示接口
  qqmapsdk.getSuggestion({
    //获取输入框值并设置keyword参数
    keyword: city, //用户输入的关键词，可设置固定值,如keyword:'KFC'
    region:city, //设置城市名，限制关键词所示的地域范围，非必填参数
    region_fix:1,
    policy:1,
    page_size: 20,
    filter:'category=飞机场,火车站,长途汽车站,综合医院,地铁站,大学,旅游景点,文化场馆',
    success: function(res) {//搜索成功后的回调
      console.log(res);
      var sug = [];
      for (var i = 0; i < res.data.length; i++) {
        sug.push({ // 获取返回结果，放到sug数组中
          title: res.data[i].title,
          id: res.data[i].id,
          addr: res.data[i].address,
          city: res.data[i].city,
          district: res.data[i].district,
          latitude: res.data[i].location.lat,
          longitude: res.data[i].location.lng
        });
      }
      _this.setData({ //设置suggestion属性，将关键词搜索结果以列表形式展示
        suggestion: sug
      });
    },
    fail: function(error) {
      console.error(error);
    },
    complete: function(res) {
      console.log(res);
    }
  });
}
})