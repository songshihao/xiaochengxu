// pages/ucenter/membership/membership.js
const util = require('../../../utils/util.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    avatarUrl:'',
    nickName:'',
    type:[
      {id:0,name:'注册'},
      {id:1,name:'充值'},
      {id:2,name:'住宿'},
      {id:3,name:'购物'},
      {id:4,name:'置换'},
      {id:5,name:'加盟'},
      {id:6,name:'红包'},
      {id:7,name:'转账'},
    ],
    couponType:[
      {id:0,name:'酒店券'},
      {id:1,name:'红包券'},
      {id:2,name:'奖励券'},

    ],
  },
  copy: function(e) {
    util.copy(e.target.dataset.copy)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
    wx.getStorage({
      key: 'userInfo',
      success:function(res){
        console.log(res)
        that.setData({
          avatarUrl:res.data.avatarUrl,
          nickName:res.data.nickName
        })
       
      }
    })
    wx.request({
    url:app.globalData.url80+'income/list',
    method: "POST",
    header: {
      'content-type': "application/x-www-form-urlencoded",
      'X-Dts-Token': wx.getStorageSync('token')
    },
    data: {
      page:1,
      size:20

   },
   success:function(res){
     console.log(res)
     that.setData({
       list:res.data.data
     })
     console.log(res.data.data)
     console.log(that.data.list)
   },
   fali:function(res){
    console.log(res)
   }
  })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onLoad();
    setTimeout(function(){
      wx.stopPullDownRefresh({
        complete: (res) => {
          wx.showToast({
            title: '刷新成功',
            icon:'none',
          })
        },
      })
    },200)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})