var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
var time = require('../../../utils/time.js');


Page({
  data:{
    orderList: [],
    page: 1,
    loadmoreText: '正在加载更多数据',
    nomoreText: '全部加载完成',
    nomore: false,
    totalPages: 1,
    ordertime:''
    
  },
  onLoad:function(options){
    // 页面初始化 options为页面跳转所带来的参数
    // 页面显示

    wx.showLoading({
      title: '加载中...',
      success: function () {

      }
    });
    this.getOrderList();

  },

  /**
       * 页面上拉触底事件的处理函数
       */
  onReachBottom: function () {
    let that = this;
    var page=1
    page++;
    wx.showLoading({
      title: '加载中...',
    })
     util.request(api.chargeOrder, {page:page, size: 10},'POST').then(function (res) {
      console.log(res)
      wx.hideLoading()
      if (res.errno === 0) {
        var list =res.data.list
        if(list.length>0){
          that.setData({
            orderList: that.data.orderList.concat(res.data.list),
            page:page
          });
        }else{
          wx.showToast({
            title: '暂无更多',
            icon:'none'
          })
        }
      }

    });
  },

  getOrderList(){
    let that = this;

    if (that.data.totalPages <= that.data.page - 1) {
      that.setData({
        nomore: true
      })
      return;
    }
    console.log(that.data.nomore)
    util.request(api.chargeOrder, {page: 1, size: 10},'POST').then(function (res) {
      console.log(res)
      if (res.errno === 0) {
        // var index =
        that.setData({
          orderList: that.data.orderList.concat(res.data.list),
          page:res.data.nextPage,
          totalPages: res.data.pages
        });
        if(res.data.pages<=res.data.nextPage-1){
          that.setData({
            nomore: true
          })
        }
        wx.hideLoading();
      }

    });
  },
  payOrder(event){
      let that = this;
      let orderIndex = event.currentTarget.dataset.orderIndex;
      let order = that.data.orderList[orderIndex];
      wx.redirectTo({
          url: '/pages/pay/pay?orderId=' + order.id + '&actualPrice=' + order.actual_price,
      })
  },
  onReady:function(){
    // 页面渲染完成
  },
  onShow:function(){

  },
  onHide:function(){
    // 页面隐藏
  },
  onUnload:function(){
    // 页面关闭
  }
})