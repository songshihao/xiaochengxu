const QQMapWX = require('../../../lib/qqmap-wx-jssdk.min.js');
// 实例化API核心类
var qqmapsdk = new QQMapWX({
  key: 'GVOBZ-APK6P-BOZDT-V5ZWX-MRI7T-6GF5H' // 必填
});
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    region: [],
    qyopen: false, //点击筛选滑动弹窗显示效果，默认不显示
    qyshow: true, //用户点击闭关区域的弹窗设置，默认不显示
    nzshow: true,
    pxshow: true,
    isfull: false,
    cityleft: [], //获取的下拉框筛选项内容
    province: [],
    cityname: [],
    county: [],
    citycenter: '',
    select1: '',
    select2: '',
    select3: '',
    value: '',
    shownavindex: '',
    franchiseArea: '', //拟加盟地区
    contactPerson: '', //联系人
    contactNumber: '', //联系电话
    referrer: '', //推荐人
    referrerMobileNumber: '', //推荐人电话
    status: 1,
    code:[]

  },
  /**
   * 生命周期函数--监听页面加载
   */

  bindRegionChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value,
      code:e.detail.code
    })
    
  },
  contactPerson(e) {

    this.setData({
      contactPerson: e.detail.value
    })
  },
  contactNumber(e) {

    this.setData({
      contactNumber: e.detail.value
    })
  },
  referrer(e) {
    this.setData({
      referrer: e.detail.value
    })
  },
  referrerMobileNumber(e) {
    this.setData({
      referrerMobileNumber: e.detail.value
    })
  },
  onLoad: function (options) {
    var that = this;
    //调用获取城市列表接口
    qqmapsdk.getCityList({
      success: function (res) { //成功后的回调
        console.log(res.result)
        // console.log('省份数据：', res.result[0]); //打印省份数据
        // console.log('城市数据：', res.result[1]); //打印城市数据
        // console.log('区县数据：', res.result[2]); //打印区县数据
        that.setData({
          cityleft: res.result,
          province: res.result[0],
          cityname: res.result[1],
          county: res.result[2],
        });
      },
      fail: function (error) {
        console.error(error);
      },
      // complete: function(res) {
      //   console.log(res);
      // }
    });
    // this.setData({
    //   citycenter: this.data.cityleft['地铁'],
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  // 区域列表下拉框是否隐藏
  listqy: function (e) {
    if (this.data.qyopen) {
      this.setData({
        qyopen: false,
        nzopen: false,
        pxopen: false,
        nzshow: true,
        pxshow: true,
        qyshow: false,
        isfull: false,
        shownavindex: 0
      })
    } else {
      this.setData({
        qyopen: true,
        pxopen: false,
        nzopen: false,
        nzshow: true,
        pxshow: true,
        qyshow: false,
        isfull: true,
        shownavindex: e.currentTarget.dataset.nav
      })
    }

  },
  // 区域第一栏选择内容
  selectleft: function (e) {
    console.log(e.currentTarget.dataset.id)
    console.log(e.currentTarget.dataset);
    this.setData({
      cityright: {},
      citycenter: this.data.cityleft[e.currentTarget.dataset.city],
      select1: e.currentTarget.dataset.id,
      select2: '',
      'region[0]':e.currentTarget.dataset.name
    });
    console.log(this.data.region)
  },
  // 区域中间栏选择的内容
  selectcenter: function (e) {
    this.setData({
      //   cityright: this.data.citycenter[e.currentTarget.dataset.city],
      //   select2: e.target.dataset.city
      select2: e.currentTarget.dataset.id,
      'region[1]':e.currentTarget.dataset.name
    });
    console.log(this.data.region)
  },
  // 区域左边栏选择的内容
  selectright: function (e) {
    console.log(e.currentTarget.dataset.city);
    this.setData({
      select3: e.currentTarget.dataset.id,
      'region[2]':e.currentTarget.dataset.name
      //   select3: e.currentTarget.dataset.city
    });
    console.log(this.data.region)
  },
  // 点击灰色背景隐藏所有的筛选内容
  hidebg: function (e) {
    this.setData({
      qyopen: false,
      nzopen: false,
      pxopen: false,
      nzshow: true,
      pxshow: true,
      qyshow: true,
      isfull: false,
      shownavindex: 0,
    })
  },
  // 区域清空筛选项
  quyuEmpty: function () {
    this.setData({
      select1: '',
      select2: '',
      select3: '-1'
    })
  },
  // 区域选择筛选项后，点击提交
  submitFilter: function () {
    var data = this.data
    if (data.select3 != '') {
      this.setData({
        value: data.select3
      })
      this.refer1();
    } else if (data.select3 == '' && data.select2 != '') {
      this.setData({
        value: data.select2
      })
      this.refer1();
    } else if (data.select3 == '' && data.select2 == '') {
      this.setData({
        value: data.select1
      })
      this.refer1();
    }
    console.log(data.value)
    // console.log('选择的一级选项是：' + this.data.select1);
    // console.log('选择的二级选项是：' + this.data.select2);
    // console.log('选择的三级选项是：' + this.data.select3);
    // 隐藏地铁区域下拉框
    this.setData({
      qyopen: false,
      nzopen: false,
      pxopen: false,
      nzshow: true,
      pxshow: true,
      qyshow: false,
      isfull: false,
      shownavindex: 0
    })
  },
  refer1: function () {
    var that = this
    wx.request({
      url: app.globalData.url83 + '/api/partnerJoining/queryByArea',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data: {
        area: that.data.value,


      },
      success: function (res) {
        console.log(res)
        var data = res.data.data
        if (data.code == '0') {
          wx.showModal({
            title: '该地区可加盟',
            content: '点击确定申请加盟',
            success(res) {
              if (res.confirm) {
                console.log('用户点击确定')
                that.setData({
                  status: '0',
                })
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (data.code == '1') {
          wx.showModal({
            title: '该地区不可加盟',
            content: '请选择其他区域',
            success(res) {
              if (res.confirm) {
                console.log('用户点击确定')


              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function (res) {
        console.log(res)
      }
    })
    console.log(this.data.value)

  },
  refer: function (res) {
    var that = this
    wx.request({
      url: app.globalData.url83 + 'api/partnerJoining/save',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data: {
        franchiseArea: that.data.region, //拟加盟地区
        contactPerson: that.data.contactPerson, //联系人
        contactNumber: that.data.contactNumber, //联系电话
        referrer: that.data.referrer, //推荐人
        referrerMobileNumber: that.data.referrerMobileNumber //推荐人电话
      },
      success: function (res) {
        console.log(res)
        if (res.data.code == 0) {
          wx.showModal({
            title: '提交成功',
            content: '我们会尽快联系您',
          })
        }
      },
      fail: function (res) {
        console.log(res)
      }
    })
  },
  // 左边滑块滑动的值
  leftSchange: function (e) {
    console.log('左边改变的值为：' + e.detail.value);
    let currentValue = parseInt(e.detail.value);
    let currentPer = parseInt(currentValue)
    var that = this;
    that.setData({
      leftValue: e.detail.value //设置左边当前值
    })
  },
  // 右边滑块滑动的值
  rightSchange: function (e) {
    console.log('右边改变的值为：' + e.detail.value);
    let currentValue = parseInt(e.detail.value);
    var that = this;
    that.setData({
      rightValue: e.detail.value,
    })
  },
  // 价格筛选框重置内容
  PriceEmpty: function () {
    this.setData({
      leftValue: 1000, //左边滑块默认值
      rightValue: 6000, //右边滑块默认值
    })
  },
  // 价格筛选框提交内容
  submitPrice: function () {

    // 隐藏价格下拉框选项
    this.setData({
      nzopen: false,
      pxopen: false,
      qyopen: false,
      nzshow: false,
      pxshow: true,
      qyshow: true,
      isfull: false,
      shownavindex: 0
    })
  },
  // 排序内容下拉框筛选
  selectPX: function (e) {
    console.log('排序内容下拉框筛选的内容是' + e.currentTarget.dataset.index);
    this.setData({
      pxIndex: e.currentTarget.dataset.index,
      nzopen: false,
      pxopen: false,
      qyopen: false,
      nzshow: true,
      pxshow: false,
      qyshow: true,
      isfull: false,
      shownavindex: 0
    });
    console.log('当前' + this.data.pxIndex);
  },

})