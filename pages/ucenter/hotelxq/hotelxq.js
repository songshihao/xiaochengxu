// pages/ucenter/hotelxq/hotelxq.js
var back = require('../../../utils/back.js'); //引用外部的js文件
var utils =require('../../../utils/util')
var api = require('../../../config/api');
import qqmap from '../../../utils/map.js';
var app = getApp();
Page({
  data: {
    roomtype: [],
    roomdata:{},
    hotelName: '',
    hotelId: '',
    juli: '',
    add: '',
    name: '',
    img: '',
    img1:'',
    roomFacilitys: '',
    detail: [],
    mode: 'aspectFill',
    hotelPolicys: '',
    latitude: '',
    longitude: '',
    hotel_id: '',
    banner: [],
    hotellabel: '',
    imglist:'',
    checkintime:'',
    checkouttime:'',
    startdate:'',
    enddate:'',
    roomxq:0,
    openShare: false,
    canWrite: false, //用户是否获取了保存相册的权限
    hotelArounds:'',
    hotel_telephone:'',
    estimatelist:[],
    estimate:{},
    score:'',
    roomid:'',
    qq_success:0,
    showtab:0,
    dates:'',
    activity_theme:''
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
    var hotel=options.id
    that.setData({
      startdate:options.startDate,
      enddate:options.endDate,
      hotel_id:options.id,
    })
    var A = options.startDate
    var B = options.endDate
    var sdate = new Date(A);
    var now = new Date(B);
    var days = now.getTime() - sdate.getTime();
    var day = parseInt(days / (1000 * 60 * 60 * 24));
    if (day >= 0) {
      that.setData({
        dates: day,
      })
    }
    if(hotel!=''&&options.startdate!=''){
      that.hotel_xinxi(hotel);
    }
    wx.setNavigationBarTitle({title:options.hotelname + that.data.activity_theme}) //页面标题为路由参数 
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },
  hotel_xinxi(hotel){
    var that=this
    utils.request1(api.hotel_selectById,{
      id:hotel
    },'POST').then(res => {
    if(res.code==0){
      var data=res.data
      that.setData({
        juli: data.juli,
        add: data.address,
        location: data.location,
        hotelPolicys: data.hotelPolicys,
        hotel_id: data.id,
        hotellabel: data.hotelLabel,
        imglist:data.appearancePicUrl,
        checkintime:data.checkInTime,
        checkouttime:data.checkOutTime,
        hotelName: data.hotelName,
        hotelId: data.id,
        hotelArounds:data.hotelArounds,
        hotel_telephone:data.telephone,
        score:data.score
      })
      var location = data.location
      var a = location.split(",");
      that.setData({
        latitude: a[0],
        longitude: a[1]
      })
      var imglist=data.appearancePicUrl
        if(imglist!=null){
        var b = imglist.split(",");
        that.setData({
          img1: b[0]
        })
        }
      that.cha();
      that.estimate();
    }else{
      utils.showErrorToast('加载异常')
    }
  })
  },
  cha: function () {
    var that = this
    if (!app.globalData.hasLogin) {
      wx.navigateTo({
        url: "/pages/auth/login/login"
      });
    }
    wx.showLoading();
    wx.request({
      url: app.globalData.url83 + 'api/roomType/queryByAll',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data: {
        name: that.data.name,
        bedTypeId: '',
        breakfast: '',
        broadNet: '',
        addBedflag: '',
        hotelId: that.data.hotel_id,
        orderByClause: '',
        checkInTime: that.data.startdate
      },
      success: function (res) {
        wx.hideLoading();
        if(res.data.code==0){
          if (res.data.data[0]) {
            that.setData({
              roomtype: res.data.data,
              img: res.data.data[0].detailsImgUrl
            })
            var bannerlist = [];
            for (var i = 0; i < res.data.data.length; i++) {
              bannerlist.push(res.data.data[i].detailsImgUrl)
            }
            that.setData({
              banner: bannerlist,
            })
          }else{
            that.setData({
              qq_success:1
            })
          }
        }else{
          wx.showToast({
            title: '请求异常',
            icon:'none'
          })
        }
      },
      fail: function (res) {
        console.log(res)
      }
    })    
  },
  order:function (e) {
    var that=this
    this.setData({
      roomxq:0
    })
    var id = e.currentTarget.dataset.id
    var price = e.currentTarget.dataset.price
    var roomNub = e.currentTarget.dataset.roomnub
    var hotelName = that.data.hotelName
    var roomType = e.currentTarget.dataset.name
    var roomLabel=e.currentTarget.dataset.roomlabel
    var hotel_telephone=that.data.hotel_telephone
    var checkintime=that.data.startdate
    var checkouttime=that.data.enddate
    var dates=that.data.dates
    var hotelId = that.data.hotelId
    if (roomNub > 0) {    
      wx.navigateTo({
        url: '/pages/hotelorder/hotelorder?id=' + id + '&price=' + price + '&hotelName=' + hotelName + 
        '&hotel_id=' + hotelId + '&roomType=' + roomType + '&roomLabel=' + roomLabel+ '&hotel_telephone=' + hotel_telephone
       + '&checkouttime=' + checkouttime+ '&checkintime=' + checkintime+'&dates='+dates
      })
    } else if (roomNub <= 0 || roomNub == null) {
      wx.showModal({
        title: '您要预定的房型被订完了',
        confirmColor: '#e9332f ',
        content: '点击确定去查看其他房型',
        success: function (res) {
          console.log(res)
        }
      })
    }
  },
 
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that=this
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    let hotelId=that.data.hotel_id
    if(getDate){
      that.setData({
        startdate: getDate.checkInDate,
        enddate: getDate.checkOutDate,
      })
      var A =  getDate.checkInDate
      var B = getDate.checkOutDate
      var sdate = new Date(A);
      var now = new Date(B);
      var days = now.getTime() - sdate.getTime();
      var day = parseInt(days / (1000 * 60 * 60 * 24));
      // console.log('天数：'+day)
      if (day >= 0) {
        that.setData({
          dates: day,
        })
      }
      if(hotelId!=''){
        this.cha();
      }
    }else{
      //获取当前时间
      var TIME = utils.formatTime1(new Date());
      var TIME2 = utils.formatTime3(new Date());
      var startTime1 = TIME[0];
      var endTime1 = TIME2[0];
      that.setData({
        startdate: startTime1,
        enddate: endTime1,
      })
      var A = startTime1
      var B = endTime1
      var sdate = new Date(A);
      var now = new Date(B);
      var days = now.getTime() - sdate.getTime();
      var day = parseInt(days / (1000 * 60 * 60 * 24));
      
      if (day >= 0) {
        that.setData({
          dates: day,
        })
      }
      if(hotelId!=''){
        this.cha();
      }
    }
  },
    // 获取滚动条当前位置
  onPageScroll: function (e) {
    if (e.scrollTop > 100) {
      this.setData({
        showtab: 1
      });
    } else {
      this.setData({
        showtab: 0
      });
    }
  },
  //停止下拉刷新
  onPullDownRefresh: function () {
    // console.log(res)
    var that = this
    wx.stopPullDownRefresh({
      success: function (res) {
        console.log(res)
        that.onLoad()
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onLoad();
    setTimeout(function(){
      wx.stopPullDownRefresh({
        complete: (res) => {
          wx.showToast({
            title: '刷新成功',
            icon:'none',
          })
        },
      })
    },200)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
   // 页面分享
   onShareAppMessage: function() {
    // console.log(that.data.hotel_id)
    let that = this;
    let userInfo = wx.getStorageSync('userInfo');
    let shareUserId = 1;
    if (userInfo) {
      shareUserId = userInfo.userId;
    }
    return {
      title: that.data.hotelName,
      desc: '酒店分享',
      path: '/pages/ucenter/hotelxq/hotelxq?id=' + that.data.hotel_id + '&shareUserId=' + shareUserId + '&sharecs=1'
    }
  },
  onBackTap: function (e) {
    back.onBackTap() //调用
  },
  onBackHome:function(){
    wx.switchTab({
      url: '/pages/topic/topic',
    })
  },
  facility: function () {
    wx.navigateTo({
      url: '/pages/ucenter/facility/facility'
    })
  },
  intoMap: function () {
    var that = this
    var latitude1 = new Number(that.data.latitude)
    var longitude1 = new Number(that.data.longitude)
    wx.getLocation({
      type: 'wgs84', //返回可以用于wx.openLocation的经纬度
      success: function (res) { //因为这里得到的是你当前位置的经纬度
        console.log(res)
        // var latitude = res.latitude
        // var longitude = res.longitude
        wx.openLocation({ //所以这里会显示你当前的位置
          latitude: latitude1,
          longitude: longitude1,
          name: that.data.hotelName,
          address: that.data.add,
          scale: 28
        })
      }
    })
  },
  //导航
  nav: function () {
    var latitude = this.data.latitude
    var longitude = this.data.longitude
    console.log(latitude)
    console.log(longitude)
    //使用微信内置地图查看位置，地点的经纬度可用腾讯地图坐标拾取器获取（https://lbs.qq.com/tool/getpoint/）
    wx.getLocation({ //获取当前经纬度
      type: 'wgs84', //返回可以用于wx.openLocation的经纬度，官方提示bug: iOS 6.3.30 type 参数不生效，只会返回 wgs84 类型的坐标信息
      success: function (res) {
        wx.openLocation({ //?使用微信内置地图查看位置。
          latitude: 31.24917, //要去地点的纬度
          longitude: 121.36526, ///要去地点的经度-地址
          name: that.data.hotelName, //
          address: that.data.add
        })
      }
    })
  },
  //调用定位
  getLocate() {
    let that = this;
    new qqmap().getLocateInfo().then(function (val) { //这个方法在另一个文件里，下面有贴出代码
      console.log(val);
      // console.log(val.address);
      that.setData({
        address: val
      })
      if (val.indexOf('市') !== -1) { //这里是去掉“市”这个字
        console.log(val.indexOf('市') - 1);
        val = val.slice(0, val.indexOf('市'));
        console.log(val);
      }
      that.setData({
        locateCity: val
      });
      //把获取的定位和获取的时间放到本地存储
      wx.setStorageSync('locatecity', {
        city: val,
        time: new Date().getTime()
      });

    });
    // this.intoMap()
  },
vr:function(e){
  var vrurl=e.currentTarget.dataset.vrurl
  wx.navigateTo({
    url: '/pages/ucenter/hotelxq/vr/vr?vrurl='+vrurl,
  })
},
roomxq:function(q){
  var that=this
  var id=q.currentTarget.dataset.id
  var list = that.data.roomtype
  if(id!=''){
    that.setData({
      roomxq:1,
      roomid:id
    })
    for( var i=0;i<list.length;i++){
      var aa=list[i].id
      if(id==aa){
        that.setData({
          roomdata:list[i]
        })
      }
    }
  }
},
hide:function(){
  this.setData({
    roomxq:0
  })
},
closeShare: function() {
  this.setData({
    openShare: false,
  });
},
shareFriendOrCircle: function() {
  //var that = this;
  if (this.data.openShare === false) {
    this.setData({
      openShare: !this.data.openShare
    });
  } else {
    return false;
  }
},
handleSetting: function(e) {
  var that = this;
  // console.log(e)
  if (!e.detail.authSetting['scope.writePhotosAlbum']) {
      wx.showModal({
          title: '警告',
          content: '不授权无法保存',
          showCancel: false
      })
      that.setData({
          canWrite: false
      })
  } else {
      wx.showToast({
          title: '保存成功'
      })
      that.setData({
          canWrite: true
      })
  }
},

// 保存分享图
saveShare: function() {
let that = this;
wx.downloadFile({
  url: that.data.img,
  success: function(res) {
    console.log(res)
    wx.saveImageToPhotosAlbum({
      filePath: res.tempFilePath,
      success: function(res) {
        wx.showModal({
          title: '存图成功',
          content: '图片成功保存到相册了，可以分享到朋友圈了',
          showCancel: false,
          confirmText: '好的',
          confirmColor: '#a78845',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定');
            }
          }
        })
      },
      fail: function(res) {
        console.log('fail')
      }
    })
  },
  fail: function() {
    console.log('fail')
  }
})
},

estimate(){
  var that=this
  wx.request({
    url: api.estimateAll,
    method: "POST",
    header: {
      'content-type': "application/x-www-form-urlencoded",
      'X-Nideshop-Token': wx.getStorageSync('token')
    },
   data:{
    hotelId: that.data.hotel_id,
    hasPicture:'',
    orderCode:'',
    page:1,
    pageSize:20

   },
   success:function(res){
    console.log(res)
    if(res.data.code==0){
      that.setData({
        estimate:res.data.list[0],
        estimatelist:res.data.list,
      })
    }
   },
   fail:function(res){
    console.log(res)
   }

  })
},
estimateAll:function(){
  wx.navigateTo({
    url: '/pages/ucenter/hotelxq/estimateAll/estimateAll?hotelId=' + this.data.hotel_id,
  })
},
scrollTap: function(e) {
  this.setData({
    to_Id: e.target.dataset.hash
  })
},
checkInDate(){
  wx.navigateTo({
    url: '/pages/hotellist/pricedate/index',
  })
}
})