var util = require('../../../../utils/util');
var api = require('../../../../config/api.js');
var util = require('../../../../utils/back.js');      //引用外部的js文件

var app = getApp();
// import cfg from '../../utils/config.js';
// import util from '../../utils/util.js';

Page({
  /**
   * 页面的初始数据
   */
  data: {
    navTab: [
     {id:0,name:'全部'},
     {id:1,name:'有图'},
     {id:2,name:'好评'},
    //  {id:3,name:'已完成'},
    //  {id:5,name:'已取消'},
    ],        
    currentTab: 0,
    estimatelist:[],
    idx:0,
    page:1,
    token:'',
    hasPicture:'',
    hdatahieht:'',
    hdatatop:'',
    hotelId:'',
    star:'',
    itemimg:'',
    maskimg:false,
    mode1:'aspectFit',
    mode2:'aspectFill',
    touch: {
      distance: 0,
      scale: 1,
      baseWidth: null,
      baseHeight: null,
      scaleWidth: '300rpx',
      scaleHeight: '1000rpx'
    }
  },
  select: {
    page: 1,
    size: 6,
    isEnd: false,
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
    let hotelId=options.hotelId
    that.setData({
      hotelId:hotelId
    })
    var token = wx.getStorageSync('token');
    if (token == '') {
      wx.navigateTo({
        url: '/pages/auth/login/login',
      })
    }
    wx.getStorage({
      key: 'token',
      success:function(res) {
        that.setData({
          token:res.data
        })
      },
      fail:function(res){
        if(res.errMsg=="getStorage:fail data not found"){
             wx.navigateTo({
                url: '/pages/auth/login/login',
              })
        }
      }
    })
    var hdatahieht=wx.getStorageSync('hdatahieht')
    var hdatatop=wx.getStorageSync('hdatatop')
      that.setData({
        hdatahieht:hdatahieht,
        hdatatop:hdatatop
      });
    that.getData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getData();
    setTimeout(function(){
      wx.stopPullDownRefresh({
        complete: (res) => {
          wx.showToast({
            title: '刷新成功',
            icon:'none',
          })
        },
      })
    },200)
  },

  //拨号
  open: function () {
    wx.makePhoneCall({
      phoneNumber:this.data.dianhua, 
      success: function () {
        console.log("拨打电话成功！")
      },
      fail: function () {
        console.log("拨打电话失败！")
      }
});
},
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  currentTab: function (e) {
    if (this.data.currentTab == e.currentTarget.dataset.idx){
      return;
    }
    this.setData({
      currentTab: e.currentTarget.dataset.idx
    })
    this.getData();
  },
  getData:function(){
    var _this=this;
    if(_this.data.currentTab=='0'||_this.data.currentTab=='2'){
      wx.request({
        url: api.estimateAll,
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
      data:{
        hotelId:_this.data.hotelId,
        hasPicture:"",
        page:1,
        pageSize:20

      },
      success:function(res){
        if(res.data.code==0){
          if(_this.data.currentTab=='0'){
            _this.setData({
              estimatelist:res.data.list,
              star:'',
              hasPicture:false,
            })
          }else if(_this.data.currentTab=='2'){
            var li=res.data.list
            var list=[]
            for(var i=0;i<li.length;i++){
              var star=li[i].star
              if(star==5){
                list.push(li[i])
                _this.setData({
                  estimatelist:list,
                  star:5
                })
              }
            }
          } 
        }
      },
      fail:function(res){
        wx.hideLoading();
        wx.showToast({
          title: '请求失败',
          duration:1000
        })
      }
    })
    }else if(_this.data.currentTab=='1'){
      wx.request({
        url: api.estimateAll,
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
      data:{
        hotelId:_this.data.hotelId,
        hasPicture:true,
        page:1,
        pageSize:20

      },
      success:function(res){
        if(res.data.code==0){
          _this.setData({
            estimatelist:res.data.list,
            hasPicture:true,
            star:'',
          })
        }
      },
      fail:function(res){
        wx.hideLoading();
        wx.showToast({
          title: '请求失败',
          duration:1000
        })
      }
    })
    }
  
  },
  onBackTap: function(e) {
    util.onBackTap()          //调用
    // wx.switchTab({
    //   url: '/pages/dts_index/index',
    // })
  },
  
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var _this = this;
    // console.log(that)
    var list_ALL=_this.data.estimatelist
    var page = _this.data.page
    page++;
    _this.setData({
      page:page
    })
    // 显示加载图标
    wx.showLoading({
    title: '玩命加载中',
    })
    if(_this.data.currentTab=='0'){
      wx.request({
        url: api.estimateAll,
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
      data:{
        hotelId:_this.data.hotelId,
        hasPicture:"",
        page:page,
        pageSize:20

      },
      success:function(res){
        wx.hideLoading();
        if(res.data.code==0){
          var list=res.data.list
          if(list.length>0){
            var lists = list_ALL.concat(list) 
                if(_this.data.currentTab=='0'){
                  _this.setData({
                    estimatelist:lists,
                    star:'',
                    hasPicture:false,
                  })
                }else if(_this.data.currentTab=='2'){
                  var li=lists
                  var list=[]
                  for(var i=0;i<li.length;i++){
                    var star=li[i].star
                    if(star==5){
                      list.push(li[i])
                      _this.setData({
                        estimatelist:list,
                        star:5
                      })
                    }
                  } 
              }
          }else{
            wx.showToast({
              title: '暂无更多',
              icon:'none'
            })
          }
        }
      },
      fail:function(res){
        wx.hideLoading();
        wx.showToast({
          title: '请求失败',
          duration:1000
        })
      }
    })
    }else if(_this.data.currentTab=='1'){
      wx.request({
        url: api.estimateAll,
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
      data:{
        hotelId:_this.data.hotelId,
        hasPicture:true,
        page:page,
        pageSize:20

      },
      success:function(res){
        wx.hideLoading();
        if(res.data.code==0){
          var list=res.data.list
          if(list.length>0){
            var lists = list_ALL.concat(list) 
            _this.setData({
              estimatelist : lists,
              });
          }else{
            wx.showToast({
              title: '暂无更多',
              icon:'none'
            })
          }
        }
      },
      fail:function(res){
        wx.hideLoading();
        wx.showToast({
          title: '请求失败',
          duration:1000
        })
      }
    })
    }else if(_this.data.currentTab=='2'){
      wx.request({
        url: api.estimateAll,
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
      data:{
        hotelId:_this.data.hotelId,
        hasPicture:"",
        page:page,
        pageSize:20

      },
      success:function(res){
        wx.hideLoading();
        if(res.data.code==0){
          var list=res.data.list
          if(list.length>0){
            wx.hideLoading();
            
            var lists = list_ALL.concat(list) 
            _this.setData({
              estimatelist : lists,
              });
          }else{
            wx.hideLoading();
            wx.showToast({
              title: '暂无更多',
              icon:'none'
            })
          }
        }
      },
      fail:function(res){
        console.log(res)
      }
    })
    }
 },
 Reservation_again:function(e){
   console.log(e)
   var hotel_name=e.currentTarget.dataset.hotel_name
   wx.navigateTo({
    //  url: '/pages/ucenter/hotelxq/hotelxq?hotel_name='+'hotel_name',
    url: '/pages/hotellist/hotellist',
   })
 },
 details:function(e){
  var code=e.currentTarget.dataset.code
  // wx.navigateTo({
  //   url: '/pages/hotelorder/orderlist/hotelorderDetails/hotelorderDetails?code=' + code,
  // })
 },
 evaluate:function(e){
   console.log(e)
   var code=e.currentTarget.dataset.code
   wx.navigateTo({
    url: '/pages/hotelorder/orderlist/evaluate/evaluate?code=' + code,
  })
 },
 itemImg:function(e){
   var itemimg=e.currentTarget.dataset.itemimg
   this.setData({
    maskimg:true,
    itemimg:itemimg
   })
 },
 hide:function(){
  this.setData({
    maskimg:false,
   })
 },
 touchStartHandle(e) {
  // 单手指缩放开始，也不做任何处理 
  if (e.touches.length == 1) {
    console.log("单滑了")
    return
  }
  console.log('双手指触发开始')
  // 注意touchstartCallback 真正代码的开始 
  // 一开始我并没有这个回调函数，会出现缩小的时候有瞬间被放大过程的bug 
  // 当两根手指放上去的时候，就将distance 初始化。 
  let xMove = e.touches[1].clientX - e.touches[0].clientX;
  let yMove = e.touches[1].clientY - e.touches[0].clientY;
  let distance = Math.sqrt(xMove * xMove + yMove * yMove);
  this.setData({
    'touch.distance': distance,
  })
},
touchMoveHandle(e) {
  let touch = this.data.touch
  // 单手指缩放我们不做任何操作 
  if (e.touches.length == 1) {
    console.log("单滑了");
    return
  }
  console.log('双手指运动开始')
  let xMove = e.touches[1].clientX - e.touches[0].clientX;
  let yMove = e.touches[1].clientY - e.touches[0].clientY;
  // 新的 ditance 
  let distance = Math.sqrt(xMove * xMove + yMove * yMove);
  let distanceDiff = distance - touch.distance;
  let newScale = touch.scale + 0.005 * distanceDiff
  // 为了防止缩放得太大，所以scale需要限制，同理最小值也是 
  if (newScale >= 2) {
    newScale = 2
  }
  if (newScale <= 0.6) {
    newScale = 0.6
  }
  let scaleWidth = newScale * touch.baseWidth
  let scaleHeight = newScale * touch.baseHeight
  // 赋值 新的 => 旧的 
  this.setData({
    'touch.distance': distance,
    'touch.scale': newScale,
    'touch.scaleWidth': scaleWidth,
    'touch.scaleHeight': scaleHeight,
    'touch.diff': distanceDiff
  })
},
load: function (e) {
  console.log(e)
  // bindload 这个api是<image>组件的api类似<img>的onload属性 
  // this.setData({
  //   'touch.baseWidth': e.detail.width/2,
  //   'touch.baseHeight': e.detail.height/2.2,
  //   'touch.scaleWidth': e.detail.width/2,
  //   'touch.scaleHeight': e.detail.height/2.6
  // });
}
})