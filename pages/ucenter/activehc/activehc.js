// pages/vip/vip.js
const util = require('../../../utils/util.js');
const api = require('../../../config/api.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    passwod:'',
    passwod1:'123456',
    user:'',
    userLevelId:'',
    kqpassword1:''
  },
  kqinput(e){
    this.setData({
      id:e.detail.value
    })
  },
  user(e){
    this.setData({
      user:e.detail.value
    })
  },
  kqpassword(e){
    this.setData({
      passwod:e.detail.value
    })
  },
  kqpassword1(e){
    this.setData({
      kqpassword1:e.detail.value
    })
  },
  //公司激活
  btn:function(){
    var that = this
    if(that.data.passwod==that.data.passwod1){
      wx.showLoading();
    wx.request({
      url: app.globalData.url83 + 'api/rechargeCard/activation',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data: {
        cardNumber:that.data.id,
        employer:that.data.user,
      },
      success: function (res) {
        wx.hideLoading();
        if(res.data.code=='0'){
          wx.showModal({
            title:'开卡成功'
          })
        }else{
          wx.showModal({
            title:'开卡失败',
            content: res.data.errmsg
          })
        }
      },
      fail:function(res){
        console.log(res)
      }
    })   
    }else{
      wx.showModal({
        title:'密码错误',
        content: res.data.errmsg
      })
    }
  },
    //个人激活
    btn1:function(){
      var that = this
      wx.showLoading();
      wx.request({
        url: app.globalData.url83 +'api/rechargeCard/couponUse',
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
        data: {
          cardNumber:that.data.id,
          password:that.data.kqpassword1,
        },
        success: function (res) {
          wx.hideLoading();
          console.log(res)
          if(res.data.code=='0'){
            wx.showModal({
              title:'激活成功'
            })
          }else{
            wx.showModal({
              title:'激活失败',
              content: res.data.errmsg
            })
          }
        },
        fail:function(res){
          console.log(res)
        }
      })   
    
    },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var userLevelId=wx.getStorageSync('userLevelId')
    this.setData({
      userLevelId:userLevelId
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var userLevelId=wx.getStorageSync('userLevelId')
    this.setData({
      userLevelId:userLevelId
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})