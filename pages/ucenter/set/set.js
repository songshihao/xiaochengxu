// pages/ucenter/set/set.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username:'',
    nickName:''

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      var that=this
    wx.getStorage({
        key: 'userInfo',
        success:function(res){
            console.log(res.data.nickName)
            that.setData({
                nickName:res.data.nickName
              })
        }
      })
      that.setData({
        username:wx.getStorageSync('username')
      })
    
  },
  exitLogin: function () {
    wx.showModal({
        title: '',
        confirmColor: '#e9332f ',
        content: '退出登录？',
        success: function (res) {
            if (res.confirm) {
                wx.removeStorageSync('token');
                wx.removeStorageSync('token');
                wx.removeStorageSync('userInfo');
                wx.removeStorageSync('username');
                wx.switchTab({
                    url: '/pages/dts_index/index'
                });
            }
        }
    })

},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
 
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})