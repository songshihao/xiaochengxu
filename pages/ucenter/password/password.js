// pages/ucenter/password/password.js
var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
var app=getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    oldPayPass:'' ,  // 旧密码
    newPayPass:'' ,   // 新密码
    cursor:'',
    new:0,
    zf:'' ,
    C_password:0,
    C_password1:0 
  },
  oldPayPass(e){
    console.log(e)
    this.setData({
      oldPayPass:e.detail.value
    })
  },
  newPayPass(e){
    console.log(e)
    this.setData({
      newPayPass:e.detail.value,
      cursor:e.detail.cursor
    })
  },
  C_password:function(e){
    console.log(e)
    var that=this
    var type=e.currentTarget.dataset.password
    if(type==0){
      that.setData({
        C_password:1
      })
    }else if(type==1){
      that.setData({
        C_password:0
      })
    }
  
  },
  C_password1:function(e){
    console.log(e)
    var that=this
    var type=e.currentTarget.dataset.password
    if(type==0){
      that.setData({
        C_password1:1
      })
    }else if(type==1){
      that.setData({
        C_password1:0
      })
    }
  
  },
  btn:function(e){
    var _this=this
    var cursor=_this.data.cursor
    console.log(cursor)
    if(cursor==6){
      wx.request({
        url: app.globalData.url80+ 'user/setPassword',
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
           'X-Dts-Token': wx.getStorageSync('token')
        },
        data: {
          oldPassword:_this.data.oldPayPass,
          newPassword:_this.data.newPayPass,
        },
        success:function(res){
          console.log(res)
          if(res.data.errno==0){
          if(_this.data.oldPayPass!=''&&_this.data.newPassword!=''){
            wx.showModal({
              content:'修改成功',
              cancelColor: 'cancelColor',
              icon:'none'
            })
            var pages = getCurrentPages();                       //获取当前页面
            var prePage = pages[pages.length - 2];               //获取上一页面
            prePage.setData({
              'search.page': 1                                   //给上一页面的变量赋值
            })
            setTimeout(function(){
              wx.navigateBack({                                    //返回上一页面
                delta: 1,
              })
            },2000)
          }else if(_this.data.oldPayPass==''){
            wx.showModal({
              content:'密码设置成功',
              cancelColor: 'cancelColor',
              icon:'none'
            })
            var pages = getCurrentPages();                       //获取当前页面
            var prePage = pages[pages.length - 2];               //获取上一页面
            prePage.setData({
              'search.page': 1                                   //给上一页面的变量赋值
            })
            setTimeout(function(){
              wx.navigateBack({                                    //返回上一页面
                delta: 1,
              })
            },2000)
          }
        }else if(res.data.errno==501){
          wx.showModal({
            title: '登录失效',
            content: '点击确定重新登录',
            success (res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '/pages/auth/login/login',
              })
            } else if (res.cancel) {
              wx.navigateTo({
                url: '/pages/hotel/hotel',
              })
            }
           }
          })
          
        }else if(res.data.errmsg=="旧密码不匹配"){
          wx.showModal({
            title: '密码修改失败',
            content: '旧密码不匹配',
            success (res) {
              
           }
          })
        }else if(res.data.errno==-1){
          wx.showModal({
            title: "旧密码未输入",
            content: '设置过密码，必须输入旧密码',
            success (res) {
              
           }
          })
        }
        },
        fail:function(res){
          console.log(res)
          wx.showModal({
            content:'修改失败',
            cancelColor: 'cancelColor',
            icon:'none'
          })
        }
      })
    }else if(cursor!=6){
      wx.showModal({
        title: "格式错误",
        content:'密码必须是六位数',
        cancelColor: 'cancelColor',
        icon:'none'
      })
    }
   
  },
  btn1:function(){
    var _this=this
    wx.request({
    url: app.globalData.url80+ 'user/changePassword',
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
           'X-Dts-Token': wx.getStorageSync('token')
        },
        data: {
          oldPassword:_this.data.oldPayPass,
          newPassword:_this.data.newPayPass,
        },
        success:function(res){
          console.log(res)
          if(res.data.errno==0){
            wx.showModal({
              title: '修改成功！',
              content: '登录密码修改成功，请重新登录',
              success (res) {
              if (res.confirm) {
                wx.removeStorageSync('userInfo');
                wx.removeStorageSync('token');
                wx.removeStorageSync('token');
                wx.removeStorageSync('userLevelId');
                wx.removeStorageSync('userId');
                wx.removeStorageSync('username');
                wx.removeStorageSync('nickName');
                wx.removeStorageSync('password');
                wx.navigateTo({
                  url: '/pages/auth/login/login',
                })
              } else if (res.cancel) {
                wx.removeStorageSync('userInfo');
                wx.removeStorageSync('token');
                wx.removeStorageSync('token');
                wx.removeStorageSync('userLevelId');
                wx.removeStorageSync('userId');
                wx.removeStorageSync('username');
                wx.removeStorageSync('nickName');
                wx.removeStorageSync('password');
                wx.navigateTo({
                  url: '/pages/auth/login/login',
                })
              }
            }
            })
          }
        },
        fail:function(res){
          console.log(res)
        }
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var that=this
    if(options.zf==0){
      that.setData({
        zf:options.zf
      })
    }else{
      that.setData({
        zf:1
      })
    }
  
    if(options.newpayPass==1){
      that.setData({
        new:options.newpayPass
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})