const util = require('../../../utils/util.js');
const api = require('../../../config/api.js');
const WXBizDataCrypt = require('../../../utils/Node/WXBizDataCrypt');

var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isRuleTrue:0,
    isRuleTrue1:0,
    token:'',
    code:'',
    claimAmount:'',
    redAllCount:'',
    redCount:'',
    redEnvelopeToken:'',
    redPacUser:'',
    receiveInfo:[],
    list:[],
    greeting:'',
    Amount:''
  },
  onBackTap: function (e) {
    wx.reLaunch({
      url: '/pages/dts_index/index',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    console.log(options.encryption)
    var that=this
    wx.setStorageSync('username1', options.username);
    if(options.encryption!=''&&options.encryption!=undefined){
      wx.setStorage({
        data: options,
        key: 'haobao_options',
      })
      that.setData({
       greeting:options.greeting,
       redEnvelopeToken:options.encryption,
       isRuleTrue: 1,
       nickName:options.nickName,
       avatarUrl:options.avatarUrl,
       token:wx.getStorageSync('token')
      })
    }
      wx.getStorage({
        key: 'haobao_options',
        success:function(res){
          console.log(res)
          that.setData({
            greeting:res.data.greeting,
            redEnvelopeToken:res.data.encryption,
            isRuleTrue: 1,
            nickName:res.data.nickName,
            avatarUrl:res.data.avatarUrl
           })
        }
      })
  },
  bindGetUserInfo: function(e) {
    var that = this;
   var token=wx.getStorageSync('token')
    if(token!=''){
      that.lingqu();
    }else{
      wx.navigateTo({
              url: '/pages/auth/login/login?linghongbao=1',
            })
      // wx.showModal({
      //   title: '确定去登录',
      //   content: '您还没有登录或者登录登录失效了',
      //   success (res) {
      //   if (res.confirm) {
      //     wx.navigateTo({
      //       url: '/pages/auth/login/login?linghongbao=1',
      //     })
      //   } else if (res.cancel) {
      //     wx.showModal({
      //       title: '领取失败',
      //       content: '需要验证登录之后才可以领取',
      //       success (res) {
      //       if (res.confirm) {
      //         wx.navigateTo({
      //           url: '/pages/auth/login/login?linghongbao=1',
      //         })
      //       }else if (res.cancel) {
      //         wx.switchTab({
      //           url: '/pages/topic/topic',
      //         })
      //       }
      //     }
      //     })
      //   }
      // }
      // })
    }
  },
  lingqu:function (res) {
    var that=this
      this.animate('#container1', [
        { opacity: 1.0, rotateY: 0,  },
        { opacity: 1.0, rotateY: 360,  offset: 0.8},
        // { opacity: 1.0, rotateY: 360, backgroundColor: '#FF0000' },
        ], 800, function () {
          this.clearAnimation('#container1', { opacity: true, rotate: true }, function () {
          })
      }.bind(this))
    wx.request({
      // url:'http://192.168.0.243:8383/reservation/api/user/get/red_envelope',
      url: app.globalData.url83 + 'api/user/get/red_envelope',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data: {
        redEnvelopeToken:that.data.redEnvelopeToken,  
      },
      success:function (res) {
        // console.log(res)
        if(res.data.code==0){
          setTimeout(function () {
         if(res.data.mark==2){
              wx.showToast({
                title: '您已经领取过红包了',
                icon:"none"
              },200)
              that.setData({
               isRuleTrue: 0,
             })    
           } 
           else if(res.data.mark==1){
             wx.showToast({
               title: '领取失败',
               icon:"none"
              },200)
             that.setData({
              isRuleTrue: 0,
            })
          } else if(res.data.mark!=0&&res.data.redCount==0){
            wx.showToast({
              title: '您点慢了，红包被抢完了！',
              icon:"none"
            },200)
          }
            that.setData({
              isRuleTrue: 0,
              isRuleTrue1:1,
              Amount:res.data.claimAmount,
              // redAllCount:res.data.redAllCount,
              // redCount:res.data.redCount,
              // redPacUser:res.data.redPacUser,
              list:res.data,
              receiveInfo:res.data.receiveInfo
            })
          },800)
        }else if(res.data.errno==408){
          that.setData({
            isRuleTrue: 0,
          })
          util.showErrorToast('金额错误')
          setTimeout(function(){
            wx.switchTab({
              url: '/pages/topic/topic',
            })
          },2000)
        }else if(res.data.errno==409){
          that.setData({
            isRuleTrue: 0,
          })
          util.showErrorToast('红包失效')
          setTimeout(function(){
            wx.switchTab({
              url: '/pages/topic/topic',
            })
          },2000)
        }else if(res.data.errno==401){
          wx.showModal({
            title: '登录失效',
            content: '长时间未登录需重新登录',
            success (res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '/pages/auth/login/login?linghongbao=1',
              })
            } else if (res.cancel) {
              wx.showModal({
                title: '领取失败',
                content: '需要验证登录之后才可以领取',
                success (res) {
                if (res.confirm) {
                  wx.navigateTo({
                    url: '/pages/auth/login/login?linghongbao=1',
                  })
                }else if (res.cancel) {
                  wx.switchTab({
                    url: '/pages/topic/topic',
                  })
                }
              }
              })
            }
          }
          })
        }
      },
      fail:function (res){
        console.log(res)
      }
    })
  },
    //打开规则提示
  showRule: function () {
      this.setData({
        isRuleTrue: 1
      })
  },
  //关闭规则提示
  hideRule: function () {
    this.setData({
      isRuleTrue: 0
    })
    wx.reLaunch({
      url: '/pages/dts_index/index',
    })
  },
  lookmaney:function(){
    wx.navigateTo({
      url: '/pages/ucenter/balance/balance',
    })
    wx.removeStorageSync('haobao_options')
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that=this
    wx.getStorage({
      key: 'haobao_options',
      success:function(res){
        that.setData({
          greeting:res.data.greeting,
          redEnvelopeToken:res.data.encryption,
          isRuleTrue: 1,
          nickName:res.data.nickName,
          avatarUrl:res.data.avatarUrl
         })
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  onUnload:function(){
    this.setData({
      isRuleTrue: 0
    })
  }
})