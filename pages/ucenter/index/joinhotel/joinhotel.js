var util = require('../../../../utils/util.js');
var api = require('../../../../config/api');
var app = getApp();
Page({
  data: {
    hotelname: '',
    hoteladdress: '',
    ota: '',
    roomnum: '',
    roomarea: '',
    price: '',
    phone: '',
    phone1: '',
    referrer: '',
    user: '',
    full_region: '',
    address: {
      id: 0,
      province_id: 0,
      city_id: 0,
      district_id: 0,
      address: '',
      full_region: '',
      userName: '',
      telNumber: '',
      is_default: 0
    },
    addressId: 0,
    openSelectRegion: false,
    provinceName: '',
    isDefault: 0,
    provinceName: '',
    cityName: '',
    areaName: '',
    region: ['上海市', '上海市', '嘉定区'],
    regionType: 1,
    regionList: [],
    selectRegionDone: false
  },
  bindRegionChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
       hoteladdress: e.detail.value,
        region:e.detail.value
    })
  },
  hotelname(e) {
     console.log(e)
    this.setData({
      hotelname: e.detail.value
    })
  },
  //   hoteladdress(e){
  //     // console.log(e.detail.value)
  //     this.setData({
  //       hoteladdress:e.detail.value
  //     })
  //   },
  ota(e) {
    // console.log(e.detail.value)
    this.setData({
      ota: e.detail.value
    })
  },
  roomnum(e) {
    // console.log(e.detail.value)
    this.setData({
      roomnum: e.detail.value
    })
  },
  roomarea(e) {
    // console.log(e.detail.value)
    this.setData({
      roomarea: e.detail.value
    })
  },
  price(e) {
    // console.log(e.detail.value)
    this.setData({
      price: e.detail.value
    })
  },
  phone(e) {
    // console.log(e.detail.value)
    this.setData({
      phone: e.detail.value
    })
  },
  phone1(e) {
    // console.log(e.detail.value)
    this.setData({
      phone1: e.detail.value
    })
  },
  referrer(e) {
    // console.log(e.detail.value)
    this.setData({
      referrer: e.detail.value
    })
  },
  user(e) {
    // console.log(e.detail.value)
    this.setData({
      user: e.detail.value
    })
  },
  bindinputMobile(event) {
    let address = this.data.address;
    address.telNumber = event.detail.value;
    this.setData({
      address: address
    });
  },
  bindinputName(event) {
    let address = this.data.address;
    address.userName = event.detail.value;
    this.setData({
      address: address
    });
  },
  bindinputAddress(event) {
    let address = this.data.address;
    address.detailInfo = event.detail.value;
    this.setData({
      address: address
    });
    console.log(address)
  },
  bindIsDefault() {
    let address = this.data.address;
    address.is_default = !address.is_default;
    this.setData({
      address: address
    });
  },
  getAddressDetail() {
    let that = this;
    // util.request(api.AddressDetail, { id: that.data.addressId }).then(function (res) {
    //   if (res.errno === 0) {
    //     if(res.data){
    //         that.setData({
    //             address: res.data
    //         });
    //     }
    //   }
    // });
  },
  setRegionDoneStatus() {
    let that = this;
    let doneStatus = that.data.selectRegionList.every(item => {
      return item.id != 0;
    });

    that.setData({
      selectRegionDone: doneStatus
    })

  },
  chooseRegion() {
    let that = this;
    this.setData({
      openSelectRegion: !this.data.openSelectRegion
    });

    //设置区域选择数据
    let address = this.data.address;
    if (address.provinceId > 0 && address.cityId > 0 && address.areaId > 0) {
      let selectRegionList = this.data.selectRegionList;
      selectRegionList[0].id = address.provinceId;
      selectRegionList[0].name = address.provinceName;
      selectRegionList[0].pid = 0;

      selectRegionList[1].id = address.cityId;
      selectRegionList[1].name = address.cityName;
      selectRegionList[1].pid = address.provinceId;

      selectRegionList[2].id = address.areaId;
      selectRegionList[2].name = address.areaName;
      selectRegionList[2].pid = address.cityId;

      this.setData({
        selectRegionList: selectRegionList,
        regionType: 3
      });

      this.getRegionList(address.cityId);
    } else {
      this.setData({
        selectRegionList: [{
            id: 0,
            name: '省份',
            pid: 0,
            type: 1
          },
          {
            id: 0,
            name: '城市',
            pid: 0,
            type: 2
          },
          {
            id: 0,
            name: '区县',
            pid: 0,
            type: 3
          }
        ],
        regionType: 1
      })
      this.getRegionList(0);
    }

    this.setRegionDoneStatus();

  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    // if (options.id != '' && options.id != 0) {
    //   this.setData({
    //     addressId: options.id
    //   });
    //   this.getAddressDetail();
    // }

    this.getRegionList(1);

  },
  onReady: function () {

  },
  selectRegionType(event) {
    let that = this;
    let regionTypeIndex = event.target.dataset.regionTypeIndex;
    let selectRegionList = that.data.selectRegionList;

    //判断是否可点击
    if (regionTypeIndex + 1 == this.data.regionType || (regionTypeIndex - 1 >= 0 && selectRegionList[regionTypeIndex - 1].id <= 0)) {
      return false;
    }

    this.setData({
      regionType: regionTypeIndex + 1
    })

    let selectRegionItem = selectRegionList[regionTypeIndex];

    this.getRegionList(selectRegionItem.parent_id);

    this.setRegionDoneStatus();

  },
  selectRegion(event) {
    let that = this;
    let regionIndex = event.target.dataset.regionIndex;
    let regionItem = this.data.regionList[regionIndex];
    let regionType = regionItem.type;
    let selectRegionList = this.data.selectRegionList;
    selectRegionList[regionType - 1] = regionItem;


    if (regionType != 3) {
      this.setData({
        selectRegionList: selectRegionList,
        regionType: regionType + 1
      })
      this.getRegionList(regionItem.id);
    } else {
      this.setData({
        selectRegionList: selectRegionList
      })
    }

    //重置下级区域为空
    selectRegionList.map((item, index) => {
      if (index > regionType - 1) {
        item.id = 0;
        item.name = index == 1 ? '城市' : '区县';
        item.parent_id = 0;
      }
      return item;
    });

    this.setData({
      selectRegionList: selectRegionList
    })


    that.setData({
      regionList: that.data.regionList.map(item => {

        //标记已选择的
        if (that.data.regionType == item.type && that.data.selectRegionList[that.data.regionType - 1].id == item.id) {
          item.selected = true;
        } else {
          item.selected = false;
        }

        return item;
      })
    });

    this.setRegionDoneStatus();

  },
  doneSelectRegion() {
    if (this.data.selectRegionDone === false) {
      return false;
    }

    let address = this.data.address;
    let selectRegionList = this.data.selectRegionList;
    address.provinceId = selectRegionList[0].id;
    address.cityId = selectRegionList[1].id;
    address.areaId = selectRegionList[2].id;
    address.provinceName = selectRegionList[0].name;
    address.cityName = selectRegionList[1].name;
    address.areaName = selectRegionList[2].name;

    this.setData({
      address: address,
      openSelectRegion: false
    });

  },
  cancelSelectRegion() {
    this.setData({
      openSelectRegion: false,
      regionType: this.data.regionDoneStatus ? 3 : 1
    });

  },
  getRegionList(regionId) {
    let that = this;
    let regionType = that.data.regionType;
    util.request(api.RegionList, {
      parentId: regionId
    }).then(function (res) {
      if (res.errno === 0) {
        that.setData({
          regionList: res.data.map(item => {

            //标记已选择的
            if (regionType == item.type && that.data.selectRegionList[regionType - 1].id == item.id) {
              item.selected = true;
            } else {
              item.selected = false;
            }

            return item;
          })
        });
      }
    });
  },
  cancelAddress() {
    wx.navigateBack({
      url: '/pages/shopping/address/address',
    })
  },
  saveAddress() {
    let address = this.data;
    let addresslist = this.data.address;
    // var hotelAddress=addresslist.full_region + addresslist.detailInfo
    this.setData({
      hoteladdress: addresslist.full_region + addresslist.detailInfo
    })
    console.log(address.hoteladdress)

    if (address.hotelname == '') {
      util.showErrorToast('请输入酒店名');

      return false;
    }
    if (address.ota == '') {
      util.showErrorToast('请输入ota评分');

      return false;
    }

    if (address.roomnum == '') {
      util.showErrorToast('请输入房间数量');

      return false;
    }

    if (address.roomarea == '') {
      util.showErrorToast('请输入房间面积');

      return false;
    }
    if (address.price == '') {
      util.showErrorToast('请输入客房平均单价');

      return false;
    }



    if (address.user == '') {
      util.showErrorToast('请输入联系人');

      return false;
    }
    if (address.phone == '') {
      util.showErrorToast('请输入手机号码');
      return false;
    }
    // if (address.referrer == '') {
    //   util.showErrorToast('请输入推荐人（选填）');

    //   return false;
    // }

    // if (address.phone1 == '') {
    //   util.showErrorToast('推荐人手机号码（选填）');
    //   return false;
    // }

    if (address.district_id == 0) {
      util.showErrorToast('请选择地区');
      return false;
    }

    if (address.detailInfo == '') {
      util.showErrorToast('请输入详细地址');
      return false;
    }

    if (address.ota < 4.3 || address.roomnum < 30 || address.roomarea < 15 || address.price < 150) {
      // util.showErrorToast('对不起，您的酒店不符合我们的要求');
      wx.showToast({
        title: '对不起，您的酒店不符合我们的要求',
        icon:'none'
      })
      return false;
    }

    let that = this;
    // var urlll='http://192.168.0.28:8383/reservation/'
    wx.request({


      // url:urlll + 'api/hotelFranchise/save',
      url: app.globalData.url83 + 'api/hotelFranchise/save',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        // 'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data: {
        hotelName: address.hotelname, //酒店名
        hotelAddress: address.hoteladdress, //地址
        roomNub: address.roomnum, //房间数量
        averageArea: address.roomarea, //面积
        otaScore: address.ota, //评分
        contactNumber: address.phone, //电话
        averageHotelPrice: address.price, //平均单价,
        contactPerson: address.user, //联系人
        referrer: address.referrer, //推荐人
        referrerMobileNumber: address.phone1 //推荐人电话
      },
      success: function (res) {
        console.log(res)
        if (res.data.code == 0) {
          wx.showModal({
            title: '提交成功',
            content: '我们会尽快联系您'
          })
        }
      },
      fail: function (res) {

      }
    })

  },
  onShow: function () {
    // 页面显示

  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭

  }
})