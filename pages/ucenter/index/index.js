var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
var user = require('../../../services/user.js');

var app = getApp();

Page({
    data: {
        userInfo: {},
        hasMobile: '',
        username1:'',
        item:0,
        userLevelId:'',
        user_level:{0:'会员',1:'VIP会员',2:'普通合伙人',3:'区县合伙人',4:'城市运营中心',5:'城市合伙人',6:'平台供应商',7:'平台酒店供应商',8:'平台管理员'},
        avatar:''
      
    },
    onLoad: function (options) {
  
        // 页面初始化 options为页面跳转所带来的参数
        var username1=wx.getStorageSync('username');
        // var token=wx.getStorageSync('token');
        console.log( this.data.userLevelId)
        // this.setData({
        //     userLevelId:userLevelId
        // })
        wx.getStorage({
          key: 'token',
          // username : 'username',
          success:function(res){
            console.log(res)
          },
          fail:function(res){
            console.log(res)
            if(res.errMsg=="getStorage:fail data not found"){
                 wx.navigateTo({
                    url: '/pages/auth/login/login',
                  })
            }
          }
        })
        // if(token==''){
        //   wx.navigateTo({
        //     url: '/pages/auth/login/login',
        //   })
        // }else if(token!=''){
         
        // }
      
    },
    copy: function(e) {
      util.copy(e.target.dataset.copy)
    },
    onReady: function () {

    },
    onShow: function () {

        let userInfo = wx.getStorageSync('userInfo');
        let token = wx.getStorageSync('token');
        var username1=wx.getStorageSync('username');
        var userLevelId=wx.getStorageSync('userLevelId');

        // 页面显示
        if (userInfo && token) {
            app.globalData.userInfo = userInfo;
            app.globalData.token = token;
        }

        this.setData({
            userInfo: app.globalData.userInfo,
            username1: username1,
            userLevelId:userLevelId
        });

    },
    onHide: function () {
        // 页面隐藏

    },
    onUnload: function () {
        // 页面关闭
    },
    bindGetUserInfo(e) {
      let userInfo = wx.getStorageSync('userInfo');
      let token = wx.getStorageSync('token');
      if (userInfo && token) {
        return;
      }
        if (e.detail.userInfo){
            //用户按了允许授权按钮
            user.loginByWeixin(e.detail).then(res => {
                this.getPhoneNumber()
                this.setData({
                    userInfo: res.data.userInfo
                });
                app.globalData.userInfo = res.data.userInfo;
                app.globalData.token = res.data.token;
            }).catch((err) => {
                console.log(err)
            });
        } else {
            //用户按了拒绝按钮
            wx.showModal({
                title: '警告通知',
                content: '您点击了拒绝授权,将无法正常显示个人信息,点击确定重新获取授权。',
                success: function (res) {
                    if (res.confirm) {
                        wx.openSetting({
                            success: (res) => {
                                if (res.authSetting["scope.userInfo"]) {////如果用户重新同意了授权登录
                                    user.loginByWeixin(e.detail).then(res => {
                                        this.setData({
                                            userInfo: res.data.userInfo
                                        });
                                        app.globalData.userInfo = res.data.userInfo;
                                        app.globalData.token = res.data.token;
                                    }).catch((err) => {
                                        console.log(err)
                                    });
                                }
                            }
                        })
                    }
                }
            });
        }
    },
    exitLogin: function () {
        wx.showModal({
            title: '',
            confirmColor: '#e9332f ',
            content: '退出登录？',
            success: function (res) {
                if (res.confirm) {
                    wx.removeStorageSync('token');
                    wx.removeStorageSync('userInfo');
                    wx.switchTab({
                        url: '/pages/topic/topic'
                    });
                }
            }
        })

    },
    getPhoneNumber (e) {
      console.log(e)
        console.log(e.detail.errMsg)
        console.log(e.detail.iv)
        console.log(e.detail.encryptedData)
      },
   

    vip:function(){
        wx.navigateTo({
          url: '/pages/vip/vip',
        })
    },
    set:function(){
        wx.navigateTo({
          url: '/pages/ucenter/set/set',
        })
    },
    allorder:function(){
        wx.navigateTo({
          url: '/pages/hotelorder/orderlist/orderlist',
        })
    },
    //分享
      //成为会员
      membership:function(){
          wx.navigateTo({
            url: 'url',
          })
      },
      myteam:function(){
          wx.navigateTo({
            url: '/pages/ucenter/myteam/myteam',
          })
      },
      onShareAppMessage: function (res) {
        console.log(res)
        console.log(wx.getStorageSync('username'))
        var nickName=''
        wx.getStorage({
          key: 'userInfo',
          success:function(res){
            console.log(res)
            nickName=res.data.nickName
          }
        })
       
        if (res.from === 'button') {
          // 通过按钮触发
          var data = res.target.dataset
          return {
            title: '家畅——人人都可以免费住酒店点击领取100元全额抵用红包券！',
            desc: '家畅智能酒店置换商城平台通过置换酒店空置房，来帮助酒店免费升级为智慧酒店，并免费提供酒店所需的物资和服务，致力于打造中国最大的智慧酒店',
           path: '/pages/topic/topic?username=' + wx.getStorageSync('username') + '&nickName=' + wx.getStorageSync('nickName'),
            imageUrl: 'https://jiachang8.com/mpimage/share_img4.jpg',
            success: function (res) {
              // 转发成功
              console.log('转发成功')
            },
            fail: function (res) {
              // 转发失败
              console.log('转发失败')
            }
          }
        }
        //通过右上角菜单触发
        return {
          title: '家畅——人人都可以免费住酒店点击领取100元全额抵用红包券！',
          desc: '家畅智能酒店置换商城平台通过置换酒店空置房，来帮助酒店免费升级为智慧酒店，并免费提供酒店所需的物资和服务，致力于打造中国最大的智慧酒店',
          // path: "/pages/researches/index",
         path: '/pages/topic/topic?username=' + wx.getStorageSync('username') + '&nickName=' + wx.getStorageSync('nickName'),
          imageUrl: 'https://jiachang8.com/mpimage/share_img4.jpg',//fen
        };
      },
      subscribeMessage:function(){
        
        // let str='t4nvJyQlvSdfCI0xgjLTnM2xjI6O_aAiL5cLiFQ8Uhs',
        //     str1='X9A3BcDyeWZy_O7nO_d5pA--9SWXGtXFfgHNfGehv6w'
            
        wx.requestSubscribeMessage({
          tmplIds:['t4nvJyQlvSdfCI0xgjLTnM2xjI6O_aAiL5cLiFQ8Uhs', 'X9A3BcDyeWZy_O7nO_d5pA--9SWXGtXFfgHNfGehv6w'],
          success (res) {
              console.log('123456')
              console.log(res)
              wx.navigateTo({
                url: '/pages/ucenter/vipshouyi/vipshouyi',
              })
           }
        })
      }
})