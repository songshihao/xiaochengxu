const QQMapWX = require('../../../../lib/qqmap-wx-jssdk.min.js');
// 实例化API核心类
var qqmapsdk = new QQMapWX({
    key: 'GVOBZ-APK6P-BOZDT-V5ZWX-MRI7T-6GF5H' // 必填
});
 const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    province:[],
    cityname:[],
    county:[],
    province_val:'',
    cityname_val:'',
    county_val:'',
    value:'',
    selectList: [
      {
          name: '省级行政区',
      },
      {
          name: '市级行政区',
      },
      {
          name: '区县行政区',
      }
  ],
  showIndex: -1,
  status:'status1',
  status1:'',
  idxr:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var that = this;
    //调用获取城市列表接口
    qqmapsdk.getCityList({
      success: function(res) {//成功后的回调
        // console.log('省份数据：', res.result[0]); //打印省份数据
        // console.log('城市数据：', res.result[1]); //打印城市数据
        // console.log('区县数据：', res.result[2]); //打印区县数据
          that.setData({
            list:res.result,
            province:res.result[0],
            cityname:res.result[1],
            county:res.result[2],
          });
      },
      fail: function(error) {
        console.error(error);
      },
      // complete: function(res) {
      //   console.log(res);
      // }
    });
    that.list();
  },
  list:function(){
    var that=this
  },
     // 选中select_tab
     chooseTab(e){
      let index = e.currentTarget.dataset.id;
      if(index !== this.data.showIndex){
          this.setData({
              showIndex: index
          })
      }else{
          // 再次点击应该收起
          this.setData({
              showIndex: -1
          })
      }
  },
  // 选中选项
  chooseOption(e){
      console.log(e.currentTarget.dataset.id)
      let val = e.currentTarget.dataset.value,
          idx = e.currentTarget.dataset.index;
       console.log(idx)
      this.setData({
          [`selectList[${idx}].active`]: val,
          showIndex: -1,
          province_val:e.currentTarget.dataset.id,
          idxr:idx
      });
  },
  chooseOption1(e){
    let val = e.currentTarget.dataset.value,
        idx = e.currentTarget.dataset.index;
        console.log(idx)
      console.log(val)
    this.setData({
        [`selectList[${idx}].active`]: val,
        showIndex: -1,
        cityname_val:e.currentTarget.dataset.id,
        idxr:idx
    });
},
chooseOption2(e){
  let val = e.currentTarget.dataset.value,
      idx = e.currentTarget.dataset.index;
      console.log(idx)
  this.setData({
      [`selectList[${idx}].active`]: val,
      showIndex: -1,
      county_val:e.currentTarget.dataset.id,
      idxr:idx
  });
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  refer:function(){
 
    var data=this.data
    if(data.county_val!=''){
      this.setData({
        value:data.county_val
      })
      this.refer1();
    }else if(data.county_val==''&&data.cityname_val){
      this.setData({
        value:data.cityname_val
      })
      this.refer1();
    }else if(data.county_val==''&&data.cityname_val==''){
      this.setData({
        value:data.province_val
      })
      this.refer1();
    }
  },
  refer1:function(){
    var that=this
    wx.request({
      url:app.globalData.url83 + '/api/partnerJoining/queryByArea',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data:{
        area:that.data.value
      },
      success:function(res){
        console.log(res)
        var data=res.data.data
        if(data.code=='0'){
          wx.showModal({
              title: '该地区可加盟',
              // content: '点击确定申请加盟',
              success (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
        }else if(data.code=='1'){
          wx.showModal({
            title: '该地区不可加盟',
            // content: '点击确定申请加盟',
            success (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              
                // that.setData({
                //   status:'status',
                //   status1:'status1'
                // })
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail:function(res){
        console.log(res)
      }
    }) 
    console.log(this.data.value)
    
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var _this = this;
    //调用获取城市列表接口
    qqmapsdk.getCityList({
      success: function(res) {//成功后的回调
        // console.log(res);
        // console.log('省份数据：', res.result[0]); //打印省份数据
        // console.log('城市数据：', res.result[1]); //打印城市数据
        // console.log('区县数据：', res.result[2]); //打印区县数据
      },
      fail: function(error) {
        console.error(error);
      },
      complete: function(res) {
        console.log(res);
      }
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})