var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
var app = getApp();

Page({
  data: {
    addressList: [],
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
    this.getAddressList();
  },
  getAddressList (){
    let that = this;
    util.request(api.AddressList).then(function (res) {
      console.log(res)
      if (res.errno === 0) {
        that.setData({
          addressList: res.data
        });
      }
    });
  },
  addressAddOrUpdate1 (event) {
    console.log(event)
    // wx.navigateTo({
    //   url: '/pages/goods/goods?addressId=' + event.currentTarget.dataset.addressId
    //   // url: '/pages/goods/goods?addressId=' + event.currentTarget.dataset.addressId
    // })
    var addressId=event.currentTarget.dataset.addressId
    wx.setStorage({
      data: addressId,
      key: 'addressId',
    })
    var pages = getCurrentPages();                       //获取当前页面
    var prePage = pages[pages.length - 2] ;               //获取上一页面
    wx.navigateBack({                                    //返回上一页面
      delta: 1,
    })
 

  },
  addressAddOrUpdate(event){
    console.log(event)
    wx.navigateTo({
      url: '/pages/ucenter/addressAdd/addressAdd?id=' + event.currentTarget.dataset.addressId
    })
  },
  bianji(event){
    console.log(event)
    wx.navigateTo({
      url: '/pages/ucenter/addressAdd/addressAdd?id=' + event.currentTarget.dataset.addressId
    })
  },
  deleteAddress(event){
    let that = this;
    wx.showModal({
      title: '',
      content: '确定要删除地址？',
      success: function (res) {
        if (res.confirm) {
          let addressId = event.target.dataset.addressId;
          util.request(api.AddressDelete, { id: addressId },'POST', 'application/json').then(function (res) {
            if (res.errno === 0) {
              that.getAddressList();
            }
          });
        }
      }
    })
    return false;
    
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onLoad();
    setTimeout(function(){
      wx.stopPullDownRefresh({
        complete: (res) => {
          wx.showToast({
            title: '刷新成功',
            icon:'none',
          })
        },
      })
    },200)
  },
})