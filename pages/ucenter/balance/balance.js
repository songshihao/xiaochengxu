// pages/ucenter/balance/balance.js
const util = require('../../../utils/util');
const api = require('../../../config/api');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    token:'',
    roomVoucherNub:'',
    red_envelopes:'',
    incentive_voucher:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  qingqiu:function(){
    var _this=this
    util.request(api.checkBalances, null, 'POST').then(function(res) {
      wx.hideLoading();
      _this.setData({
          hotel_voucher:res.data.hotelVoucher,
          red_envelopes:res.data.redEnvelopes,
          incentive_voucher:res.data.incentiveVoucher
        })
    })
  },
  incentive_voucher:function(){
    wx.navigateTo({
      url: '/pages/ucenter/vipshouyi/vipshouyi',
    })
  },
  hotel_voucher:function(){
    wx.navigateTo({
      url: '/pages/ucenter/jy_record/jy_record',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var _this=this
      //获取用户的登录信息
      if (app.globalData.hasLogin) {
        let userInfo = wx.getStorageSync('userInfo');
        // let username1=wx.getStorageSync('username');
        // let userLevelId=wx.getStorageSync('userLevelId');
        _this.setData({
          userInfo: userInfo,
          userLevelId:userInfo.userLevelId,
          username1:userInfo.username,
          hasLogin: true
        });
      }
      _this.qingqiu();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onLoad();
    setTimeout(function(){
      wx.stopPullDownRefresh({
        complete: (res) => {
          wx.showToast({
            title: '刷新成功',
            icon:'none',
          })
        },
      })
    },200)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  chongzhi:function(){
    wx.navigateTo({
      url: '/pages/ucenter/chongzhi/chongzhi',
    })
  },
  czorder:function(){
    wx.navigateTo({
      url: '/pages/ucenter/czorder/czorder',
    })
  },
  jihuo:function(){
    wx.navigateTo({
      url: '/pages/ucenter/activehc/activehc',
    })
  },
  hongbao:function(){
    wx.navigateTo({
      url: '/pages/ucenter/redpacket/redpacket',
    })
  },
  jy_record:function(){
    wx.navigateTo({
      url: '/pages/ucenter/jy_record/jy_record',
    })
  },
  chakan:function(){
    this.setData({
      isRuleTrue: true
    })
  },
      //打开输入弹窗
  showRule: function () {
        this.setData({
          isRuleTrue: true
        })
      },
      //关闭输入弹窗
      hideRule: function () {
        this.setData({
          isRuleTrue: false
        })
      },
})