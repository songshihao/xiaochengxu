var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
var user = require('../../../services/user.js');
var util1 = require('../../../utils/back.js'); 

var app = getApp();

import * as echarts from '../../../ec-canvas/echarts1';
function initChart(canvas, width, height, dpr) {
  var that = this
  var token = wx.getStorageSync('token');
  if (token == '') {
    wx.navigateTo({
      url: '/pages/auth/login/login',
    })
    return;
  }
  util.request(api.recommended,null,'POST').then(function(res){
    // console.log(res)
    if(res.errno==501||res.errmsg=='请登录'){
      wx.showModal({
        itle: '提示',
        content: '验证失效请重新登录',
        success (res) {
        if (res.confirm) {
          wx.navigateTo({
            url: '/pages/auth/login/login',
          })
        } else if (res.cancel) {
          wx.switchTab({
            url: '/pages/dts_index/index',
          })
        }
      }
      })
    }
    // console.log(res)
    var timedate = []
    var conts = []
    var list = res.data.list
    for (var i = 0; i < list.length; i++) {
      conts.push(list[i].new_count)
      timedate.push(list[i].show_time.substring(5, 10))
    }
    var xdata = [timedate[0], timedate[1], timedate[2], timedate[3], timedate[4], timedate[5], timedate[6]]
    var ydata = [conts[0], conts[1], conts[2], conts[3], conts[4], conts[5], conts[6]]

    const chart = echarts.init(canvas, null, {
      width: width,
      height: height,
      devicePixelRatio: dpr // new
    });
    canvas.setChart(chart);

    var option = {
      title: {
        text: '数据统计',
        left: 'center'
      },
      color: ["#37A2DA", "#67E0E3", "#9FE6B8"],
      legend: {
        data: [' '],
        top: 50,
        left: 'center',
        // backgroundColor: 'red',
        z: 100
      },
      grid: {
        containLabel: true
      },
      tooltip: {
        show: true,
        trigger: 'axis'
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: xdata
        // show: false
      },
      yAxis: {
        x: 'center',
        type: 'value',
        splitLine: {
          lineStyle: {
            type: 'dashed'
          }
        }
        // show: false
      },
      series: [{
          name: '分享人数',
          type: 'line',
          smooth: true,
          data: ydata
        }
      ]
    };
    chart.setOption(option);
    return chart;
  })
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    hasMobile: '',
    ec: {
      onInit: initChart
    },
    count: '',
    count1: '',
    rennum: [],
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    visible: 'none',
    echarts: '',
    ewm: '',
    statusBarHeight: app.globalData.statusBarHeight,
    titleBarHeight: app.globalData.titleBarHeight,
    mode: 'aspectFill',
    token:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    //获取用户的登录信息
    if (app.globalData.hasLogin) {
      let userInfo = wx.getStorageSync('userInfo');
      let token=wx.getStorageSync('token');
      // let userLevelId=wx.getStorageSync('userLevelId');
      that.setData({
        userInfo: userInfo,
        userLevelId:userInfo.userLevelId,
        username1:userInfo.username,
        hasLogin: true,
        token:token
      });
    }
    wx.setNavigationBarTitle({ title:'家畅智能酒店置换平台——人人都可以免费住酒店点击领取100元全额抵用红包券！'}) //页面标题为路由参数 
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },
  //打开规则提示
  showRule: function () {
    this.setData({
      isRuleTrue: true
    })
  },

  //关闭规则提示
  hideRule: function () {
    this.setData({
      isRuleTrue: false
    })
  },

  bindGetUserInfo(e) {
    let userInfo = wx.getStorageSync('userInfo');
    let token = wx.getStorageSync('token');
    if (userInfo && token) {
      return;
    }
    if (e.detail.userInfo) {
      //用户按了允许授权按钮
      user.loginByWeixin(e.detail).then(res => {
        this.getPhoneNumber()
        this.setData({
          userInfo: res.data.userInfo
        });
        app.globalData.userInfo = res.data.userInfo;
        app.globalData.token = res.data.token;
      }).catch((err) => {
        console.log(err)
      });
    } else {
      //用户按了拒绝按钮
      wx.showModal({
        title: '警告通知',
        content: '您点击了拒绝授权,将无法正常显示个人信息,点击确定重新获取授权。',
        success: function (res) {
          if (res.confirm) {
            wx.openSetting({
              success: (res) => {
                if (res.authSetting["scope.userInfo"]) { ////如果用户重新同意了授权登录
                  user.loginByWeixin(e.detail).then(res => {
                    this.setData({
                      userInfo: res.data.userInfo
                    });
                    app.globalData.userInfo = res.data.userInfo;
                    app.globalData.token = res.data.token;
                  }).catch((err) => {
                    console.log(err)
                  });
                }
              }
            })
          }
        }
      });
    }
  },
  exitLogin: function () {
    wx.showModal({
      title: '',
      confirmColor: '#e9332f ',
      content: '退出登录？',
      success: function (res) {
        if (res.confirm) {
          wx.removeStorageSync('token');
          wx.removeStorageSync('userInfo');
          wx.switchTab({
            url: '/pages/index/index'
          });
        }
      }
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that=this
    util.request(api.recommended,null,'POST').then(function(res){
      that.setData({
        count: res.data.count,
        count1: 15 - res.data.count,
      })
        //   if(res.erron == 0){
        //     console.log(res.data.count)
        //   that.setData({
        //     count: res.data.count,
        //     count1: 15 - res.data.count,
        //   })
        // }else 
        if(res.errno==501||res.errmsg=='请登录'){
          wx.showModal({
            itle: '提示',
            content: '验证失效请重新登录',
            success (res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '/pages/auth/login/login',
              })
            } else if (res.cancel) {
              wx.switchTab({
                url: '/pages/dts_index/index',
              })
            }
          }
          })
        }
    })

  that.hb();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onLoad();
    setTimeout(function(){
      wx.stopPullDownRefresh({
        complete: (res) => {
          wx.showToast({
            title: '刷新成功',
            icon:'none',
          })
        },
      })
    },200)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  onBackTap: function (e) {

    util1.onBackTap() //调用

  },
  onShareAppMessage: function (res) {
    // console.log(res)
    // console.log(wx.getStorageSync('username'))
    var nickName = ''
    wx.getStorage({
      key: 'userInfo',
      success: function (res) {
        // console.log(res)
        nickName = res.data.nickName
      }
    })

    if (res.from === 'button') {
      // 通过按钮触发
      var data = res.target.dataset
      return {
        title: '家畅——人人都可以免费住酒店点击领取100元全额抵用红包券！',
        desc: '家畅智能酒店置换商城平台通过置换酒店空置房，来帮助酒店免费升级为智慧酒店，并免费提供酒店所需的物资和服务，致力于打造中国最大的智慧酒店',

       path: '/pages/topic/topic?username=' + wx.getStorageSync('username') + '&nickName=' + wx.getStorageSync('nickName'),
        imageUrl: 'https://jiachang8.com/mpimage/share_img4.jpg',
        success: function (res) {
          // 转发成功
          console.log('转发成功')
        },
        fail: function (res) {
          // 转发失败
          console.log('转发失败')
        }
      }
    }
    //通过右上角菜单触发
    return {
      title: '家畅——人人都可以免费住酒店点击领取100元全额抵用红包券！',
      desc: '家畅智能酒店置换商城平台通过置换酒店空置房，来帮助酒店免费升级为智慧酒店，并免费提供酒店所需的物资和服务，致力于打造中国最大的智慧酒店',
      // path: "/pages/researches/index",
     path: '/pages/topic/topic?username=' + wx.getStorageSync('username') + '&nickName=' + wx.getStorageSync('nickName'),
      imageUrl: 'https://jiachang8.com/mpimage/share_img4.jpg',
    };
  },
  vipshouyi: function () {
    wx.navigateTo({
      url: '/pages/ucenter/vipshouyi/vipshouyi',
    })
  },
  mingxi: function (res) {
    wx.navigateTo({
      url: '/pages/ucenter/vipshouyi/vipshouyi',
    })
  },
  share: function () {
    var that = this
    wx.request({
      url: app.globalData.url83 + 'api/user/getImgUrl',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data: {},
      success: function (res) {
        console.log(res)
        if(res.data.errmsg=='token失效，请重新登'){
          wx.navigateTo({
            url: '/pages/auth/login/login',
          })
          return;
        }
        wx.setStorageSync('ewm', res.data.imgUrl);
        that.setData({
          ewm: res.data.imgUrl
        })
      },
      fail: function (res) {
        console.log(res)
      }
    })
    this.setData({
      echarts: 1
    })
    this.show()
    // wx.navigateTo({
    //   url: '/pages/ceshi111/hb/hb',
    // })
  },
  //事件处理函数
  show: function () {
    this.setData({
      visible: 'black'
    })
  },
  close: function () {
    this.setData({
      visible: 'none'
    })
    this.setData({
      echarts: ''
    })
  },
  savephoto: function () {
    //获取相册授权
    var that=this
    var imgSrc = this.data.ewm
    wx.downloadFile({
      url: imgSrc,
      success: function (res) {
        console.log(res);
        //图片保存到本地
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success: function (data) {
            console.log(data);
            if(data.errMsg=="saveImageToPhotosAlbum:ok"){
              wx.showToast({
                title: '保存成功',
                duration:2000
              })
              that.setData({
                visible: 'none'
              })
              that.setData({
                echarts: ''
              })
            }
          },
          fail: function (err) {
            console.log(err);
            if (err.errMsg === "saveImageToPhotosAlbum:fail auth deny") {
              console.log("用户一开始拒绝了，我们想再次发起授权")
              console.log('打开设置窗口')
              wx.openSetting({
                success(settingdata) {
                  console.log(settingdata)
                  if (settingdata.authSetting['scope.writePhotosAlbum']) {
                    console.log('获取权限成功，给出再次点击图片保存到相册的提示。')
                  } else {
                    console.log('获取权限失败，给出不给权限就无法正常使用的提示')
                  }
                }
              })
            }
          }
        })
      }
    })

  },
  hb:function () {
    if (this.data.userInfo == '') {
      var ewm = wx.getStorageSync('ewm')
      wx.getStorage({
        key: 'userInfo',
        success: function (res) {
          this.setData({
            userInfo: res,
            hasUserInfo: true
          })
        }

      })

    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          console.log(res)
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    }
  },
  getUserInfo: function (e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  }
})