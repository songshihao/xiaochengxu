var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
// 引入SDK核心类
var QQMapWX = require('../../../lib/qqmap-wx-jssdk.min.js');
// 实例化API核心类
var qqmapsdk = new QQMapWX({
    key: 'GVOBZ-APK6P-BOZDT-V5ZWX-MRI7T-6GF5H' // 必填
});

Page({

  /**
   * 页面的初始数据
   */
  data: {
    markers:[],
    hotelName:'',
    from:'',
    list:'',
    show:false
  },
  hotelName(e){
    // console.log(e)
    this.setData({
      hotelName:e.detail.value
    })
  },
  clearInput: function (e) {
    console.log(e)
    switch (e.currentTarget.id) {
      case 'clear-hotelName':
        this.setData({
          hotelName: ''
        });
        break;
  }
},
search:function(){
  var that=this
  util.request1(api.hotel_selectByMap,{
      text:that.data.hotelName,
      from:that.data.from
  },'POST').then(function(res){
    console.log(res)
    if(res.code==0){
      that.setData({
        list:res.data,
        show:false
      })
    }else
    if(res.code==200){
      util.request1(api.hotel_selectByMap,{
        text:that.data.hotelName,
        from:that.data.from
    },'POST').then(function(ant){
      console.log(ant)
      if(ant.code==0){
        that.setData({
          list:ant.data,
          show:false
        })
      }else if(ant.code==200)
      that.setData({
        show:true,
        list:[]
      })
    })
    }
  })
},
hotelxq:function(e){
  // console.log(e)
  var that=this
  var index = e.currentTarget.dataset.index
  var hotelid = e.currentTarget.dataset.id
  var add = e.currentTarget.dataset.add
  var imglist = e.currentTarget.dataset.appearancepicurl
  if (that.data.list[index].hotelLabel){
  var hotellabel = that.data.list[index].hotelLabel
}else{var hotellabel = ''}
  wx.setStorage({
    data: hotelid,
    key: 'hotelId',
  })
  wx.navigateTo({
    url: '/pages/ucenter/hotelxq/hotelxq?id=' + hotelid  +'&add=' + add + '&location=' + location +'&imglist=' + imglist,
  })
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var _this=this
    wx.getStorage({
      key: 'location',
      success:function(res){
        console.log(res.data)
        _this.setData({
          from:res.data.lat +','+res.data.lng
        })
      }
    })
  },

  //触发表单提交事件，调用接口
  formSubmit(e) {
     
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})