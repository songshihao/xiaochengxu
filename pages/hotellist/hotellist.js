// pages/hotellist/hotellist.js
var QQMapWX = require('../../lib/qqmap-wx-jssdk.min.js');
const api = require('../../config/api')
var app = getApp();
// 实例化API核心类
var qqmapsdk = new QQMapWX({
  key: 'GVOBZ-APK6P-BOZDT-V5ZWX-MRI7T-6GF5H'// 必填
}); 
Page({
  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    city:'',
    region: [],
    startTime: '',
    endTime:'',
    queryCondition:'',
    hotelLabel:'',
    starLevel:'',
    timess:'',
    check_in_time:'',
    departure_time:'',
    mode: 'aspectFill',
    buttonlist:[
      {name:"所有酒店",id:'0'},
      {name:"商务出行",id:'1'},
      {name:"VR看房",id:'2'},
      {name:"平台推荐",id:'3'},
      {name:"智慧酒店",id:'4'},
      {name:"价格优惠",id:'5'},
      // {name:"所有酒店",id:'6'},
    ],

    rangeindex:0,
    range_area:'',
    index:'',
    juli:'',
    diss:'',
    location:[],
    page: 1,
    pageSize: 30,
    from:'',
    label:'',
    label1:'',
    xzq:'',
    lowerPrice:'',
    upperPrice:'',
    checkInDate:'',
    checkOutDate: '',
    succ:0,
    setNavigationBar_Title:0,
    show:0,
    menux:[
      {id:0,name:''},
      {id:1,name:'距离'},
      {id:2,name:'价格区间'},
      // {id:3,name:'大学'},
      // {id:4,name:'机场/车站',menu:[{name:'机场'},{name:'火车站'},{name:'汽车客运站'},]}
    ],
    menux_name:'距离',
    range_area_name:'',
    menux_id:0,
    menux_name_list:[
      {name:"不限",id:'0',value:''},
      {name:"1km",id:'1',value:'1'},
      {name:"3km",id:'2',value:'3'},
      {name:"5km",id:'3',value:'5'},
      {name:"10km",id:'4',value:'10'},
    ],
    area_list:[],
    collegelist:[],
    Airport_station_list:[],
    noneHotel:false,
    loading: false,//加载动画的显示
    },
  searchinput(e){
    this.setData({
      hotelnameinput:e.detail.value
    })
  },
  minprice(e){
    this.setData({
      lowerPrice:e.detail.value
    })
  },
  move() {},
  maxprice(e){
    this.setData({
      upperPrice:e.detail.value
    })
  },
  search_hotelname:function(){
    this.shanxuan();
    wx.navigateTo({
      url: '/pages/hotellist/search/search',
    })
  },
  show:function(e){
    var that=this
    var show=that.data.show
    if(show==0){
      that.setData({
        show:1
      })
    }else{
      that.setData({
        show:0
      })
    }
    
  },
  confirm:function(e){
    var that=this
    that.setData({
      show:0
    })
    this.hqlist();
  },
    checked:function(e){
      console.log(e)
      var name=e.currentTarget.dataset.name
      this.setData({
        menux_name:name
      })
    },
    menux_active:function(e){
      console.log(e)
      var menux_id=e.currentTarget.dataset.id
      var vvv=e.currentTarget.dataset.vvv
      var name=e.currentTarget.dataset.name
      this.setData({
        menux_id:menux_id,
        range_area:vvv,
        range_area_name:name
      })
    },
    reset:function(e){
      // console.log(e)
      var that=this
      that.setData({
        id:'',
        details:'',
        menux_id:0,
        range_area:'',
        lowerPrice:'',
        upperPrice:'',
        range_area_name:'',
        startdate_show:true,
        enddate_show:true,
      })
    },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
  },
  cont:function(){
    console.log(this.data.list)
  },//合并数组
  hqlist:function(){
    var _this=this

    wx.showLoading();
    wx.request({
      url: app.globalData.url83 + 'api/hotel/selectByMap',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
     data:{
      city:_this.data.city,
      lowerPrice:_this.data.lowerPrice,//开始价格
      upperPrice:_this.data.upperPrice,//结束价格
      range:_this.data.range_area,
      from:_this.data.from
     },
     success:function(res){
       wx.hideLoading();
       if(res.data.errno==401){
        wx.showModal({
          title: '登录失效',
          content: '点击确定重新登录',
          success (res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '/pages/auth/login/login',
              })
            } else if (res.cancel) {
              wx.switchTab({
                url: '/pages/topic/topic',
              })
            }
          }
        })
       }else if(res.data.code==0){
        _this.setData({
          list:res.data.data,
          noneHotel:false
        })
       }
       else if(_this.data.range_area!=''){
        // wx.showToast('该范围内暂无符合条件的酒店')
          wx.showToast({
            title: '该范围内暂无符合条件的酒店',
            icon:'none'
          })
        _this.setData({
          list:[],
          noneHotel:true
        })
       }
       else if(res.data.data==''||res.data.data==undefined){
            wx.showModal({
              title: '没有查询到符合条件的酒店',
              content: '点击确定更改查询条件',
              success (res) {
                if (res.confirm) {
                  // console.log('用户点击确定')
                  var pages = getCurrentPages();                       //获取当前页面
                  var prePage = pages[pages.length - 2];               //获取上一页面
                  prePage.setData({
                    'search.page': 1                                   //给上一页面的变量赋值
                  })
                  wx.navigateBack({                                    //返回上一页面
                    delta: 1,
                  })
                } else if (res.cancel) {
                  wx.switchTab({
                    url: '/pages/topic/topic',
                  })
                }
              }
            })
          }else if(res.data.errmsg=="获取距离失败"){
            wx.showModal({
              title: '获取距离失败',
              content: '请检查定位是否开启',
            })
          }
     }
    })
  },
  hotelxq:function(e){
    console.log(e)
    var that=this
    var hotelid = e.currentTarget.dataset.id
    var hotelname = e.currentTarget.dataset.hotelname
    var startDate=that.data.startTime
    var endDate=that.data.endTime
    wx.navigateTo({
      url: '/pages/ucenter/hotelxq/hotelxq?id=' + hotelid +'&startDate='+startDate +'&endDate='+endDate +'&hotelname='+hotelname
    })
  },
  // bindRegionChange: function (e) {
  //   console.log(e)
  //   this.setData({
  //     region: e.detail.value,
  //     city:e.detail.value[2],
  //     xzq:e.detail.code[2]
  //   })
  //   if(e.detail.code[2]!=''){
  //     this.shanxuan();
  //   }
  //   wx.clearStorageSync('weizhi')
  // },
  bindPickerChange:function(e){
    this.setData({
      sortindex: e.detail.value
    })
  },
  acrossChange:function(e){
    this.setData({
      sortindex: e.detail.value
    })
  },
  siteChange:function(e){
    this.setData({
      site: e.detail.value
    })
  },

  starChange:function(e){
    this.setData({
      star: e.detail.value
    })
  },
  selectChange:function(e){
    this.setData({
      select: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.hotelsuggestion();
  },
  hotelsuggestion:function(){
    let that=this
    wx.request({
      url: api.hotelsuggestion,
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
     data:{
      region:that.data.city,
     },
     success:function(res){
       console.log(res)
       if(res.data.code==0){
         that.setData({
          collegelist:res.data['大学'],
          Airport_station_list:res.data['机场/车站']['机场']
         })
       }
     },fail:function(res){
       console.log(res)
     }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow:function () {
    var _this= this
      wx.getStorage({
        key: 'locatecity',
        success:function(res){
          if(res.data.city){
            _this.setData({
              city:res.data.city
            })
          }else{
            _this.setData({
              city:res.data
            })
          }
        }
      })
      let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
      if(getDate){
        _this.setData({
          startTime: getDate.checkInDate,
          endTime: getDate.checkOutDate,
        })
      }else{
        wx.getStorage({
          key: 'timedata',
          success:function(res){
            var startTime_a=res.data[0].startTimeq
            var endTime_a =res.data[0].endTimeq
            var startTime_y = startTime_a.replace('年', '-');
            var startTime_m = startTime_y.replace('月', '-');
            var startTime1 = startTime_m.replace('日', '');
    
            var startTime_y = endTime_a.replace('年', '-');
            var startTime_m = startTime_y.replace('月', '-');
            var endTime1 = startTime_m.replace('日', '');
    
            _this.setData({
              startTime:startTime1,
              endTime:endTime1,
              timess:res.data[0].timess,
            })
          }
        })
      }
      if(_this.data.from==''){
        wx.getStorage({
          key: 'location',
          success:function(res){
            // console.log(res.data.lat +','+res.data.lng)
            _this.setData({
             from:res.data.lat +','+res.data.lng
            })
            _this.hqlist()
          }
        })
      }
      else if(_this.data.from!=''){
        _this.hqlist()
      }
  },
  //停止下拉刷新
  onPullDownRefresh () {
    wx.stopPullDownRefresh()
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  city:function(e){
    wx.navigateTo({
      url: '/pages/ucenter/ceshi/ceshi?hotlelist=1',
    })
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onLoad();
    setTimeout(function(){
      wx.stopPullDownRefresh({
        complete: (res) => {
          wx.showToast({
            title: '刷新成功',
            icon:'none',
          })
        },
      })
    },200)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    // console.log(that)
    var page = that.data.page
    var list = that.data.list
    page++;
    that.setData({
    page: page
    })
    // 显示加载图标
    wx.showLoading({
    title: '玩命加载中',
    })
    setTimeout(function(){
      wx.hideLoading();
      wx.showToast({
        title: '暂无更多',
        icon: 'none',
        })
    },800)
 },
 pricedate:function(){
   var checkInDate=this.data.startTime
   var checkOutDate=this.data.endTime
  wx.navigateTo({
    url: '/pages/hotellist/pricedate/index?checkInDate='+ checkInDate+'&checkOutDate='+checkOutDate,
  })
 },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  button:function(e){
    // console.log(e.currentTarget.dataset.name)
    var _this=this
    var idx=e.currentTarget.dataset.id
    _this.setData({
      label:e.currentTarget.dataset.name,
      label1:e.currentTarget.dataset.name
    }) 
    if(idx!=''&&idx!=0){
      _this.shanxuan();
    }else if(idx==''||idx==0){
      _this.hqlist();
    }
   },
  shanxuan:function(){
    var _this=this
    wx.request({
      url: app.globalData.url83 + 'api/hotel/selectByMap',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      data:{
        label:_this.data.label,
        from:_this.data.from,
        area:_this.data.xzq,
        // name:_this.data.hotelnameinput,//上一个页面传得固定条件
        city:_this.data.city,
        range:_this.data.range_area,
        lowerPrice:_this.data.lowerPrice,//开始价格
        upperPrice:_this.data.upperPrice//结束价格
        // page:'',
        // pageSize:10
      },
      success:function(res){
        if(res.data.code==0){
          _this.setData({
            list:res.data.data,
            xzq:'',
            label:'',
          })
        }else if(res.data.errmsg=="获取距离失败"){
          wx.showModal({
            title: '获取距离失败',
            content: '请检查定位是否开启',
          })
        }else if(res.data.code=='200'){this
          wx.showModal({
            title: "您搜索的酒店不存在",
            success (res) {
              if (res.confirm) {
                _this.setData({
                  xzq:'',
                  label:'',
                  city:_this.data.city
                })
                _this.hqlist();
              } else if (res.cancel) {
                wx.switchTab({
                  url: '/pages/topic/topic',
                })
              }
            }
          })
        }
        else{
          _this.setData({
            list:'',
            xzq:'',
            label:''
          })
          _this.onShow()
        }
      },
      fail:function(res){
        console.log(res)
      }
    })
  },
  nearby:function(){
 wx.navigateTo({
   url: '/pages/hotellist/nearby/nearby',
 })
  },
  onShareAppMessage: function (res) {
    wx.getStorage({
      key: 'userInfo',
      success:function(res){
        nickName=res.data.nickName
      }
    }) 
    if (res.from === 'button') {
      // 通过按钮触发
      var data = res.target.dataset
      return {
        title: '免费提供智慧酒店升级，以及酒店所需物资和服务',
        desc: '家畅智能酒店置换商城平台通过置换酒店空置房，来帮助酒店免费升级为智慧酒店，并免费提供酒店所需的物资和服务，致力于打造中国最大的智慧酒店',
       path: '/pages/topic/topic?username=' + wx.getStorageSync('username') + '&nickName=' + wx.getStorageSync('nickName'),
        imageUrl: 'https://jiachang8.com/mpimage/share_img4.jpg',
        success: function (res) {
          // 转发成功
          console.log('转发成功')
        },
        fail: function (res) {
          // 转发失败
          console.log('转发失败')
        }
      }
    }
    
    //通过右上角菜单触发
    return {
     
      title: '免费提供智慧酒店升级，以及酒店所需物资和服务',
      desc: '家畅智能酒店置换商城平台通过置换酒店空置房，来帮助酒店免费升级为智慧酒店，并免费提供酒店所需的物资和服务，致力于打造中国最大的智慧酒店',
      // path: "/pages/researches/index",
       path: '/pages/topic/topic?username=' + wx.getStorageSync('username') + '&nickName=' + wx.getStorageSync('nickName'),
      imageUrl: 'https://jiachang8.com/mpimage/share_img4.jpg',
    };
  },

})