var QQMapWX = require('../../../lib/qqmap-wx-jssdk.min.js');
var qqmapsdk;
qqmapsdk = new QQMapWX({
  key: 'GVOBZ-APK6P-BOZDT-V5ZWX-MRI7T-6GF5H'
});
Page({
 
  /**
   * 页面的初始数据
   */
  data: {
    // 获取的经纬度
    // t_lat: '32.00335',
    // t_lng: '118.73145',
    t_lat: '',
    t_lng: '',
    // 地图的markers
    markers: [],
    // 当前选中第几个
    xmwzB_index: 0,
    // tab列表
    tabs: [{
        name: '交通'
      },
      {
        name: '学校'
      },
      {
        name: '医疗'
      },
      {
        name: '购物'
      },
      {
        name: '餐饮'
      },
    ],
    // 把从腾讯地图SDK获取的位置存起来，以后每次点击就不用请求了。
    arrlist:[[],[],[],[],[]],
    arrlist_cur:'',
    address:''
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad:function(options){
    var that=this
    wx.getStorage({
      key: 'address',
      success:function(res){
        console.log(res)
        that.setData({
          address:res.data
        })
      }
    })
    wx.getStorage({
      key: 'location',
      success:function(res){
        console.log(res)
        that.setData({
          t_lat:res.data.lat,
          t_lng:res.data.lng
        })
        var marks = [];
        // 地图上的icon图标
        marks.push({ // 获取返回结果，放到mks数组中
          latitude: res.data.lat,
          longitude: res.data.lng,
          // iconPath: '../../images/around/address.png', //图标路径
          width: 20,
          height: 20,
        });
        // 改写气泡
        marks.push({ // 获取返回结果，放到mks数组中
          title: '当前位置',
          latitude: res.data.lat,
          longitude: res.data.lng,
          // iconPath: '../../images/around/cover_1.png', //图标路径
          width: 20,
          height: 20,
          address: that.data.address,
          callout: {
            content: '当前位置',
            color: '#fff',
            bgColor: '#3072f6',
            fontSize: 14,
            padding: 10,
            borderRadius: 10,
            display: 'ALWAYS'
          }
        });
        that.setData({
          markers: marks
        });
        // 进页面先请求一波(第一个tab下对应的列表内容)
        that.nearby_search(that.data.tabs[0].name);
      }
    })
  },
  oncha: function () {
  
  },
  // 点击tab切换
  xmwzB_click(e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    that.setData({
      xmwzB_index: index
    }, () => {
      var name = that.data.tabs[index].name;
      that.nearby_search(name);
    });
  },
  // 通过关键字调用地图SDK，搜索获取结果列表
  nearby_search(key) {
    console.log(key)
    var that = this;
    var xmwzB_index = that.data.xmwzB_index;
    var list_c = that.data.arrlist[xmwzB_index];
    // 判断是否请求过了，如果没请求过则请求；请求过了就直接赋值
    if(list_c.length){
      that.setData({
        arrlist_cur:list_c
      });
    }else{
      wx.showToast({

        title: '请稍后',
        icon: 'loading',
        duration: 2000
      })
      qqmapsdk.search({
        keyword: key, // 搜索关键词
        page_size: 5, // 一页展示几个
        location: that.data.t_lat + ',' + that.data.t_lng, //设置周边搜索中心点
        success: function (res) { //搜索成功后的回调
          wx.hideToast({});
          var arrlist = [];
          // 对获取的信息进行处理，整理出需要的字段
          // 有些可能会涉及位置跳转（需要经纬度）；展示位置名（详细参数设置参考官网说明）
          for (var i = 0; i < res.data.length; i++) {
            arrlist.push({ // 获取返回结果，放到mks数组中
              title: res.data[i].title,
              latitude: res.data[i].location.lat,
              longitude: res.data[i].location.lng,
              distance: res.data[i]._distance,
            })
          }
          // 每次不用重新赋值，通过下标给需要的赋值
          var arrlist_key = 'arrlist['+xmwzB_index+']';
          that.setData({
            [arrlist_key]: arrlist,
            arrlist_cur:arrlist
          });
        },
        fail: function (res) {
          console.log(res);
        },
        complete: function (res) {
          //console.log(res.data);
        }
      });
    }
  },
  // 小程序地图api，跳转大地图
  show_big_map: function () {
    var that = this;
    wx.openLocation({
      latitude: parseFloat(that.data.t_lat), // 纬度，浮点数，范围为90 ~ -90
      longitude: parseFloat(that.data.t_lng), // 经度，浮点数，范围为180 ~ -180。
      name: '当前位置', // 位置名
      address:that.data.address, // 地址详情说明
      scale: 16, // 地图缩放级别,整形值,范围从1~28。默认为最大
    });
  },
  onShow:function(){
    var that=this
    wx.getStorage({
      key: 'location',
      success:function(res){
        // console.log(res)
        that.setData({
          t_lat:res.data.lat,
          t_lng:res.data.lng
        })
      }
    })
    wx.getStorage({
      key: 'address',
      success:function(res){
        // console.log(res)
        that.setData({
          address:res.data
        })
      }
    })
    if(that.data.t_lat!=''){
      that.oncha();
    }
  }
})