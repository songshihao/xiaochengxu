// pages/comInform/comInform.js

var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userList:{}
  },
  onShow: function () {
    this.showList()
  },
  showList(){
    util.request1(api.userInfoList,{
      page:1
    },'POST').then(res => {
      if(res.code == 0){
        this.setData({
          userList: res.data.list
        })
        console.log(this.data.userList)
      }
    })
  },


  // 删除
  updataUsercom(event){
    let that = this
    let id = event.currentTarget.dataset.userId
    console.log(id)
    wx.showModal({
      title:'',
      content:'确认要删除该条信息',
      success: function(res) {
        if(res.confirm){
          util.request1(api.userInfoDelete,{
            id:id
          },"POST").then( function(res) {
            if(res.code === 0){
              that.showList()
            }else{
              util.showErrorToast('删除失败')
            }
          })
        }
      }
    })
  },

  // 页面跳转
  userComAddOrUpdate(event){
    wx.navigateTo({
      url: '/pages/dts_index/comInformAdd/comInformAdd?id=' + event.currentTarget.dataset.userId
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})