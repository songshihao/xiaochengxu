const util = require('../../../utils/util');
const api = require('../../../config/api.js');

var focus
var isShowView
Page({
data:{
// text:"这是一个页面"
focus:false,
isShowView:true,
messages:[]
},
bindtap:function(event){

},
bindfocus:function(){
this.setData({
     focus:true
})
this.setData({
  isShowView:false
})
},
bindblur:function(){

      this.setData({
  focus:false
})
this.setData({
       isShowView:true
})
},
onLoad:function(options){
// 页面初始化 options为页面跳转所带来的参数
console.log(options);
var that=this
util.request(api.article_list,
  {
    type: 3
  }).then(function(res) {
    console.log(res)
    if(res.errno==0){
      that.setData({
        messages:res.data
      })
    }else{
      util.showErrorToast('请求失败！')
    }
  })
 
},
detail:function(res){
  console.log(res)
  var id=res.currentTarget.dataset.index
  wx.navigateTo({
    url: '/pages/article/article?id='+id,
  })
},
onReady:function(){
// 页面渲染完成
},
onShow:function(){
// 页面显示
},
onHide:function(){
// 页面隐藏
},
onUnload:function(){
// 页面关闭
}
})