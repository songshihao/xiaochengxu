// pages/ucenter/membership/membership.js
const util = require('../../../utils/util.js');
const api=require('../../../config/api')
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    avatarUrl:'',
    nickName:'',
    typelist:[
      {name:'收入'},
      {name:'支出'}
    ],
    array: [
      {
        id: 2,
        name: '请选择分类'
      },
      {
        id: 0,
        name: '收入'
      },
      {
        id: 1,
        name: '支出'
      },

    ],
    index: 0,
    amountType:[
      {id:0,name:'酒店券'},
      {id:1,name:'红包券'},
      {id:2,name:'奖励券'},
      {id:3,name:'现金'},
    ],
    menu:[
      {id:0,name:''},
      {id:1,name:'住宿'},
      {id:2,name:'注册支出'},
      {id:3,name:'注册分成支出'},
      {id:4,name:'购物'},
    ],
    page:1,
    multiIndex: [0, 0, 0],
    startdate: '',
    enddate:'',
    startdate_show:true,
    enddate_show:true,
    startTime:'',
    endTime:'',
    show:false,
    id:'',
    sum:{},
    type:-1,
    details:'',
    },
  copy: function(e) {
    util.copy(e.target.dataset.copy)
  },
  bindPickerChange: function(e) { 
    // console.log(e)
    var i=e.detail.value
    var type=this.data.array[i].id
    // console.log(type)
    this.setData({
      index: e.detail.value,
      type:type
    })
    this.diaoyong();
  },
  show:function(e){
    this.setData({
      show:true
    })
  },
  hide:function(e){
  this.setData({
      show:false
    })
    this.diaoyong();
  },
  checked:function(e){
    console.log(e)
    var id=e.currentTarget.dataset.id
    var name=e.currentTarget.dataset.name
    this.setData({
      id:id,
      details:name
    })
  },
  reset:function(e){
    console.log(e)
    this.setData({
      id:'',
      details:'',
      startTime:'',
      endTime:'',
      startdate_show:true,
      enddate_show:true,
    })
  },
  bindDateChange: function(e) {
    this.setData({
      startdate: e.detail.value,
      startTime:e.detail.value,
      startdate_show:false
    })
  },
  bindDateChange1: function(e) {
    this.setData({
      enddate: e.detail.value,
      endTime: e.detail.value,
      enddate_show:false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
    wx.getStorage({
      key: 'userInfo',
      success:function(res){
        console.log(res)
        that.setData({
          avatarUrl:res.data.avatarUrl,
          nickName:res.data.nickName
        })
       
      }
    })
    var date = util.formatTime6(new Date())
    var date1 = util.formatTime7(new Date())
    this.setData({
      startdate: date[0],
      enddate: date1[0]
    })
    util.request(api.accountBalanceSheet,{
      page:1,
      pageSize:20,
      bool:true,
      type:that.data.type,
      details:that.data.details,
      startTime:that.data.startTime,
      endTime:that.data.endTime
    },'POST').then(function(res){
      console.log(res)
      if(res.errno==0){
        that.setData({
          list:res.data.list,
          sum:res.data.sum
        })
      }
    
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  diaoyong: function () {
    var that=this
    util.request(api.accountBalanceSheet,{
      page:1,
      pageSize:20,
      bool:true,
      type:that.data.type,
      details:that.data.details,
      startTime:that.data.startTime,
      endTime:that.data.endTime
    },'POST').then(function(res){
      console.log(res)
      if(res.errno==0){
        that.setData({
          list:res.data.list,
          sum:res.data.sum
        })
      }
    
    })
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that=this
    util.request(api.accountBalanceSheet,{
      page:1,
      pageSize:20,
      bool:true,
      type:that.data.type,
      details:that.data.details,
      startTime:that.data.startTime,
      endTime:that.data.endTime
    },'POST').then(function(res){
      wx.stopPullDownRefresh({
        complete: (res) => {
          wx.showToast({
            title: '刷新成功',
          })
        },
      })
      if(res.errno==0){
        that.setData({
          list:res.data.list,
          sum:res.data.sum
        })
      }
    
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    // console.log(that.data.hotelManalist)
    var page = that.data.page
    var list_ALL = that.data.list
    page++;
    that.setData({
    page: page
    })
    // 显示加载图标
    wx.showLoading({
    title: '玩命加载中',
    })
    util.request(api.accountBalanceSheet,{
      page:page,
      pageSize:20,
      bool:true,
      type:that.data.type,
      details:that.data.details,
      startTime:that.data.startTime,
      endTime:that.data.endTime
    },'POST').then(function(res){
      // console.log(res)
      wx.hideLoading();
      var list =res.data.list
    if (list == '') {
    wx.showToast({
    title: '暂无更多',
    icon: 'none',
    })
    } else {
    var lists = list_ALL.concat(list)     //grade  为一进入页面请求完数据定义的集合
    that.setData({
      list : lists,
      sum:res.data.sum
    });
    }
  })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})