var util = require('../../utils/util.js');
var api = require('../../config/api.js');
var user = require('../../utils/user.js');
import NumberAnimate from "../../utils/num";
const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
      userInfo: {
        nickName: '点击登录',
        avatarUrl: ''
      },
      order: {
        unpaid: 0,
        unship: 0,
        unrecv: 0,
        uncomment: 0
      },
    MyMenus: [
      // { url: "/pages/ucenter/collect/collect", pic:"icon_collect.png",name:"商品收藏"},
      // { url: "/pages/ucenter/footprint/footprint", pic: "footprint.png", name: "浏览足迹" },
      { url: "/pages/groupon/myGroupon/myGroupon", pic: "wdpt.png", name: "我的拼团" },
      { url: "/pages/ucenter/address/address", pic: "ddgl.png", name: "地址管理" },
      { url: "/pages/ucenter/feedback/feedback", pic: "yjfk.png", name: "意见反馈" }
      // { url: "/pages/about/about", pic: "about_us.png", name: "关于我们" }
      // *,{ url: "/pages/about/about", pic: "comment.png", name: "使用帮助" }
      ],
      PingtaiMenus: [
        { url: "/pageA/pages/platformMana/partnerMana/partnerMana", pic:"https://jiachang8.com/mpimage/join.png",name:"合伙人管理",userLevelId:'8'},
        { url: "/pageA/pages/platformMana/hotelManalist/hotelManalist", pic: "https://jiachang8.com/mpimage/hotel_co.png", name: "酒店管理",userLevelId:'8' },
        { url: "/pageA/pages/platformMana/supplierMana/supplierMana", pic: "https://jiachang8.com/mpimage/supplier.png", name: "供应商管理",userLevelId:'8' },
        { url: "/pageA/pages/platformMana/contract/contract", pic: "https://jiachang8.com/mpimage/feedback.png", name: "合同审核",userLevelId:'8' },
        { url: "/pages/dts_index/liushui/liushui", pic: "https://www.jiachang8.com/mpimage/flow.png", name: "平台流水",userLevelId:'8' },
        // { url: "/pageA/pages/platformMana/supplierMana/supplierMana", pic: "https://jiachang8.com/mpimage/join.png", name: "房态/价格管理",userLevelId:'100' },
        // { url: "/pages/about/about", pic: "https://jiachang8.com/mpimage/join.png", name: "订单审核",userLevelId:'100' },
        // { url: "/pages/about/about", pic: "https://jiachang8.com/mpimage/join.png", name: "订单管理",userLevelId:'100' },
        // { url: "/pages/ucenter/collect/collect", pic:"https://jiachang8.com/mpimage/join.png",name:"财务管理",userLevelId:'100'},
        // { url: "/pages/ucenter/footprint/footprint", pic: "https://jiachang8.com/mpimage/join.png", name: "授权区域",userLevelId:'100' }
        ],
      hasLogin: false,
      totalAmount: 0.00,
      userLevelId:'',
      user_level1:{0:'会员',1:'VIP会员',2:'普通合伙人',3:'区县合伙人',4:'城市运营中心',5:'城市合伙人',6:'平台供应商',7:'平台酒店供应商',8:'平台管理员',9:'平台助理',12:'企业员工'},
      user_level:{0:'会员',1:'VIP会员',2:'合伙人',3:'合伙人',4:'合伙人',5:'合伙人',6:'平台供应商',7:'平台酒店供应商',8:'平台管理员',9:'总助',12:'企业员工'},
      username1:'',
      hdatahieht:'',
      hdatatop:'',
      partner_name:'',
      show:false,
      platform_tool:false,

      roomVoucherNub:'',
      hotel_voucher:'',
      red_envelopes:'',
      incentive_voucher:'',

      movable: {
        image: 'https://jczh.jiachang8.com/images/follow.gif'
      },
      bbh:''
  },

  /**
   * 页面跳转
  */
  goPages:function(e){
    console.log();
    if (this.data.hasLogin) {
      wx.navigateTo({
        url: e.currentTarget.dataset.url
      });
    } else {
      wx.navigateTo({
        url: "/pages/auth/login/login"
      });
    };
  },
  incentive_voucher:function(){
    wx.navigateTo({
      url: '/pages/ucenter/vipshouyi/vipshouyi',
    })
  },
  hotel_voucher:function(){
    wx.navigateTo({
      url: '/pages/ucenter/jy_record/jy_record',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that=this
    wx.setNavigationBarTitle({ title:'家畅智能酒店置换平台——人人都可以免费住酒店点击领取100元全额抵用红包券！'}) //页面标题为路由参数 
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline'],
      complete:function(res){
        console.log(res)
      }
    })
    var bbh=app.globalData.bbh
    that.setData({
      bbh:bbh
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },
  onShow:function(){
    //获取用户的登录信息
    let that=this
    if (app.globalData.hasLogin) {
      let userInfo = wx.getStorageSync('userInfo');
      // console.log(userInfo)
      that.setData({
        userInfo: userInfo,
        userLevelId:userInfo.userLevelId,
        username1:userInfo.username,
        hasLogin: true
      });
      if(userInfo.userLevelId==8){
        that.setData({
          platform_tool:true
        })
      }else if(userInfo.userLevelId==9){
        that.setData({
          platform_tool:true
        })
      }else if(userInfo.userLevelId!=8||userInfo.userLevelId!=9){
        that.setData({
          platform_tool:false
        })
      }
      that.qingqiu();
    }
 
    var hdatahieht=wx.getStorageSync('hdatahieht')
    var hdatatop=wx.getStorageSync('hdatatop')
      // console.log(hdatahieht)
      // console.log(hdatatop)
      that.setData({
        hdatahieht:hdatahieht,
        hdatatop:hdatatop
      });
      util.request(api.UserIndex).then(function (res) {
        if (res.errno === 0) {
          that.setData({
            order: res.data.order,
            totalAmount: res.data.totalAmount,
            remainAmount: res.data.remainAmount,
            couponCount: res.data.couponCount
          });
        }
      });
  },
  qingqiu:function(){
    var _this=this
    util.request(api.checkBalances, null, 'POST').then(function(res) {
      wx.hideLoading();
      _this.setData({
          hotel_voucher:res.data.hotelVoucher,
          red_envelopes:res.data.redEnvelopes,
          incentive_voucher:res.data.incentiveVoucher
          })
          _this.animate(res);
    })
  
  },
  //数字变动
  animate:function(res){
    var _this=this
    _this.setData({
      hotel_voucher:'',
      red_envelopes:'',
      incentive_voucher:'',
    }); 
    var num1 = res.data.hotelVoucher;
     let n1 = new NumberAnimate({
         from:num1,
         speed:1000,
         refreshTime:100,
        //  decimals:3,
         onUpdate:()=>{
          _this.setData({
            hotel_voucher:n1.tempValue
           });
         },
     }); 
     let num2 = res.data.redEnvelopes;
     let n2 = new NumberAnimate({
         from:num2,
         speed:1500,
         decimals:0,
         refreshTime:100,
         onUpdate:()=>{
          _this.setData({
            red_envelopes:n2.tempValue
           });
         },
     });
 
     let num3 = res.data.incentiveVoucher;
     let n3 = new NumberAnimate({
         from:num3,
         speed:2000,
         refreshTime:100,
        //  decimals:2,
         onUpdate:()=>{
          _this.setData({
            incentive_voucher:n3.tempValue
           });
         },
     });
  },
  /**
  * 生命周期函数--监听页面卸载
  */
  onUnload: function () {

  },
  goLogin() {
    if (!this.data.hasLogin) {
      wx.navigateTo({
        url: "/pages/auth/login/login"
      });
    }
  },
  goBrokerage() {
    if (this.data.hasLogin) {
      wx.navigateTo({
        url: "/pages/brokerage/main/main"
      });
    } else {
      wx.navigateTo({
        url: "/pages/auth/login/login"
      });
    };
  },
  goOrder() {
    if (this.data.hasLogin) {
      try {
        wx.setStorageSync('tab', '0');
      } catch (e) {

      }
      wx.navigateTo({
        url: "/pages/ucenter/order/order"
      });
    } else {
      wx.navigateTo({
        url: "/pages/auth/login/login"
      });
    }
  },
  goOrderIndex(e) {
    if (this.data.hasLogin) {
      let tab = e.currentTarget.dataset.index
      let route = e.currentTarget.dataset.route
      try {
        wx.setStorageSync('tab', tab);
      } catch (e) {

      }
      wx.navigateTo({
        url: route,
        success: function (res) { },
        fail: function (res) { },
        complete: function (res) { },
      })
    } else {
      wx.navigateTo({
        url: "/pages/auth/login/login"
      });
    };
  },
  goAfterSale: function () {
    wx.showToast({
      title: '目前不支持',
      icon: 'none',
      duration: 2000
    });
  },
  allorder:function(){
    if (this.data.hasLogin) {
      wx.navigateTo({
        url: '/pages/hotelorder/orderlist/orderlist',
      })
    } else {
      wx.navigateTo({
        url: "/pages/auth/login/login"
      });
    }
  
},
selectByPartner:function(e){
var that=this
var partner=e.currentTarget.dataset.partner_name
if(partner=='合伙人'){
  util.request(api.selectByPartner, null, 'POST').then(function(res){
    that.setData({
      show:true,
      partner_name:res.data
    })
  })
}
},
Partner_hide:function(){
this.setData({
  show:false
})
},
myteam:function(){
  wx.navigateTo({
    url: '/pages/ucenter/myteam/myteam',
  })
},
subscribeMessage:function(){
  wx.requestSubscribeMessage({
    "tmplIds":['t4nvJyQlvSdfCI0xgjLTnM2xjI6O_aAiL5cLiFQ8Uhs', 'X9A3BcDyeWZy_O7nO_d5pA--9SWXGtXFfgHNfGehv6w'],
    success (res) {
        wx.navigateTo({
          url: '/pages/ucenter/vipshouyi/vipshouyi',
        })
     }
  })
},
copy: function(e) {
  util.copy(e.target.dataset.copy)
},
onShareAppMessage: function (res) {
  var nickName = ''
  wx.getStorage({
    key: 'userInfo',
    success: function (res) {
      nickName = res.data.nickName
    }
  })

  if (res.from === 'button') {
    // 通过按钮触发
    var data = res.target.dataset
    return {
      title: '家畅——人人都可以免费住酒店点击领取100元全额抵用红包券！',
      desc: '家畅智能酒店置换商城平台通过置换酒店空置房，来帮助酒店免费升级为智慧酒店，并免费提供酒店所需的物资和服务，致力于打造中国最大的智慧酒店',

     path: '/pages/topic/topic?username=' + wx.getStorageSync('username') + '&nickName=' + wx.getStorageSync('nickName'),
      imageUrl: 'https://jiachang8.com/mpimage/share_img4.jpg',
      success: function (res) {
        // 转发成功
        console.log('转发成功')
      },
      fail: function (res) {
        // 转发失败
        console.log('转发失败')
      }
    }
  }
  //通过右上角菜单触发
  return {
    title: '家畅——人人都可以免费住酒店点击领取100元全额抵用红包券！',
    desc: '家畅智能酒店置换商城平台通过置换酒店空置房，来帮助酒店免费升级为智慧酒店，并免费提供酒店所需的物资和服务，致力于打造中国最大的智慧酒店',
    // path: "/pages/researches/index",
   path: '/pages/topic/topic?username=' + wx.getStorageSync('username') + '&nickName=' + wx.getStorageSync('nickName'),
    imageUrl: 'https://jiachang8.com/mpimage/share_img4.jpg',
  };
}
})