// pages/comInformAdd/comInformAdd.js
var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    inform: {
      name:'',
      mobile:'',
      identity:'',
      isDefault: 0,
    },
    userId:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if(options.id && options.id !=-502){
      console.log(options.id)
      this.setData({
        userId:options.id
      })
      this.userDetail(options.id)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  userDetail(id){
    let that = this
    util.request1(api.userInforDetail,{
      id:id
    },'POST').then(function(res){
      if(res.code == 0){
        that.setData({
          inform:res.userInfo
        })
      }
    })
  },

  bindinputName(event){
    let inform = this.data.inform;
    inform.name = event.detail.value;
    this.setData({
      inform: inform
    });
  },

  bindinputMobile(event){
    let inform = this.data.inform;
    inform.mobile = event.detail.value;
    this.setData({
      inform: inform
    });
  },

  bindinputIdentity(event){
    let inform = this.data.inform;
    inform.identity = event.detail.value;
    this.setData({
      inform: inform
    });
  },

  // 设置默认地址
  bindIsDefault(){
    let inform = this.data.inform;
    inform.isDefault = !inform.isDefault
    inform.isDefault = inform.isDefault ? 1 : 0
    console.log(inform)
    this.setData({
      inform: inform
    });
  },

  // 保存
  saveInform(){
    let inform = this.data.inform
    if(inform.name == ''){
      util.showErrorToast('请输入姓名')
      return;
    }
    if(inform.mobile == ''){
      util.showErrorToast('请输入手机号')
      return;
    }
    if(inform.identity == ''){
      util.showErrorToast('请输入身份证号')
      return;
    }
    if(inform.id){
      util.request1(api.userInforUpdate, {
        id:inform.id,
        name:inform.name,
        mobile:inform.mobile,
        identity:inform.identity,
        isDefault:inform.isDefault
      }, 'POST').then(function(res){
        if(res.code === 0){
          wx.navigateBack();
        }else if(res.code == 404){
          util.showErrorToast(res.errmsg)
        }
      })
    }else{
      util.request1(api.userInfoAdd, {
        name:inform.name,
        mobile:inform.mobile,
        identity:inform.identity,
        isDefault:inform.isDefault
      }, 'POST').then(function(res){
        if(res.code === 0){
          wx.navigateBack();
        }else if(res.code == 404){
          util.showErrorToast(res.errmsg)
        }
      })
    }
    
  }
})