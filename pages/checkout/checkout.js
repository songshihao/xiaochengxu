var util = require('../../utils/util.js');
var api = require('../../config/api.js');

var app = getApp();
var lastTime = null;

Page({
  data: {
    isMultiOrderModel: 0,
    brandCartgoods:[],
    checkedGoodsList: [],
    checkedAddress: {},
    availableCouponLength: 0, // 可用的优惠券数量
    goodsTotalPrice: 0.00, //商品总价
    freightPrice: 0.00, //快递费
    couponPrice: 0.00, //优惠券的价格
    grouponPrice: 0.00, //团购优惠价格
    orderTotalPrice: 0.00, //订单总价
    actualPrice: 0.00, //实际需要支付的总价
    cartId: 0,
    addressId: 0,
    couponId: 0,
    message: '',
    grouponLinkId: 0, //参与的团购，如果是发起则为0
    grouponRulesId: 0, //团购规则ID
    C_password:0,
    couponchecked:false,
    coupons:1,
    coupon:0,
    status:false,
    payPass:'',
    isRuleTrue:false,
    //用户输入明文、暗文
    showType:'password',//如果是密码换成'password'
    isFocus: false,
    zfpassword: '',
    couponchecked:false,
    payPass:'',
    isRuleTrue:false,
    //正常密码框是6位
    dataSource: [{
      initValue: ''
    }, {
      initValue: ''
    }, {
      initValue: ''
    }, {
      initValue: ''
    },
    {
      initValue: ''
    }, {
      initValue: ''
    }],
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    
  },
  checkboxChange: function (e) {
   this.setData({
    coupon:e.detail.value,
    status:true
   })
  },
  
  //获取checkou信息
  getCheckoutInfo: function() {
    let that = this;
    util.request(api.CartCheckout, {
      cartId: that.data.cartId,
      addressId: that.data.addressId,
      couponId: that.data.couponId,
      grouponRulesId: that.data.grouponRulesId
    }).then(function(res) {
      if (res.errno === 0) {
        let brandCartgoods = [];
        let checkedGoodsList = [];
        if (res.data.isMultiOrderModel === 1) {
          brandCartgoods = res.data.brandCartgoods;
        } else {
          checkedGoodsList = res.data.checkedGoodsList;
        }
        that.setData({
          isMultiOrderModel: res.data.isMultiOrderModel,
          brandCartgoods: brandCartgoods,
          checkedGoodsList: checkedGoodsList,
          checkedAddress: res.data.checkedAddress,
          availableCouponLength: res.data.availableCouponLength,
          actualPrice: res.data.actualPrice,
          couponPrice: res.data.couponPrice,
          grouponPrice: res.data.grouponPrice,
          freightPrice: res.data.freightPrice,
          goodsTotalPrice: res.data.goodsTotalPrice,
          orderTotalPrice: res.data.orderTotalPrice,
          addressId: res.data.addressId,
          couponId: res.data.couponId,
          grouponRulesId: res.data.grouponRulesId,
        });
      }
      wx.hideLoading();
    });
  },
  selectCoupon() {
    wx.navigateTo({
      url: '/pages/ucenter/couponSelect/couponSelect',
    })
  },
  bindMessageInput: function(e) {
    this.setData({
      message: e.detail.value
    });
  },
  onReady: function() {
    // 页面渲染完成

  },
  onShow: function() {
    // 页面显示
         //获取用户的登录信息
         if (app.globalData.hasLogin) {
          let userInfo = wx.getStorageSync('userInfo');
          this.setData({
            userInfo: userInfo,
            userLevelId:userInfo.userLevelId,
            username1:userInfo.username,
            hasLogin: true
          });
    
          let that = this;
          util.request(api.UserIndex).then(function (res) {
            if (res.errno === 0) {
              that.setData({
                order: res.data.order,
                totalAmount: res.data.totalAmount,
                remainAmount: res.data.remainAmount,
                couponCount: res.data.couponCount
              });
            }
          });
        }
    wx.showLoading({
      title: '加载中...',
    });
    try {
      var cartId = wx.getStorageSync('cartId');
      if (cartId) {
        this.setData({
          'cartId': cartId
        });
      }
      var addressId = wx.getStorageSync('addressId');
      if (addressId) {
        this.setData({
          'addressId': addressId
        });
      }
      var couponId = wx.getStorageSync('couponId');
      if (couponId) {
        this.setData({
          'couponId': couponId
        });
      }
      var grouponRulesId = wx.getStorageSync('grouponRulesId');
      if (grouponRulesId) {
        this.setData({
          'grouponRulesId': grouponRulesId
        });
      }
      var grouponLinkId = wx.getStorageSync('grouponLinkId');
      if (grouponLinkId) {
        this.setData({
          'grouponLinkId': grouponLinkId
        });
      }
    } catch (e) {
      console.log(e);
    }

    this.getCheckoutInfo();
    this.fanquan();
  },
  onHide: function() {
    // 页面隐藏

  },
  onUnload: function() {
    // 页面关闭

  },
    //打开输入弹窗
    showRule: function () {
      this.setData({
        isRuleTrue: true
      })
    },
  
    //关闭输入弹窗
    hideRule: function () {
      this.setData({
        isRuleTrue: false,
        payPass:''
      })
    },
    clearInput: function (e) {
      this.setData({
        isRuleTrue: false
      })
  
      switch (e.currentTarget.id) {
        case 'clear-username':
          this.setData({
            username: ''
          });
          break;
        case 'clear-password':
          this.setData({
            password: ''
          });
          break;
        case 'clear-confirm-password':
          this.setData({
            confirmPassword: ''
          });
          break;
        case 'clear-code':
          this.setData({
            code: ''
          });
          break;
      }
    },
    payPass:function(e){
    this.setData({
      payPass: e.detail.value
    },100);
    if(e.detail.cursor>6){
      wx.showModal({
          title: '密码必须六位数',
          showCancel: false
     
      })
    }
    },
    mobileInput: function(e) {
      console.log(e)
      let dataSource = this.data.dataSource;
      let curInpArr = e.detail.value.split('');
      let curInpArrLength = curInpArr.length;
        for (let i = 0; i < dataSource.length - curInpArrLength; i++){
          curInpArr.push('');
        }
        for (let i = 0; i < this.data.dataSource.length; i++) {
          let initValue = 'dataSource[' + i + '].initValue';1
          this.setData({
            [initValue]: curInpArr[i]
          });
        }
        this.setData({
          payPass: e.detail.value
        },100);
     
    },
  submitOrder1: function(){
    var that=this
    var coupon=that.data.coupon
    if(coupon==1){
      wx.request({ 
        url: api.whetherToSet,
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded",
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
       data:{},
       success:function(res){
         if(res.data.code==1){
          that.setData({
            isRuleTrue: true
          })
         }else if(res.data.code==-1){
          wx.showModal({
            title: '提示',
            content: '支付密码未设置，请先设置支付密码',
            success (res) {
              if (res.confirm) {
                console.log('用户点击确定')
                wx.navigateTo({
                  url: '/pages/ucenter/password/password?newpayPass=1',
                })
              }
            }
          })
         }else{
           util.showErrorToast('请求异常')
         }
        
       },
       fail:function(res){
        console.log(res)
       },
      })
 
    }else if(coupon==''){
        this.submitOrder();
    }
  },
  queding:function(){
    var that=this
    util.jc_LoadShow("加载中...");
    util.request(api.pasVerification, {
      payPassword:that.data.payPass
    }, 'POST').then(res => {
      util.jc_LoadHide();
      that.hideRule();
      if (res.errno === 0) {
        that.submitOrder();
      }else if(res.errno==-2){
      util.showErrorToast("支付密码错误")
      }else if(res.errno==-1){
        wx.showModal({
          title: '提示',
          content: '支付密码未设置，请先设置支付密码',
          success (res) {
            if (res.confirm) {
              wx.navigateTo({
                url: '/pages/ucenter/password/password?newpayPass=1',
              })
            }
          }
        })
      }
    }
  )
 
  },
  submitOrder: function() {
    if (this.data.addressId <= 0) {
      util.showErrorToast('请选择收货地址');
      return false;
    }
    util.jc_LoadShow("正在下单，请稍后...");
    let nowTime = + new Date();
    if (nowTime - lastTime > 5000 || !lastTime) { //5秒内避免重复提交订单
      lastTime = nowTime;
    } else {
      return false;
    }
    util.request(api.OrderSubmit, {
      cartId: this.data.cartId,
      addressId: this.data.addressId,
      couponId: this.data.couponId,
      message: this.data.message,
      grouponRulesId: this.data.grouponRulesId,
      grouponLinkId: this.data.grouponLinkId,
      status:this.data.status
    }, 'POST').then(res => {
      console.log(res)
      util.jc_LoadHide();
      if (res.errno === 0) {
        
        // 下单成功，重置couponId
        try {
          wx.setStorageSync('couponId', 0);
        } catch (error) {

        }
        const orderId = res.data.orderId;
        if(res.data.payStatus==1){
          console.log("支付过程成功");
                wx.redirectTo({
                  url: '/pages/payResult/payResult?status=1&orderId=' + orderId
                });
        }else if(res.data.payStatus==0 ){
        util.request(api.OrderPrepay, {
          orderId: orderId
        }, 'POST').then(function(res) {
          if (res.errno === 0) {
            const payParam = res.data;
            console.log("支付过程开始");
            wx.requestPayment({
              'timeStamp': payParam.timeStamp,
              'nonceStr': payParam.nonceStr,
              'package': payParam.packageValue,
              'signType': payParam.signType,
              'paySign': payParam.paySign,
              'success': function(res) {
                console.log("支付过程成功");
                wx.redirectTo({
                  url: '/pages/payResult/payResult?status=1&orderId=' + orderId
                });
              },
              'fail': function(res) {
                console.log("支付过程失败");
                wx.redirectTo({
                  url: '/pages/payResult/payResult?status=0&orderId=' + orderId
                });
              },
              'complete': function(res) {
                console.log("支付过程结束")
              }
            });
          } else {
            wx.redirectTo({
              url: '/pages/payResult/payResult?status=0&orderId=' + orderId
            });
          }
        });

      } else {
        wx.redirectTo({
          url: '/pages/payResult/payResult?status=0&orderId=' + orderId
        });
      }
                
    }
    });
  },
  fanquan:function(res){
    var _this=this
    util.request(api.checkBalances, null, 'POST').then(function(res) {
      wx.hideLoading();
      console.log(res)
      _this.setData({
          hotel_voucher:res.data.hotelVoucher,
          red_envelopes:res.data.redEnvelopes,
          incentive_voucher:res.data.incentiveVoucher
        })
    })
  },
  C_password:function(e){
    console.log(e)
    var that=this
    var type=e.currentTarget.dataset.password
    if(type==0){
      that.setData({
        C_password:1
      })
    }else if(type==1){
      that.setData({
        C_password:0
      })
    }
  
  },
});