var util = require('../../utils/util.js');
var api = require('../../config/api.js');
var user = require('../../utils/user.js');

var app = getApp();

Page({
  data: {
    isMultiOrderModel:0,
    cartGoods: [],
    brandCartgoods:[],
    cartTotal: {
      "goodsCount": 0,
      "goodsAmount": 0.00,
      "checkedGoodsCount": 0,
      "checkedGoodsAmount": 0.00
    },
    isEditCart: false,
    checkedAllStatus: true,
    editCartList: [],
    hasLogin: false,
    hdatahieht:'',
    hdatatop:'',
    goodsMoq:'',
    addressList:'',
    number_pop:false,
    pop_productid:'',
    pop_number:'',
    pop_goodsmoq:'',
    userLevelId:''
  },
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
  },
  onReady: function() {
    // 页面渲染完成
  },
  onPullDownRefresh() {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    this.getCartList();
    wx.hideNavigationBarLoading() //完成停止加载
    wx.stopPullDownRefresh() //停止下拉刷新
  },
  onShow: function() {
    // 页面显示
    if (app.globalData.hasLogin) {
      this.getAddressList()
    }

    this.setData({
      hasLogin: app.globalData.hasLogin
    });
    let that=this
    var hdatahieht=wx.getStorageSync('hdatahieht')
    var hdatatop=wx.getStorageSync('hdatatop')
    var userLevelId=wx.getStorageSync('userLevelId')
      that.setData({
        hdatahieht:hdatahieht,
        hdatatop:hdatatop,
        userLevelId:userLevelId
      });
  },
  
   //查询用户默认地址
   getAddressList (){
    let that = this;
    util.request(api.AddressList).then(function (res) {
      let addressList = res.data.find(item => {
        return item.isDefault = true
      })
      that.setData({
        addressList:addressList
      })
      that.getCartList();
      if(!that.data.addressList){
        that.setData({
          addressList:''
        })
        that.isNoAddress()
      }
      
      console.log(that.data.addressList)
    });
  },
  onHide: function() {
    // 页面隐藏
  },
  onUnload: function() {
    // 页面关闭
  },
  goLogin() {
    wx.navigateTo({
      url: "/pages/auth/login/login"
    });
  },
  getCartList: function() {
    let addressId = this.data.addressList === undefined ? -1 : this.data.addressList.id
    let that = this;
    util.request(api.CartList+'?addressId='+addressId).then(function(res) {
      if (res.errno === 0) {
        if (res.data.isMultiOrderModel === 1){
          that.setData({
            isMultiOrderModel: res.data.isMultiOrderModel,
            brandCartgoods: res.data.brandCartgoods,
            cartTotal: res.data.cartTotal,
          });
        } else {
          that.setData({
            isMultiOrderModel: res.data.isMultiOrderModel,
            cartGoods: res.data.cartList,
            cartTotal: res.data.cartTotal
          });
        }
        that.setData({
          checkedAllStatus: that.isCheckedAll()
        });
      }
    });
  },
  isChildCheckedAll: function (cartList){
    return cartList.every(function (element, index, array) {
      if (element.checked == true) {
        return true;
      } else {
        return false;
      }
    });
  },
  isCheckedAll: function() { 
    let that = this;
    if (that.data.isMultiOrderModel === 1){
      //多店铺模式的商品全选判断  
      return that.data.brandCartgoods.every(function (element, index, array) {
        if (that.isChildCheckedAll(element.cartList)){
          return true;
        } else {
          return false;
        }
      });
    } else {
      //判断购物车商品已全选
      return that.data.cartGoods.every(function (element, index, array) {
        if (element.checked == true) {
          return true;
        } else {
          return false;
        }
      });
    }
  },
  doCheckedAll: function() {
    let checkedAll = this.isCheckedAll()
    this.setData({
      checkedAllStatus: this.isCheckedAll()
    });
  },
  getProductChecked:function(productId){
    let that = this;
    let isChecked = null;
    if (that.data.isMultiOrderModel === 1) {
      that.data.brandCartgoods.forEach(function (v) {
        let cartList = v.cartList;
        cartList.forEach(function(o){
          if (o.productId === productId) {
            isChecked = o.checked ? 0 : 1;
          }
        });
      });
    } else {
      that.data.cartGoods.forEach(function(o){
        if (o.productId === productId) {
          isChecked = o.checked ? 0 : 1;
        }
      });
    }
    return isChecked;
  },
  checkedItem: function(event) {
    //let itemIndex = event.target.dataset.itemIndex;
    let addressId = this.data.addressList === undefined ? -1 : this.data.addressList.id
    let productId = event.currentTarget.dataset.productid;
    let that = this;
    let productIds = [];
    productIds.push(productId);
    let isChecked = that.getProductChecked(productId);
    if (!this.data.isEditCart) {
      util.request(api.CartChecked, {
        productIds: productIds,
        isChecked: isChecked,
        addressId: addressId
      }, 'POST').then(function(res) {
        if (res.errno === 0) {
          if (res.data.isMultiOrderModel === 1) {
            that.setData({
              isMultiOrderModel: res.data.isMultiOrderModel,
              brandCartgoods: res.data.brandCartgoods,
              cartTotal: res.data.cartTotal
            });
          } else {
            that.setData({
              isMultiOrderModel: res.data.isMultiOrderModel,
              cartGoods: res.data.cartList,
              cartTotal: res.data.cartTotal
            });
          }
        }
        that.setData({
          checkedAllStatus: that.isCheckedAll()
        });
      });
    } else {
      //编辑状态
      if (that.data.isMultiOrderModel === 1) {
        let tmpBrandCartData = this.data.brandCartgoods.map(function (element, index, array) {
          let tmpBrandGoods = element.cartList.map(function (childEle,childIndex,childArr){
            if (childEle.productId === productId) {
              childEle.checked = !childEle.checked;
             }
            return childEle;
          });
          element.cartList = tmpBrandGoods;
          return element;
        });

        that.setData({
          brandCartgoods: tmpBrandCartData,
          checkedAllStatus: that.isCheckedAll(),
          'cartTotal.checkedGoodsCount': that.getCheckedGoodsCount()
        });
      } else {
        let tmpCartData = this.data.cartGoods.map(function (element, index, array) {
          if (element.productId === productId) {
            element.checked = !element.checked;
          }
          return element;
        });

        that.setData({
          cartGoods: tmpCartData,
          checkedAllStatus: that.isCheckedAll(),
          'cartTotal.checkedGoodsCount': that.getCheckedGoodsCount()
        });
      }
    }
  },
  getCheckedGoodsCount: function() {
    let that = this;
    let checkedGoodsCount = 0;
    if (that.data.isMultiOrderModel === 1) {
      that.data.brandCartgoods.forEach(function (v) {
        v.cartList.forEach(function (o){
          if (o.checked === true) {
            checkedGoodsCount += o.number;
          }
        });
      });
    } else {
      that.data.cartGoods.forEach(function (v) {
        if (v.checked === true) {
          checkedGoodsCount += v.number;
        }
      });
    }
    return checkedGoodsCount;
  },
  checkedAll: function() {
    let that = this;
    let addressId = this.data.addressList === undefined ? -1 : this.data.addressList.id
    if (!this.data.isEditCart) {
      let productIds = [];
      if (that.data.isMultiOrderModel === 1) {
        that.data.brandCartgoods.forEach(function (v) {
          v.cartList.forEach(function (o) {
            // if(o.inStock){
              productIds.push(o.productId);
            // }
          });
        });
      } else {
        var productIds = that.data.cartGoods.map(function (v) {
          return v.productId;
        });
      }

      util.request(api.CartChecked, {
        productIds: productIds,
        isChecked: that.isCheckedAll() ? 0 : 1,
        addressId: addressId
      }, 'POST').then(function(res) {
        if (res.errno === 0) {
          if (res.data.isMultiOrderModel === 1) {
            that.setData({
              isMultiOrderModel: res.data.isMultiOrderModel,
              brandCartgoods: res.data.brandCartgoods,
              cartTotal: res.data.cartTotal
            });
          } else {
            that.setData({
              isMultiOrderModel: res.data.isMultiOrderModel,
              cartGoods: res.data.cartList,
              cartTotal: res.data.cartTotal
            });
          }
        }
        that.setData({
          checkedAllStatus: that.isCheckedAll()
        });
      });
    } else {
      //编辑状态,将所有
      let checkedAllStatus = that.isCheckedAll();

      if (that.data.isMultiOrderModel === 1) {
        let tmpBrandCartData = this.data.brandCartgoods.map(function (element, index, array) {
          let tmpBrandGoods = element.cartList.map(function (childEle, childIndex, childArr) {
            childEle.checked = !checkedAllStatus;
            return childEle;
          });
          element.cartList = tmpBrandGoods;
          return element;
        });

        that.setData({
          brandCartgoods: tmpBrandCartData,
          checkedAllStatus: that.isCheckedAll(),
          'cartTotal.checkedGoodsCount': that.getCheckedGoodsCount()
        });
      } else {
        let tmpCartData = this.data.cartGoods.map(function (element, index, array) {
          element.checked = !checkedAllStatus;
          return element;
        });

        that.setData({
          cartGoods: tmpCartData,
          checkedAllStatus: that.isCheckedAll(),
          'cartTotal.checkedGoodsCount': that.getCheckedGoodsCount()
        });
      }
    }
  },
  editCart: function() {
    var that = this;
    if (this.data.isEditCart) {
      this.getCartList();
      this.setData({
        isEditCart: !this.data.isEditCart
      });
    } else {
      //编辑状态
      if (that.data.isMultiOrderModel === 1) {
        let tmpBrandCartData = that.data.brandCartgoods.map(function (element, index, array) {
          let tmpBrandGoods = element.cartList.map(function (childEle, childIndex, childArr) {
            childEle.checked = false;
            return childEle;
          });
          element.cartList = tmpBrandGoods;
          return element;
        });

        that.setData({
          brandCartgoods: tmpBrandCartData,
          isEditCart: !that.data.isEditCart,
          checkedAllStatus: that.isCheckedAll(),
          'cartTotal.checkedGoodsCount': that.getCheckedGoodsCount()
        });
      } else {
        let tmpCartData = that.data.cartGoods.map(function (element, index, array) {
          element.checked = false;
          return element;
        });

        that.setData({
         // editCartList: this.data.cartGoods,
          cartGoods: tmpCartList,
          isEditCart: !that.data.isEditCart,
          checkedAllStatus: that.isCheckedAll(),
          'cartTotal.checkedGoodsCount': that.getCheckedGoodsCount()
        });
      }
    }
  },

  updateCart: function(productId, goodsId, number, id) {
    let that = this;
    util.request(api.CartUpdate, {
      productId: productId,
      goodsId: goodsId,
      number: number,
      id: id
    }, 'POST').then(function(res) {
      that.setData({
        checkedAllStatus: that.isCheckedAll()
      });
    });

  },
  getProductItem: function (productId){
    let that = this;
    let productItem = null;
    if (that.data.isMultiOrderModel === 1) {
      that.data.brandCartgoods.forEach(function (v) {
        let cartList = v.cartList;
        cartList.forEach(function (o) {
          if (o.productId === productId) {
            productItem = o;
          }
        });
      });
    } else {
      that.data.cartGoods.forEach(function (o) {
        if (o.productId === productId) {
          productItem = o;
        }
      });
    }
    return productItem;
  },
  setProductItem: function (cartItem,productId){
    let that = this;
    if (that.data.isMultiOrderModel === 1) {
      let tmpBrandCartData = this.data.brandCartgoods.map(function (element, index, array) {
        let tmpBrandGoods = element.cartList.map(function (childEle, childIndex, childArr) {
          if (childEle.productId === productId) {
            return cartItem;
          } else {
            return childEle;
          }
        });
        element.cartList = tmpBrandGoods;
        return element;
      });

      that.setData({
        brandCartgoods: tmpBrandCartData,
        checkedAllStatus: that.isCheckedAll(),
        'cartTotal.checkedGoodsCount': that.getCheckedGoodsCount()
      });
    } else {
      let tmpCartData = this.data.cartGoods.map(function (element, index, array) {
        if (element.productId === productId) {
          return cartItem;
        } else {
          return element;
        }
      });
      that.setData({
        cartGoods: tmpCartData,
        checkedAllStatus: that.isCheckedAll(),
        'cartTotal.checkedGoodsCount': that.getCheckedGoodsCount()
      });
    }
  },
  cutNumber: function(event) {
    //let itemIndex = event.target.dataset.itemIndex;
    let productId = event.currentTarget.dataset.productid;
    let goodsmoq = event.currentTarget.dataset.goodsmoq
    let cartItem = this.getProductItem(productId);
    
    //let number1 = event.currentTarget.dataset.number // 
    let number = (cartItem.number - 1 > goodsmoq) ? cartItem.number - 1 : goodsmoq;
    if(number == goodsmoq){
      wx.showToast({
        image: 'https://jczh.jiachang8.com/images/icon_error.png',
        title: '已是最低起订量'
      });
    }
    cartItem.number = number;
    this.setData({
      pop_number:cartItem.number
    })
    this.setProductItem(cartItem, productId);
    this.updateCart(cartItem.productId, cartItem.goodsId, number, cartItem.id);
  },
  number_popInput(e){
  
    this.setData({
      pop_number:e.detail.value
    })
  },
  addNumber: function(event) {
   // let itemIndex = event.target.dataset.itemIndex;
    let productId = event.currentTarget.dataset.productid;
    let cartItem = this.getProductItem(productId);

    let number = cartItem.number + 1;
    cartItem.number = number;
    this.setData({
      pop_number:cartItem.number
    })
    this.setProductItem(cartItem, productId);
    this.updateCart(cartItem.productId, cartItem.goodsId, number, cartItem.id);
  },
  number_pop:function(e){
    console.log(e)
    var pop_productid=e.currentTarget.dataset.productid
    var pop_number=e.currentTarget.dataset.number
    var pop_goodsmoq=e.currentTarget.dataset.goodsmoq
    var that=this
    that.setData({
      number_pop:true,
      pop_productid:pop_productid,
      pop_number:pop_number,
      pop_goodsmoq:pop_goodsmoq
    })
  },
  pop_cancel:function(){
    var that=this
    let productId = this.data.pop_productid
    let cartItem = this.getProductItem(productId);

    let number = cartItem.number;
    cartItem.number = number;
    this.setProductItem(cartItem, productId);
    this.updateCart(cartItem.productId, cartItem.goodsId, number, cartItem.id);

    that.setData({
      number_pop:false,
    })
  },
  pop_confirm:function(){
    var that=this
    var that=this
    let productId = this.data.pop_productid
    let cartItem = this.getProductItem(productId);

    let number = this.data.pop_number;
    cartItem.number = number;
    this.setProductItem(cartItem, productId);
    this.updateCart(cartItem.productId, cartItem.goodsId, number, cartItem.id);
    that.setData({
      number_pop:false
    })
  },
  checkoutOrder: function() {
    //获取已选择的商品
    let that = this;
    let isNoAddress = []
    let addressId = this.data.addressList === undefined ? -1 : this.data.addressList.id
    // var checkedGoods = this.data.cartGoods.filter(function(element, index, array) {
    //   console.log(element, index, array)
    //   if (element.checked == true) {
    //     return true;
    //   } else {
    //     return false;
    //   }
    // });
    
    // if (checkedGoods.length <= 0) {
    //   return false;
    // }*/

    // 如果用户没有填写地址，什么都不做
    var userLevelId=that.data.userLevelId
    if(userLevelId==12){
      wx.showModal({
        itle: '提示',
        content: '员工账户没有购物权限',
        success (res) {
        if (res.confirm) {
        wx.switchTab({
          url: '/pages/topic/topic',
        })
        } else if (res.cancel) {
          wx.switchTab({
            url: '/pages/topic/topic',
          })
        }
      }
      })
      return
    }else
    if(!this.data.addressList){
      this.isNoAddress()
      return
    }
   
    if(that.getCheckedGoodsCount() <= 0){
      wx.showModal({
        title: '错误信息',
        content: '请勾选需要下单的商品！',
        showCancel: false
      });
      return false;
    }

    this.data.brandCartgoods.forEach(item => {
      item.cartList.forEach(item =>{
        if((!item.inStock && item.checked)){
          isNoAddress.push(item.productId)
        }
      })
    })
    // 判断用户选择的商品，有没有无货的
    if(isNoAddress.length > 0){
      wx.showModal({
        title: '商品无货',
        content: '点击确定移除无货商品',
        success (res) {
          if (res.confirm) {
            that._CartDelete(isNoAddress,addressId)
          }
        }
      })
    }else{
      try {
        wx.setStorageSync('cartId', 0);
        wx.navigateTo({
          url: '/pages/checkout/checkout'
        })
      } catch (e) {}
    }
    
    
    // let integer = []
    // this.data.brandCartgoods.forEach(item => {
    //   item.cartList.forEach(item =>{
    //     if((item.checked === true)){
    //       integer.push(item.goodsId)
    //     }
    //   })
    // })
    
    // util.request(api.CartInStock+'?goods='+integer.toString()+'&addressId='+that.data.addressList.id,null,'POST').then(res => {
    //   console.log(res)
    // })

    // util.request(api.CartInStock, {
    //   goods:integer.toString(),
    //   addressId:that.data.addressList.id
    // }, 'POST').then(res => {
    //   console.log(res)
    // })
    
    // storage中设置了cartId，则是购物车购买
 

  },
  deleteCart: function() {
    //获取已选择的商品
    let that = this;
    let addressId = this.data.addressList === undefined ? -1 : this.data.addressList.id
    /*let productIds = this.data.cartGoods.filter(function(element, index, array) {
      if (element.checked == true) {
        return true;
      } else {
        return false;
      }
    });

    if (productIds.length <= 0) {
      return false;
    }*/

    if (that.getCheckedGoodsCount() <= 0) {
      wx.showModal({
        title: '错误信息',
        content: '请勾选需要删除的商品！',
        showCancel: false
      });
      return false;
    }

    let productIds = [];
    if (that.data.isMultiOrderModel === 1) {
      that.data.brandCartgoods.forEach(function (v) {
        v.cartList.forEach(function (o) {
          if (o.checked == true){
            productIds.push(o.productId);
          }
        });
      });
    } else {
      productIds = that.data.cartGoods.map(function (v) {
        if (v.checked == true) {
          return v.productId;
        }
      });
    }
    that._CartDelete(productIds,addressId)
    
  },
  
  /*
    @method: 删除购物车商品接口
    @param productIds:需要删除商品的 id 数组；addressId:用户地址id
  */
  _CartDelete(productIds,addressId){
    let that = this
    util.request(api.CartDelete, {
      productIds: productIds,
      addressId: addressId
    }, 'POST').then(function(res) {
      if (res.errno === 0) {
        if (res.data.isMultiOrderModel === 1) {
          that.setData({
            isMultiOrderModel: res.data.isMultiOrderModel,
            brandCartgoods: res.data.brandCartgoods,
            cartTotal: res.data.cartTotal
          });
        } else {
          that.setData({
            isMultiOrderModel: res.data.isMultiOrderModel,
            cartGoods: res.data.cartList,
            cartTotal: res.data.cartTotal
          });
        }

        that.setData({
          checkedAllStatus: that.isCheckedAll()
        });
      }
    });
  },
  addressAddOrUpdate (event) {
    wx.navigateTo({
      url: '/pages/ucenter/addressAdd/addressAdd?id=' + 0
    })
  },

  isNoAddress(){
    wx.showModal({
      title: '未填地址',
      content: '点击确定新建地址',
      success (res) {
      if (res.confirm) {
        wx.navigateTo({
          url: '/pages/ucenter/addressAdd/addressAdd?id=' + 0,
        })
      }
      }
    })
  },
  selectAddress() {
    wx.navigateTo({
      url: '/pages/ucenter/address/address',
    })
  },
  goodsdet:function(e){
    if(!this.data.isEditCart){
      var id=e.currentTarget.dataset.id
      wx.navigateTo({
        url: '/pages/goods/goods?id=' + id,
      })
    }

  }
})