// pages/platformMana/hotelManalist/hotelManalist.js
var util = require("../../../utils/util")
var api = require("../../../config/api")
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tab: [{
        name: "酒店合同"
      },
      {
        name: "合伙人合同"
      },
      {
        name: "供应商合同"
      },
    ],
    active: '酒店合同',
    contractlist: [

    ],
    partner:[],
    supplier:[],
    pageSize: '',
    status: '',
    list: '',
    page: 1,
    page1:1,
    errmsg:'',
    userId:'',
    hotelCount:'',
    hotelSum: '',
    partnerCount: '',
    partnerSum: '',
    supplierCount: '',
    userLevelId:'',
    partnershow:false,
    supplierselect:[
      {name:"姓名",id:'0'},
      {name:"电话",id:'1'},
      {name:"公司",id:'2'},
    ],
    supplierselectindex:0,
    hotelselect:[
      {name:"酒店名",id:'0'},
      {name:"酒店主体",id:'1'},
    ],
    hotelselectindex:0,
    partnerselect:[
      {name:"姓名",id:'0'},
      {name:"电话",id:'1'},
      {name:"公司",id:'2'},
    ],
    partnerselectindex:0,
    searchInput:'',
     url_cx:'',
     type:'',
     pull_down:'',
     lists:[]
  },
  supplierselectChange:function(e){
    console.log(e.detail.value)
    this.setData({
      supplierselectindex: e.detail.value
    })
  },
  hotelselectChange:function(e){
    console.log(e.detail.value)
    this.setData({
      hotelselectindex: e.detail.value
    })
  },
 
  searchInput(e){
    // console.log(e)
    this.setData({
      searchInput: e.detail.value
    })
  },
  current: function (x) {
    // console.log(x)
    var that = this
    that.setData({
      active: x.currentTarget.dataset.name
    })

    if(x.currentTarget.dataset.name=='合伙人合同'){
      that.partner();
    }
    if(x.currentTarget.dataset.name=='供应商合同'){
      that.supplier();
    }
  },
  check: function (res) {
    // console.log(res)
    var status = res.currentTarget.dataset.status
    var id = res.currentTarget.dataset.id
    var contractlist = this.data.contractlist
    var that = this
    if (status != 3) {
      wx.navigateTo({
        url: '/pageA/pages/platformMana/check/check?id=' + id + '&contract_type='+ that.data.active,
      })
    } else {
      util.showErrorToast('合同已驳回')
    }
  },
  check_partner: function (res) {
    // console.log(res)
    var status = res.currentTarget.dataset.status
    var id = res.currentTarget.dataset.id
    var that = this
    if (status != 3) {
      wx.navigateTo({
        url: '/pageA/pages/platformMana/check/check?id=' + id+ '&contract_type='+ that.data.active,
      })
    } else {
      util.showErrorToast('合同已驳回')
    }
    
  },
  
  delete: function (e) {

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   

  },
  switch1Change: function (e) {

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  copy: function(e) {
    util.copy(e.target.dataset.copy)
  },
  search:function(e){
    let that=this
    let active=that.data.active
    if(active=='酒店合同'){
      // var url_cx=api.hotelContract_cx
      // var type= that.data.supplierselectindex
      that.setData({
        url_cx:api.hotelContract_cx,
        type:that.data.hotelselectindex,
        active:active
      })
      that.search_qingqiu();
      return;
    }else if(active=='合伙人合同'){
      // var url_cx=api.partnerContract_cx
      // var type= that.data.hotelselectindex
      that.setData({
        url_cx:api.partnerContract_cx,
        type:0,
        active:active
      })
      that.search_qingqiu();
      return;
    }else if(active=='供应商合同'){
    //  var url_cx=api.supplierContract_cx
    //  var type=0
     that.setData({
      url_cx:api.supplierContract_cx,
      type:that.data.supplierselectindex,
      active:active
    })
    that.search_qingqiu();
    return;
    }
  
  },
  search_qingqiu:function(){
    let  that=this
    var active=that.data.active
    wx.request({
      url: that.data.url_cx,
      data:{
        type:that.data.type,
        text:that.data.searchInput,
        page:1,
        pageSize:10
      },
      method:'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function(res) {
        // console.log(res)
        if(res.data.code==0){
          if(active=='酒店合同'){
            that.setData({
              contractlist: res.data.data,
              pull_down:1
            });
          }else if(active=='合伙人合同'){
            that.setData({
              partner: res.data.data,
              pull_down:1
            });
          }else if(active=='供应商合同'){
            that.setData({
              supplier: res.data.data,
              pull_down:1
            });
          }
        }else{
          wx.showToast({
            title: '请求失败',
            icon:'none'
          })
        }
    
      },fail:function(e){
        console.log(res)
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    //获取用户的登录信息
    var hasLogin=wx.getStorageSync('hasLogin')
    if (hasLogin) {
      let userInfo = wx.getStorageSync('userInfo');
      // let username1=wx.getStorageSync('username');
  
      that.setData({
        userInfo: userInfo,
        userLevelId:userInfo.userLevelId,
        username1:userInfo.username,
        hasLogin: true,
        userId:userInfo.userId,
      });
      if(userInfo.userLevelId==2||userInfo.userLevelId==3||userInfo.userLevelId==4||userInfo.userLevelId==5){
        that.setData({
          partnershow:true
        })
      }
    }else{
      wx.navigateTo({
        url: '/pages/auth/login/login',
      })
    }
   that.list();
   that.performance();
  },
  list:function(){
    var that = this
    wx.request({
      url: api.c_hotelContract,
      data: {
        page: 1,
        pageSize: 10
      },
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        if (res.data.code == 0) {
          that.setData({
            contractlist: res.data.data
          })
        }else  if(res.data.errmsg=="权限不正确"){
          that.setData({
            errmsg:"权限不正确"
          })
          util.showErrorToast('权限不正确')
        } else {
          util.showErrorToast('请求异常')
        }

      },
      fail: function (res) {
        console.log(res)
      }
    })
  },
  supplier:function(){
    var that = this
    wx.request({
      url: api.supplier_selectByIdAndAll,
      data: {
        page: 1,
        pageSize: 10
      },
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        if (res.data.code == 0) {
          that.setData({
            supplier: res.data.data
          })
        } else {
          util.showErrorToast('请求异常')
        }

      },
      fail: function (res) {
        console.log(res)
      }
    })
  },
  partner:function(){
    var that = this
    wx.request({
      url: api.partner_selectByIdAndAll,
      data: {
        page: 1,
        pageSize: 10
      },
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        // console.log(res)
        if (res.data.code == 0) {
          that.setData({
            partner: res.data.data
          })
        } else {
          util.showErrorToast('请求异常')
        }

      },
      fail: function (res) {
        console.log(res)
      }
    })
  },
  performance:function(){
    var that=this
    wx.request({
      url: api.statistics,
      data:{
        userId:that.data.userId,
        startTime:'',
        endTime:'',
        page:1,
        pageSize:1
      },
      method:'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function(res) {
        console.log(res)
        if(res.data.code==0){
          that.setData({
            hotelCount:res.data.hotelCount,
            hotelSum: res.data.hotelSum,
            partnerCount: res.data.partnerCount,
            partnerSum: res.data.partnerSum,
            supplierCount: res.data.supplierCount
          })
        }
      },
      fail:function(res){
        console.log(res)
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    let that=this
    setTimeout(function(){
      wx.stopPullDownRefresh({
        complete: (res) => {
          wx.showToast({
            title: '刷新成功',
            icon:'none'
          })
          that.setData({
            pull_down:'',
            type:'',
            searchInput:'',
          })
        },
      })
      that.onShow();
    },500)
 
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    var  pull_down=that.data.pull_down
    if(pull_down!=1){
    var url=''
    if(that.data.active=='酒店合同'){
      url=api.c_hotelContract
      var lists=that.data.contractlist
    }else if(that.data.active=='合伙人合同'){
      url=api.partner_selectByIdAndAll
      var lists=that.data.partner
    }else if(that.data.active=='供应商合同'){
      url=api.supplier_selectByIdAndAll
      var lists=that.data.supplier
    }
    var page = this.data.page
    page++;
    that.setData({
      page: page
    })
    // 显示加载图标
    wx.showLoading({
      title: '玩命加载中',
    })
    wx.request({
      url: url,
      data: {
        page: page,
        pageSize: 10
      },
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        if (res.data.code == 0) {
         var list=res.data.data
        } else {
          util.showErrorToast('请求异常')
        }
        var goods = list.concat(lists) // 为一进入页面请求完数据定义的集合
        if (list == '') {
          wx.showToast({
            title: '暂无更多',
            icon: 'none',
          })
          return
        } else {
          if(that.data.active=='酒店合同'){
            that.setData({
              contractlist: goods,
            });
          }else if(that.data.active=='合伙人合同'){
            that.setData({
              partner: goods,
            });
          }else if(that.data.active=='供应商合同'){
            that.setData({
              supplier: goods,
            });
          }
        }
        wx.hideLoading();
      },
    })
  }else if(pull_down==1){
    that.onReachBottom1();
  }
  },
    /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom1: function () {
    let that=this
    let active=that.data.active
      if(active=='酒店合同'){
      that.setData({
        url_cx:api.hotelContract_cx,
        type:that.data.hotelselectindex,
        active:active,
        lists:that.data.contractlist
      })
      that.qingq();
      return;
    }else if(active=='合伙人合同'){
      that.setData({
        url_cx:api.partnerContract_cx,
        type:0,
        active:active,
        lists:that.data.partner
      })
      that.qingq();
      return;
    }else if(active=='供应商合同'){
     that.setData({
      url_cx:api.supplierContract_cx,
      type:that.data.supplierselectindex,
      active:active,
      lists:that.data.supplier
    })
    that.qingq();

    return;
    }
  },
  qingq:function(){
    var that=this
    var page = that.data.page1
    let active=that.data.active
    let lists=that.data.lists
    page++;
    that.setData({
      page1: page
    })
    //显示加载图标
    wx.showLoading({
      title: '玩命加载中',
    })
    wx.request({
      url: that.data.url_cx,
      data: {
        type:that.data.type,
        text:that.data.searchInput,
        page: page,
        pageSize: 10
      },
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function(res) {
        console.log(res)
        wx.hideLoading();
        if(res.data.code == 0){
          var list=res.data.data
          var ll=list.length
          if(ll == 0 ){
            wx.showToast({
              title: '暂无更多',
              icon: 'none',
            })
            return;
          }else
          if(active=='酒店合同'){
            var newlist = list.concat(lists) // 为一进入页面请求完数据定义的集合
            that.setData({
              contractlist: newlist,
              pull_down:1
            });
          }else if(active=='合伙人合同'){
            var newlist = list.concat(lists) // 为一进入页面请求完数据定义的集合
            that.setData({
              partner: newlist,
              pull_down:1
            });
          }else if(active=='供应商合同'){
            var newlist = list.concat(lists) // 为一进入页面请求完数据定义的集合
            that.setData({
              supplier: newlist,
              pull_down:1
            });
          }
        }
      },fail:function(e){
        console.log(res)
        wx.showToast({
          title: '请求失败',
          icon:'none'
        })
      }

    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  addhotel: function () {
    wx.navigateTo({
      url: '/pageA/pages/platformMana/hotelMana/hotelMana?type=' + '0',
    })
  },
  addcontract:function(){
    var that=this
    var  active= that.data.active
    wx.navigateTo({
      url: '/pages/myContract/myContract?active=' + active,
    })
  },
  chakan1:function(res){
    var status = res.currentTarget.dataset.status
    var id = res.currentTarget.dataset.id
    var contractlist = this.data.contractlist
    var that = this
      wx.navigateTo({
        url: '/pageA/pages/platformMana/check/check?id=' + id + '&contract_type='+ that.data.active+ '&type='+ 1+ '&chakan='+ 1,
      })
  },
})