// pages/myContract/myContract.js
var util = require('../../utils/util.js');
var api = require('../../config/api.js');
var user = require('../../utils/user.js');
const QQMapWX = require('../../lib/qqmap-wx-jssdk.min.js');
// 实例化API核心类
var qqmapsdk = new QQMapWX({
  key: 'GVOBZ-APK6P-BOZDT-V5ZWX-MRI7T-6GF5H' // 必填
});
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tab: [{
        name: "酒店合同"
      },
      {
        name: "合伙人合同"
      },
      {
        name: "供应商合同"
      },
    ],
    // bt:'请选择地址',
    active: '合伙人合同',
    showIndex: 0,
    maskDataindex1: 0,
    maskDataindex2: 0,
    maskDataindex3: 0,
    maskDataindex4: 0,
    maskData: [

    ],
    zeng: [{
      id: '1'
    }, ],
    showAddBtn: 1,
    add: 1,
    // unit:0,
    unit1: 0,
    unit2: 0,
    unit3: 0,
    unit4: 0,
    amount1: '',
    amount2: '',
    amount3: '',
    amount4: '',
    Total_amount: 0,
    date: '',
    date1: '',
    period: [
      {
        value: 1,
        id: 0,
      },
      {
        value: 24,
        id: 1,
      },
      {
        value: 36,
        id: 2,
      },
  ],
  periodvalue:1,
  periodIndex:0,
    item: '',
    checked: '',
    region: ['例上海市', '上海市', '嘉定区'], //选择的地址
    supplier: ['例上海市', '上海市', '嘉定区'], //选择的地址
    packageAmt:0,//输入的套餐总金额
    hotelArea:'',//行政编号
    hotelzt: '', //酒店主题
    hotelname: '', //酒店名
    hoteladdress: '', //详细地址
    hotelprincipal: '', //酒店负责人
    hotelphone: '', //酒店电话
    yiprincipal: '', //乙方负责人
    yiphone: '', //乙方电话
    hotelroom: '', //安装的房间号
    additional: '', //附加条款
    hotelota: '', //酒店评分
    hotelroomnum: '', //房间数量
    hotelroomprice: '', //房间均价
    hotelroomarea: '', //房间平均面积
    hehuoren: '', //合伙人
    hehuorenphone: '', //合伙人电话
    packageID1: 0, //套餐ID
    packageID2: 0, //套餐ID
    packageID3: 0, //套餐ID
    packageID4: 0, //套餐ID
    Amt_or_roomnum: '', //总金额或者折算的房间数
    remarks1: '', //套餐备注
    remarks2: '', //套餐备注
    remarks3: '', //套餐备注
    remarks4: '', //套餐备注
    remarks5: '', //置换备注
    ajxtrue: false,
    ajxtrue1: false,
    contraAmt:0,
    zcontraAmt:'',
    i_price1:4999,
    i_price2:'',
    i_price3:'',
    i_price4:'',
    Partner_list:[
      {id:1,value:'普通合伙人',type:1},//type=1,传行政区三级
      {id:2,value:'区县总代合伙人',type:1},
      {id:3,value:'城市运营中心',type:2},//type=2,传行政区二级
      {id:4,value:'城市合伙人（分红)',type:2},
    ],
    Partner_listIndex:0,
    partnerLevel:'',
    citylist: [], //获取所有的城市列表
    province: [],
    Partner_area:0,
    P_code:'',
    P_code1:'',
    P_code2:'',
    partnerArea1:'',
    partnerArea2:'',
    partnerArea3:'',
    cityname: [],
    cityname1:[],
    cityname_area:0,
    county: [],
    county1:[],
    county_area:0,
    type:1,
    municipality:'',
    Join_gold:'',//加盟金
    Join_ticket:'',//置换酒店券
    partnerName:'',//合伙人名
    // partnerArea:'',//合伙人区域
    areaNumber:'',//区域行政区编号
    partnerLevel:'',//合伙人等级
    partner_additional:'',//合伙人附加条款
    partner_salesman:'',//业务员信息

    supplierRepr:'',//供应商名
    supplierCompany:'',//供应商公司名
    supplierAddress:'',//供应商地址
    supplierPhone:'',//供应商电话
    addtion:'',//附加条款
    servicePromotion:'',//推广服务费
    partnerSplit:'',//合伙人分成
    start_date:'2020-04-01',
    checkbox:'',
    supplier_addtion:''
  },

  current: function (x) {
    console.log(x)
    var that = this
    that.setData({
      active: x.currentTarget.dataset.name
    })
  },
  packageAmtinput(e){
    this.setData({
      packageAmt:e.detail.value
    })
  },
  bindPeriod(e){
    this.setData({
      periodvalue:e.detail.value
    })
  },
  panel: function (e) {
    this.setData({
      zcontraAmt:''
    })
    console.log(e)
    if (e.currentTarget.dataset.index != this.data.showIndex) {
      this.setData({
        showIndex: e.currentTarget.dataset.index,
      })
    } else {
      this.setData({
        showIndex: 0
      })
    }
    this.clear();
  },
  jia: function (e) {
    console.log(e)
    // console.log(e.currentTarget.dataset.id)
    var that = this;
    var id = e.currentTarget.dataset.id;
    var unit = e.currentTarget.dataset.unit;
    unit++;
    console.log(unit)
    that.setData({
      zcontraAmt:''
    })
    // var amount = price * unit
    //  console.log(amount)
    if (id == 1) {
      that.setData({
        unit1: unit,
        amount1: that.data.i_price1 * unit
      })
      return;
    }
    if (id == 2) {
      that.setData({
        unit2: unit,
        amount2: that.data.i_price2 * unit
      })
      return;
    }
    if (id == 3) {
      that.setData({
        unit3: unit,
        amount3: that.data.i_price3 * unit
      })
      return;
    }
    if (id == 4) {
      that.setData({
        unit4: unit,
        amount4: that.data.i_price4 * unit
      })
      return;
    }
  },
  jian: function (e) {
    var that = this
    var id = e.currentTarget.dataset.id
    var unit = e.currentTarget.dataset.unit;
    var index = e.currentTarget.dataset.index;
    var price = that.data.maskData[id].price;
    // var Total_amount=that.data.Total_amount;price
    unit--;
    var amount = price * unit
    that.setData({
      zcontraAmt:''
    })
    if (id == 1) {
      if (unit >= 0) {
        that.setData({
          unit1: unit,
          amount1: amount
        })
      } else {
        util.showErrorToast('错误操作')
      }
    } else if (id == 2) {
      if (unit >= 0) {
        that.setData({
          unit2: unit,
          amount2: amount
        })
      } else {
        util.showErrorToast('错误操作')
      }
    } else if (id == 3) {
      if (unit >= 0) {
        that.setData({
          unit3: unit,
          amount3: amount
        })
      } else {
        util.showErrorToast('错误操作')
      }
    } else if (id == 4) {
      if (unit >= 0) {
        that.setData({
          unit4: unit,
          amount4: amount
        })
      } else {
        util.showErrorToast('错误操作')
      }
    }
  },
  hotelzt(e) {
    this.setData({
      hotelzt: e.detail.value
    })
  },
  Total_amount(e) {
    this.setData({
      Total_amount: e.detail.value
    })
  },
  hotelname(e) {
    // console.log(e)
    this.setData({
      hotelname: e.detail.value
    })
  },
  hoteladdress(e) {
    this.setData({
      hoteladdress: e.detail.value
    })
  },
  hotelprincipal(e) {
    this.setData({
      hotelprincipal: e.detail.value
    })
  },
  hotelphone(e) {
    // this.setData({
    //   hotelphone: e.detail.value
    // })
    var hotelphone = e.detail.value;
    let that = this
    // if (!(/^1[3456789]\d{9}$/.test(hotelphone))) {

    //   this.setData({
    //     ajxtrue: false
    //   })
    //   if (hotelphone.length >= 11) {
    //     wx.showToast({
    //       title: '手机号有误',
    //       icon: 'none',
    //       duration: 2000
    //     })
    //   }
    // } else {
      this.setData({
        ajxtrue: true,
        hotelphone: e.detail.value
      })
    // }
  },
  yiprincipal(e) {
    this.setData({
      yiprincipal: e.detail.value
    })
  },
  yiphone(e) {
    var hotelphone = e.detail.value;
    let that = this
    // if (!(/^1[3456789]\d{9}$/.test(hotelphone))) {
    //   this.setData({
    //     ajxtrue1: false
    //   })
    //   if (hotelphone.length >= 11) {
    //     wx.showToast({
    //       title: '手机号有误',
    //       icon: 'none',
    //       duration: 2000
    //     })
    //   }
    // } else {
      this.setData({
        ajxtrue1: true,
        yiphone: e.detail.value
      })
    // }
  },
  hotelroom(e) {
    this.setData({
      hotelroom: e.detail.value
    })
  },
  additional(e) {
    this.setData({
      additional: e.detail.value
    })
  },
  hotelota(e) {
    this.setData({
      hotelota: e.detail.value
    })
  },
  hotelroomnum(e) {
    this.setData({
      hotelroomnum: e.detail.value
    })
  },
  hotelroomprice(e) {
    this.setData({
      hotelroomprice: e.detail.value
    })
  },
  hotelroomarea(e) {
    this.setData({
      hotelroomarea: e.detail.value
    })
  },
  Amt_or_roomnum(e) {
    this.setData({
      Amt_or_roomnum: e.detail.value
    })
  },
  remarks1(e) {
    // console.log('备注1')
    this.setData({
      remarks1: e.detail.value
    })
  },
  remarks2(e) {
    // console.log('备注2')
    this.setData({
      remarks2: e.detail.value
    })
  },
  remarks3(e) {
    // console.log('备注3')
    this.setData({
      remarks3: e.detail.value
    })
  },
  remarks4(e) {
    // console.log('备注4')
    this.setData({
      remarks4: e.detail.value
    })
  },
  bindTextAreaBlur(e) {
    // console.log(e)
    this.setData({
      remarks5: e.detail.value
    })
  },
  hehuoren(e) {
    // console.log(e)
    this.setData({
      hehuoren: e.detail.value
    })
  },
  bindPickerChange:function(e){
    console.log(e)
    var index=e.detail.value
    var periodvalue=this.data.period[index].value
    this.setData({
      periodIndex:index,
      periodvalue:periodvalue
    })
  },
  hehuorenphone(e) {
    var hotelphone = e.detail.value;
    let that = this
    if (!(/^1[3456789]\d{9}$/.test(hotelphone))) {
      this.setData({
        ajxtrue1: false
      })
      if (hotelphone.length >= 11) {
        wx.showToast({
          title: '手机号有误',
          icon: 'none',
          duration: 2000
        })
      }
    } else {
      this.setData({
        ajxtrue1: true,
        hehuorenphone: e.detail.value
      })
    }
  },

  bindRegionChange: function (e) {
    // console.log(e)
    this.setData({
      region: e.detail.value,
      hotelArea: e.detail.code[2]
    })
  },
  bindsupplierChange: function (e) {
    console.log(e)
    this.setData({
      supplier: e.detail.value
    })
  },
  bindDateChange: function (e) {
    this.setData({
      date: e.detail.value
    })
  },
  bindDateChange1: function (e) {
    this.setData({
      date1: e.detail.value,
    })
  },
  bindPickerChange1: function (e) {
    var index = e.currentTarget.dataset.index
    var id=e.detail.value
    var price = this.data.maskData[id].price
    var packageID = this.data.maskData[id].id
    // console.log(packageID)
    var unit = this.data.unit1
    this.setData({
      maskDataindex1: e.detail.value,
      amount1: price * unit,
      packageID1: packageID,
      i_price1:price,
      checked: false,
      zcontraAmt:''
    })
  },
  bindPickerChange2: function (e) {
    var index = e.currentTarget.dataset.index
    var id=e.detail.value
    var price = this.data.maskData[id].price
    var packageID = this.data.maskData[id].id
    // console.log(packageID)
    var unit = this.data.unit2
    this.setData({
      maskDataindex2: e.detail.value,
      amount2: price * unit,
      packageID2: packageID,
      i_price2:price,
      checked: false,
      zcontraAmt:''
    })
  },
  bindPickerChange3: function (e) {
    var index = e.currentTarget.dataset.index
    var id=e.detail.value
    var price = this.data.maskData[id].price
    var packageID = this.data.maskData[id].id
    // console.log(packageID)
    var unit = this.data.unit3
    this.setData({
      maskDataindex3: e.detail.value,
      amount3: price * unit,
      packageID3: packageID,
      i_price3:price,
      checked: false,
      zcontraAmt:''
    })
  },
  bindPickerChange4: function (e) {
    var index = e.currentTarget.dataset.index
    var id=e.detail.value
    var price = this.data.maskData[id].price
    var packageID = this.data.maskData[id].id
    // console.log(packageID)
    var unit = this.data.unit4
    this.setData({
      maskDataindex4: e.detail.value,
      amount4: price * unit,
      packageID4: packageID,
      i_price4:price,
       checked: false,
       zcontraAmt:''
    })
  },
  Partner_area:function(e){
    var that=this
    var index=e.detail.value
    var P_code=this.data.province[index].id
    var cityname=this.data.cityname
    var municipality=this.data.province[index].fullname
    var partnerArea=this.data.province[index].fullname
    var b=[]
    var list=[]
    for(var i=0;i<cityname.length;i++){
      var a= cityname[i]
      b= String(a.id).substring(0,2);
      var bianhao=b+'0000'
      if( b+'0000' == P_code){
        // console.log(i)
        list.push(cityname[i])
      }
    }
    if(that.data.type==2){
    if(municipality=="北京市"||municipality=="天津市"||municipality=="上海市"||municipality=="重庆市"){
        that.setData({
          municipality:'zxs'
        })
      }
    }
    if(list!=[]){
      that.setData({
        Partner_area:index,
        P_code: P_code,
        P_code1:'',
        partnerArea1:partnerArea,
        cityname1:list
      })
    }else{
      
    }
  },
  cityname_area:function(e){
    var that=this
    var index=e.detail.value
    var P_code=this.data.cityname1[index].id
    var county=this.data.county
    var partnerArea=this.data.cityname1[index].fullname
    var b=[]
    var list=[]
    for(var i=0;i<county.length;i++){
      var a= county[i]
      b= String(a.id).substring(0,4);
      if( b+'00' == P_code){
        list.push(county[i])
      }
    }
    this.setData({
      cityname_area:index,
      P_code1: P_code,
      partnerArea2:partnerArea,
      county1:list
    })
  },
  county_area:function(e){
    var that=this
    var index=e.detail.value
    var P_code=this.data.county1[index].id
    var partnerArea=this.data.county1[index].fullname
    this.setData({
      cityname_area:index,
      P_code2: P_code,
      partnerArea3:partnerArea
    })
  },
  Partner_list:function(e){
    var that=this
    var index=e.detail.value
    var ty=that.data.Partner_list[index].type
    var partnerLevel=that.data.Partner_list[index].value
    that.setData({
      type:ty,
      Partner_listIndex:e.detail.value,
      partnerLevel:partnerLevel
    })
  },
  checkboxChange: function (e) {
    var that=this
    if(e.detail.value!=2){
      that.setData({
        contraAmt:1,
        checked: true,
        checkbox:e.detail.value,
      })
    }
    this.setData({
      checked: true,
      checkbox:e.detail.value,
      item: e.detail.value,
    })
  },
  zcontraAmt:function(){
    var that=this
    var packageAmt=(that.data.amount1 + that.data.amount2 + that.data.amount3 + that.data.amount4)
    if(packageAmt==''||packageAmt==null||that.data.add<=0){   
      packageAmt=that.data.packageAmt
    }
    that.setData({
      zcontraAmt:Number(packageAmt)+Number( that.data.Total_amount),
    })
  },
  clear:function(){
    this.setData({
      checkbox:'',
      checked:false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var that = this
    that.setData({
      active:options.active
    })
    util.request(api.jichupackage, null, "POST").then(function (res) {
      // console.log(res)
      that.setData({
        maskData: res.data
      })
    })
      //调用获取城市列表接口
      qqmapsdk.getCityList({
        success: function (res) { //成功后的回调
          // console.log(res.result)
          that.setData({
            citylist: res.result,
            province: res.result[0],
            cityname: res.result[1],
            county: res.result[2],
          });
        },
        fail: function (error) {
          console.error(error);
        },
        // complete: function(res) {
        //   console.log(res);
        // }
      });
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var date = util.formatTime6(new Date())
    var date1 = util.formatTime5(new Date())
    this.setData({
      date: date[0],
      date1: date1[0]
    })
    var _this=this
      //获取用户的登录信息
      var hasLogin=wx.getStorageSync('hasLogin')
      if (hasLogin) {
        let userInfo = wx.getStorageSync('userInfo');
        _this.setData({
          userInfo: userInfo,
          userLevelId:userInfo.userLevelId,
          username1:userInfo.username,
          hasLogin: true
        });
      }else{
        wx.navigateTo({
          url: '/pages/auth/login/login',
        })
      }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  zeng: function (e) {
    // console.log(e)
    let _optionList = this.data.zeng;
    var addd = e.currentTarget.dataset.add
    addd++;
    // console.log(addd)
    _optionList.push({
      id: addd
    })
    this.setData({
      zeng: _optionList,
      add: addd,
      zcontraAmt:''
    });
    // 选项大于15个后移除添加按钮
    if (_optionList.length >= 4) {
      this.setData({
        showAddBtn: 2,
      });
    } else if (_optionList.length < 5) {
      this.setData({
        showAddBtn: 1
      });
    }
  },
  delete: function (e) {
    // console.log(e)
    let _optionList = this.data.zeng;
    var delete1 = e.currentTarget.dataset.add
    delete1--;
    _optionList.splice(delete1, 1)
    this.setData({
      zeng: _optionList,
      add: delete1,
      zcontraAmt:''
    });
    // 选项大于15个后移除添加按钮
    if (_optionList.length >= 4) {
      this.setData({
        showAddBtn: 2
      });
    } else if (_optionList.length < 5) {
      this.setData({
        showAddBtn: 1
      });
    }
  },
  save: function (res) {
    // console.log('123')
    var that = this
    var data = that.data
    var region = data.region
    var address = region[0] + ',' + region[1] + ',' + region[2] + data.hoteladdress
    var packageAmt=(data.amount1 + data.amount2 + data.amount3 + data.amount4)
    console.log(packageAmt)
    if(packageAmt==''||packageAmt==null){
      packageAmt=that.data.packageAmt
    }
    var contraAmt1=Number(packageAmt)+Number( data.Total_amount);
    var periodvalue=that.data.periodvalue
    if(periodvalue==''){
      periodvalue=24
    }
    if(data.hotelzt==''){
      util.showErrorToast('酒店主题名称不能为空')
      return;
    }
    // else if(packageAmt==''){
    //   util.showErrorToast('请至少选择以一种套餐中的一个')
    //   return;
    // }
    else if(data.instAmt==''){
      util.showErrorToast('置换额度不能为空！')
      return;
    }
    else if(data.Amt_or_roomnum==''&&data.contraAmt==0){
      util.showErrorToast('空置房置换必须选一个！')
      return;
    }
    else if(data.zcontraAmt<0){
      util.showErrorToast('请计算总金额！')
      return;
    }
    else if(data.hotelArea==''){
      util.showErrorToast('请选择行政区域')
      return;
    }
    else if(data.hotelname==''){
      util.showErrorToast('酒店名称不能为空')
      return;
    }
    else if(data.hoteladdress==''){
      util.showErrorToast('请输入酒店完整地址')
      return;
    }
      else if(data.hotelprincipal==''){
      util.showErrorToast('请填写酒店负责人')
      return;
    } else if(data.ajxtrue == false){
      util.showErrorToast('酒店电话有误')
      return;
    }
    else if(data.yiprincipal==''){
      util.showErrorToast('请填写乙方负责人')
      return;
    }
    // else if(data.ajxtrue1 == false){
    //   util.showErrorToast('乙方负责人电话有误')
    //   return;
    // }else if(data.hotelroom==''){
    //   util.showErrorToast('请输入安装的房间号')
    //   return;
    // }
    var parameters = {
      hotelName: data.hotelname, //酒店名称
      hotelArea: data.hotelArea, //酒店行政区编号
      salesman: data.hehuoren, //合伙人账号
      salesmanPhone: data.hehuorenphone, //合伙人电话
      hotelSubject: data.hotelzt, //酒店主题名称
      hotelAddr: data.hoteladdress, //酒店完整地址
      packageAmt: packageAmt, //套餐总金额
      contraPeriod: 3, //合同期限
      instAmt: data.Total_amount, //置换额度
      period: periodvalue, //置换期数
      contraAmt: contraAmt1, //合同总金额=套餐总金额 + 置换总金额
      roomNum: data.Amt_or_roomnum, //折算后房间数量
      contraStartTime: data.date, //合同开始日期
      contraEndTime: data.date1, //合同结束日期
      hotelContactName: data.hotelprincipal, //酒店责任人
      hotelContactPhone: data.hotelphone, //酒店电话
      jcContactName: data.yiprincipal, //乙方责任人
      jcContactPhone: data.yiphone, //乙方电话
      roomlist: data.hotelroom, //安装的房间号
      // var jsonstr = JSON.stringify(jsonobj)
      packagelist: JSON.stringify([{
          id: data.packageID1,
          packagenum: data.unit1,
          remarks: data.remarks1
        },
        {
          id: data.packageID2,
          packagenum: data.unit2,
          remarks: data.remarks2
        },
        {
          id: data.packageID3,
          packagenum: data.unit3,
          remarks: data.remarks3
        },
        {
          id: data.packageID4,
          packagenum: data.unit4,
          remarks: data.remarks4
        },
      ]), //选择套餐列表
      addtion: data.additional, //附加条款
      replacementNotes: data.remarks5 //置换备注
    }
    // console.log(parameters)
    wx.request({
      url: api.hotelContract,
      data: parameters,
        method: 'POST',
        header: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
        success:function(res){
          console.log(res)
          if(res.data.code==0){
            wx.showModal({
              title: '提交成功',
              content: '您的合同已提交审核',
              success (res) {
                wx.switchTab({
                  url: '/pages/dts_index/index',
                })
              }   
            })
           
          }else if(res.data.errno==404){
            util.showErrorToast(res.data.errmsg)
          }else{
            util.showErrorToast('请求异常')
          }
        },
        fail:function(res){
          console.log(res)
        }
    })
  },
  Join_gold(e){
    this.setData({
      Join_gold: e.detail.value
    })
  },
  Join_ticket(e){
    this.setData({
      Join_ticket: e.detail.value
    })
  },
  partnerName(e){
    this.setData({
      partnerName: e.detail.value
    })
  },
  partner_additional(e){
    this.setData({
      partner_additional: e.detail.value
    })
  },
  partner_salesman(e){
    this.setData({
      partner_salesman: e.detail.value
    })
  },
  partner_save:function(res){
    var P_code=''
    var P_code1=this.data.P_code
    var P_code2=this.data.P_code1
    var P_code3=this.data.P_code2
    if(P_code3!=''){
      P_code=P_code3
      // return;
    }else if(P_code3==''&&P_code2!=''){
      P_code=P_code2
      // return;
    }else if(P_code3==''&&P_code2==''&&P_code1!=''){
      P_code=P_code1
    }else if(P_code3==''&&P_code2==''&&P_code1==''){
      util.showErrorToast('请选择加盟地区')
      return;
    }
    console.log(P_code)
    var partnerArea=this.data.partnerArea1 + this.data.partnerArea2 + this.data.partnerArea3
    console.log(partnerArea)
    if(this.data.Join_gold==""){
      util.showErrorToast('加盟金不能为空')
      return
    }else if(this.data.Join_ticket==''){
        util.showErrorToast('置换酒店券必填')
        return;
      }
      else if(this.data.partnerName==''){
        util.showErrorToast('合伙人名')
        return;
      }  else if(this.data.partnerLevel==''){
        util.showErrorToast('合伙人等级')
        return;
      } 
    util.request1(api.partnerContract,{
      contractAmount:this.data.Join_gold,//加盟金
      couponAmount:this.data.Join_ticket,//置换酒店券
      partnerName:this.data.partnerName,//合伙人名
      partnerArea:partnerArea,//合伙人区域
      areaNumber:P_code,//区域行政区编号
      partnerLevel:this.data.partnerLevel,//合伙人等级
      addtion:this.data.partner_additional,//合伙人附加条款
      share01:10,
      share02:5,
      salesman:'',

    }).then(function (res) {
      console.log(res)
      if(res.code==0){
        wx.showModal({
          title: '提交成功',
          content: '您的合同已提交审核',
          success (res) {
            wx.switchTab({
              url: '/pages/dts_index/index',
            })
          }   
        })
      }else if(res.errno==404){
        util.showErrorToast(res.data.errmsg)
      }else{
        util.showErrorToast('请求异常')
      }
    })
  },
  //供应商合同
  supplierRepr(e){
    this.setData({
      supplierRepr:e.detail.value
    })
  },
  supplierCompany(e){
    this.setData({
      supplierCompany:e.detail.value
    })
  },
  supplierAddress(e){
    this.setData({
      supplierAddress:e.detail.value
    })
  },
  supplierPhone(e){
    this.setData({
      supplierPhone:e.detail.value
    })
  },
  servicePromotion(e){
    this.setData({
      servicePromotion:e.detail.value
    })
  },
  partnerSplit(e){
    this.setData({
      partnerSplit:e.detail.value
    })
  },
  supplier_addtion(e){
    this.setData({
      supplier_addtion:e.detail.value
    })
  },
  supplier_save:function(){
    var data=this.data
    var supplierAddress=data.supplier[0] + ',' + data.supplier[1] + ',' + data.supplier[2] + data.supplierAddress
    if(this.data.supplierRepr==""){
      util.showErrorToast('请填写供应商名')
      return
    }else if(this.data.supplierCompany==''){
        util.showErrorToast('请填写公司名')
        return;
      }
      else if(data.supplier==''){
        util.showErrorToast('请选择地址')
        return;
      }
      else if(data.supplierAddress==''){
        util.showErrorToast('请填写详细地址')
        return;
      }
      else if(data.servicePromotion==''){
        util.showErrorToast('请填写推广服务费')
        return;
      }else if(data.servicePromotion==''){
        util.showErrorToast('请填写合伙人分成')
        return;
      } 
    util.request1(api.supplierContract,{
      supplierRepr:data.supplierRepr,//供应商名
      supplierCompany:data.supplierCompany,//供应商公司名
      supplierAddress:supplierAddress,//供应商地址
      supplierPhone:data.supplierPhone,//供应商电话
      addtion:data.supplier_addtion,//附加条款
      servicePromotion:data.servicePromotion,//推广服务费
      partnerSplit:data.partnerSplit,//合伙人分成
    }).then(function (res) {
      console.log(res)
      if(res.code==0){
        wx.showModal({
          title: '提交成功',
          content: '您的合同已提交审核',
          success (res) {
            wx.switchTab({
              url: '/pages/dts_index/index',
            })
          }   
        })
      }else if(res.errno==404){
        util.showErrorToast(res.data.errmsg)
      }else{
        util.showErrorToast('请求异常')
      }
    })
  }
})