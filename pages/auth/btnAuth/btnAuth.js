const util = require('../../../utils/util.js');
const api = require('../../../config/api.js');
const WXBizDataCrypt = require('../../../utils/Node/WXBizDataCrypt');
var util1 = require('../../../utils/back.js'); //引用外部的js文件
//获取应用实例
const app = getApp()
Page({
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    navUrl: '',
    code: '',
    cxhotel:''
  },

  onLoad: function(options) {
    let that = this;
    console.log(options)
    that.setData({
      cxhotel:options.cxhotel
    })
    wx.getSetting({
      
      success(res) {
        console.log(res)
        if (!res.authSetting['scope.userInfo']) {
          wx.authorize({
            scope: 'scope.userInfo',
            success (res) {
              console.log(res)
              // 用户已经同意小程序使用录音功能，后续调用 wx.startRecord 接口不会弹窗询问
              wx.getUserInfo()
            }
          })
        }
       
    
      }
    })
   
   

    if (wx.getStorageSync("navUrl")) {
      that.setData({
        navUrl: wx.getStorageSync("navUrl")
      })
    } else {
    
      // that.setData({
      //   // navUrl: '/pages/index/index'
      //   navUrl: '/pages/topic/topic'
      // })
    }

    wx.login({
      success: function(res) {
        console.log(res)
        if (res.code) {
          that.setData({
            code: res.code
          })
          
        }
       
      }
    });

  },
  getPhoneNumber (e) {
    console.log(e.detail.errMsg)
    console.log(e.detail.iv)
    console.log(e.detail.encryptedData)
    var WXBizDataCrypt = require('../../../utils/Node/WXBizDataCrypt')
    var appId = 'wxc1a0db73b3b3fc89'
    var sessionKey = e.detail.errMsg
    var encryptedData = e.detail.encryptedData
    var iv = e.detail.iv
    // var pc = new WXBizDataCrypt(appId, sessionKey)
    // var data = pc.decryptData(encryptedData , iv)
    wx.request({ 
      url: app.globalData.url83+'/api/user/test',
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
     data:{
         code:this.data.code,
         encryptedData:encryptedData,
        // checkInTime:_this.data.check_in_time,
        iv:iv
     },
     success:function(res){
       console.log(res)
     },
     fail:function(res){
      console.log(res)
     }
    })
  },
  bindGetUserInfo: function(e) {
    let that = this;
    //登录远程服务器
    util1.showLoading('加载中...');
    if (that.data.code) {
      util.request( app.globalData.url80 +"api/auth/login_by_weixin", {
        code: that.data.code,
        userInfo: e.detail,
        username:wx.getStorageSync('username1')
        // userId:
      }, 'POST', 'application/json').then(res => {
        console.log(res)
        util1.hideLoading()
        if (res.errno === 0) {
          //存储用户信息
          wx.setStorageSync('userInfo', res.data.userInfo);
          wx.setStorageSync('userLevelId', res.data.userLevelId);
          wx.setStorageSync('token', res.data.token);
          wx.setStorageSync('token', res.data.token);
          wx.setStorageSync('userId', res.data.userId);
          wx.setStorageSync('username', res.data.username);
          wx.setStorageSync('nickName', res.data.userInfo.nickName);
          if(that.data.cxhotel=='cxhotel'){
            wx.navigateTo({
              url: '/pages/hotellist/hotellist',
            })
          }else{
            util1.onBackTap() //调用
          }
        } else {
          // util.showErrorToast(res.errmsg)
          wx.showModal({
            title: '提示',
            content: res.errmsg,
            showCancel: false
          });
        }
      });
    }
  },
  onReady: function() {
    // 页面渲染完成
  },
  onShow: function() {
    // 页面显示
  },
  onHide: function() {
    // 页面隐藏
  },
  onUnload: function() {
    // 页面关闭
  }
})