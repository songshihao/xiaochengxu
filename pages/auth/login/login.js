var api = require('../../../config/api.js');
var util = require('../../../utils/util.js');
var user = require('../../../utils/user.js');

var app = getApp();
Page({
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    navUrl: '',
    code: '',
    cxhotel:'',
    linghongbao:''
  },

  onLoad: function(options) {
    let that = this;
    console.log(options)
    that.setData({
      cxhotel:options.cxhotel,
      linghongbao:options.linghongbao
    })
    wx.removeStorageSync('userInfo');
    wx.removeStorageSync('token');
    wx.removeStorageSync('token');
    wx.removeStorageSync('userLevelId');
    wx.removeStorageSync('userId');
    wx.removeStorageSync('username');
    wx.removeStorageSync('nickName');
    // wx.removeStorageSync('password');
  },
  onReady: function() {

  },
  onShow: function() {
    // 页面显示
  },
  onHide: function() {
    // 页面隐藏
  },
  onUnload: function() {
    // 页面关闭
  },
  wxLogin: function(e) {
    util.jc_LoadShow('正在登录')
    
    if (e.detail.userInfo == undefined) {
      app.globalData.hasLogin = false;
      util.showErrorToast('微信登录失败');
      return;
    }
    user.checkLogin().catch(() => {
      user.loginByWeixin(e.detail.userInfo).then(res => {
        util.jc_LoadHide()
        app.globalData.hasLogin = true;
        wx.setStorage({
          data: true,
          key: 'hasLogin',
        })
        wx.removeStorageSync('password');
        if(this.data.cxhotel=='cxhotel'){
          wx.navigateTo({
            url: '/pages/hotellist/hotellist',
          })
        }else if(this.data.linghongbao=='1'){
          wx.navigateTo({
            url: '/pages/ucenter/receivehb/receivehb',
          })
        }else{
          wx.navigateBack({
            delta: 1
          })
        }
      }).catch((err) => {
        console.log(err)
        app.globalData.hasLogin = false;
        util.showErrorToast('微信登录失败');
      });
    });
  },
  accountLogin: function() {
    wx.navigateTo({
      url: "/pages/auth/accountLogin/accountLogin"
    });
  }
})