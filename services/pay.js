/**
 * 支付相关服务
 */

const util = require('../utils/util.js');
const api = require('../config/api.js');

/**
 * 判断用户是否登录
 */
function payOrder(orderId,payPass) {
  return new Promise(function (resolve, reject) {
    util.request(api.PayPrepayId, {
      orderId: orderId,
      payPass: payPass
    }).then((res) => {
      console.log(res)
      if (res.errno === 0) {
        const payParam = res.data;
         wx.redirectTo({
            url: '/pages/payResult/payResult?status=1&orderId=' + orderId
          });
      //   wx.requestPayment({
      //     'timeStamp': payParam.timeStamp,
      //     'nonceStr': payParam.nonceStr,
      //     'package': payParam.package,
      //     'signType': payParam.signType,
      //     'paySign': payParam.paySign,
      //     'success': function (res) {
      //       resolve(res);
      //     },
      //     'fail': function (res) {
      //       reject(res);
      //     },
      //     'complete': function (res) {
      //       reject(res);
      //     }
      //   });
      } else {
        reject(res);
      }
    });
  });
}


module.exports = {
  payOrder,
};











