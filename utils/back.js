// onBackTap: function(e) {
//   // wx.redirectTo({
//   //   url: '../photo',
//   // })

//   var pages = getCurrentPages();                       //获取当前页面
//   var prePage = pages[pages.length - 2];               //获取上一页面
//   prePage.setData({
//     'search.page': 1                                   //给上一页面的变量赋值
//   })
//   wx.navigateBack({                                    //返回上一页面
//     delta: 1,
//   })
// },
 
//封装的函数
function onBackTap() {
  //   url: '../photo',
  // })

  var pages = getCurrentPages();                       //获取当前页面
  var prePage = pages[pages.length - 2];               //获取上一页面
  // prePage.setData({
  //   'search.page': 1                                   //给上一页面的变量赋值
  // })
  wx.navigateBack({                                    //返回上一页面
    delta: 1,
  })
  prePage.onLoad(prePage.options)
  }
  function showLoading(message) {
    if (wx.showLoading) {
      // 基础库 1.1.0 微信6.5.6版本开始支持，低版本需做兼容处理
      wx.showLoading({
        title: message,
        mask: true
      });
    } else {
      // 低版本采用Toast兼容处理并将时间设为20秒以免自动消失
      wx.showToast({
        title: message,
        icon: 'loading',
        mask: true,
        duration: 20000
      });
    }
  }
   
  function hideLoading() {
    if (wx.hideLoading) {
      // 基础库 1.1.0 微信6.5.6版本开始支持，低版本需做兼容处理
      wx.hideLoading();
    } else {
      wx.hideToast();
    }
  }


  
  //转化成小程序模板语言 这一步非常重要 不然无法正确调用
    module.exports = {
      onBackTap: onBackTap,
      showLoading:showLoading,
      hideLoading:hideLoading
    }