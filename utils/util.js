var api = require('../config/api.js');
var app = getApp();

function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}
function formatTime1(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  if(month<10){
      month = '0'+ month;  //补齐
  }
  var day = date.getDate()
  // var day = date.getDate()+1
  if(day<10){
    day = '0'+ day;  //补齐
  }
 var  multiIndex=[year,month,day]
  wx.setStorage({
    data: multiIndex,
    key: 'multiIndex',
  })
  // var hour = date.getHours()
  // var minute = date.getMinutes()
  // var second = date.getSeconds()
  return [year +'-'+ month +'-'+ day].map(formatNumber)
}
function formatTime3(date) {
  var d=date
  d.setDate(d.getDate()+1);
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  if(month<10){
    month = '0'+ month;  //补齐
  }
  var day = date.getDate()
  if(day<10){
    day = '0'+ day;  //补齐
  }
  var  multiIndex1=[year,month,day]
  wx.setStorage({
      data: multiIndex1,
      key: 'multiIndex1',
    })
  return [ year +'-'+ month +'-'+ day].map(formatNumber)
}
function formatTime5(date) {
  var year = date.getFullYear()+3
  var month = date.getMonth() + 1
  if(month<10){
    month = '0'+ month;  //补齐
  }
  var day = date.getDate()-1
  if(day<10){
    day = '0'+ day;  //补齐
  }
  var  multiIndex1=[year,month,day]
  wx.setStorage({
      data: multiIndex1,
      key: 'multiIndex1',
    })
  // var hour = date.getHours()
  // var minute = date.getMinutes()
  // var second = date.getSeconds()
  return [year +'-'+ month +'-' + day].map(formatNumber)
}
function formatTime7(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  if(month<10){
    month = '0'+ month;  //补齐
  }
  var day = date.getDate()+1
  if(day<10){
    day = '0'+ day;  //补齐
  }
  var  multiIndex1=[year,month,day]
  wx.setStorage({
      data: multiIndex1,
      key: 'multiIndex1',
    })
  // var hour = date.getHours()
  // var minute = date.getMinutes()
  // var second = date.getSeconds()

  return [year +'-'+ month +'-' + day].map(formatNumber)
}
function formatTime6(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  if(month<10){
    month = '0'+ month;  //补齐
  }
  var day = date.getDate()
  if(day<10){
    day = '0'+ day;  //补齐
  }
  var  multiIndex1=[year,month,day]
  wx.setStorage({
      data: multiIndex1,
      key: 'multiIndex1',
    })
  // var hour = date.getHours()
  // var minute = date.getMinutes()
  // var second = date.getSeconds()

  return [year +'-'+ month +'-' + day].map(formatNumber)
}
function formatTime2(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  // if(month<10){
  //   month = '0'+ month;  //补齐
  // }
  var day = date.getDate()

  // var hour = date.getHours()
  // var minute = date.getMinutes()
  // var second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('-') 
}
function formatTime4(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  // var hour = date.getHours()
  // var minute = date.getMinutes()
  // var second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('-') 
}
function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

/**
 * 封封微信的的request
 */
function request(url, data = {}, method = "GET") {
  return new Promise(function(resolve, reject) {
    wx.request({
      url: url,
      data: data,
      method: method,
      header: {
        'Content-Type': 'application/json',
        'X-Dts-Token': wx.getStorageSync('token')
      },
      success: function(res) {
        if (res.statusCode == 200) {
          if (res.data.errno == 501) {
            // 清除登录相关内容
            try {
              wx.removeStorageSync('userInfo');
              wx.removeStorageSync('token');
            } catch (e) {              // Do something when catch error
            }
            // 切换到登录页面
            wx.showModal({
              title: '提示',
              content: '您需要先登录',
              success (res) {
              if (res.confirm) {
                wx.navigateTo({
                  url: '/pages/auth/login/login'
                });
              } else if (res.cancel) {
                wx.switchTab({
                  url: '/pages/topic/topic',
                })
              }
              }
            })
            // setTimeout(function(){
            //   wx.navigateTo({
            //     url: '/pages/auth/login/login'
            //   });
            // },500)
          }else if(res.errno==502){
            wx.showToast({
              title: res.message, 
              icon: 'none', 
              duration: 20000
            });
          }else {
            resolve(res.data);
          }
        } else {
          reject(res.errMsg);
        }
      },
      fail: function(err) {
        reject(err)
        wx.showToast({
          title: err, 
          icon: 'none', 
          duration: 20000
        });
      }
    })
  });
}
function request1(url, data = {}, method = "POST") {
  wx.showLoading({
      title: '加载中...',
  });
  return new Promise(function (resolve, reject) {
      wx.request({
          url: url,
          data: data,
          method: method,
          header: {
              'Content-type': "application/x-www-form-urlencoded",
              'X-Nideshop-Token': wx.getStorageSync('token')
          },
          success: function (res) {
              wx.hideLoading();
              if (res.statusCode == 200) {
                  if (res.data.errno == 401) {
                      wx.navigateTo({
                          url: '/pages/auth/login/login',
                      })
                  }else if(res.errno==502){
                    wx.showToast({
                      title: res.message, 
                      icon: 'none', 
                      duration: 20000
                    });
                  } else {
                      resolve(res.data);
                  }
              } else {
                  reject(res.errMsg);
              }

          },
          fail: function (err) {
              reject(err)
              wx.showToast({
                title: err, 
                icon: 'none', 
                duration: 20000
              });
          }
      })
  });
}

function redirect(url) {

  //判断页面是否需要登录
  if (false) {
    wx.redirectTo({
      url: '/pages/auth/login/login'
    });
    return false;
  } else {
    wx.redirectTo({
      url: url
    });
  }
}

function showErrorToast(msg) {
  wx.showToast({
    title: msg,
    image: '/static/images/icon_error.png'
  })
}

function jc_LoadShow(message) {
  if (wx.showLoading) {  // 基础库 1.1.0 微信6.5.6版本开始支持，低版本需做兼容处理
    wx.showLoading({
      title: message, 
      mask: true
    });
  } else {    // 低版本采用Toast兼容处理并将时间设为20秒以免自动消失
    wx.showToast({
      title: message, 
      icon: 'loading', 
      mask: true, 
      duration: 20000
    });
  }
}

function jc_LoadHide() {
  if (wx.hideLoading) {    // 基础库 1.1.0 微信6.5.6版本开始支持，低版本需做兼容处理
    wx.hideLoading();
  } else {
    wx.hideToast();
  }
}

function copy(i) {
  wx.setClipboardData({
    data: i,
    success: function (res) {
      if (res.errMsg =='setClipboardData:ok'){
        wx.showToast({
          title: '复制成功',
        })
      }else{
        wx.showToast({
          title:res.errMsg,
          icon:'none'
        })
      }
    },
    fail:function(res){
      wx.showToast({
        title: '复制失败~',
        icon: 'none'
      })
    }
  })
}
module.exports = {
  formatTime,
  formatTime1,
  formatTime2,
  formatTime3,
  formatTime4,
  formatTime5,
  formatTime6,
  formatTime7,
  request,
  request1,
  redirect,
  showErrorToast,
  jc_LoadShow,
  jc_LoadHide,
  copy
}