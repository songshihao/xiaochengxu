/**
 * 用户相关服务
 */
const util = require('../utils/util.js');
const api = require('../config/api.js');
/**
 * Promise封装wx.checkSession
 */
function checkSession() {
  return new Promise(function(resolve, reject) {
    wx.checkSession({
      success: function() {
        resolve(true);
      },
      fail: function() {
        reject(false);
      }
    })
  });
}

/**
 * Promise封装wx.login
 */
function login() {
  return new Promise(function(resolve, reject) {
    wx.login({
      success: function(res) {
        // wx.getUserInfo({
        // success(res){
        //   console.log(res)
        //   wx.setStorageSync('unionid', res)
        // }
        // })
        if (res.code) {
          resolve(res);
        } else {
          reject(res);
        }
      },
      fail: function(err) {
        reject(err);
      }
    });
  });
}

/**
 * 调用微信登录
 */
function loginByWeixin(userInfo) {
  console.log(userInfo)
  let shareUserId = wx.getStorageSync('shareUserId');
  let username1 = wx.getStorageSync('username1');
  if (!shareUserId || shareUserId =='undefined'){
    shareUserId = 1;
  }
  return new Promise(function(resolve, reject) {
    return login().then((res) => {
      //登录远程服务器
      wx.showLoading();
      util.request(api.AuthLoginByWeixin, {
        code: res.code,
        userInfo: userInfo,
        shareUserId: shareUserId,
        username:username1
      }, 'POST').then(res => {
        wx.hideLoading();
        if (res.errno === 0) {
          //存储用户信息
          wx.setStorageSync('userInfo', res.data.userInfo);
          wx.setStorageSync('token', res.data.token);
          wx.setStorageSync('token', res.data.token);
          wx.setStorageSync('userLevelId', res.data.userInfo.userLevelId);
          wx.setStorageSync('userId', res.data.userInfo.userId);
          wx.setStorageSync('username', res.data.userInfo.username);
          wx.setStorageSync('nickName', res.data.userInfo.nickName);

          resolve(res);
        } else {
          reject(res);
        }
      }).catch((err) => {
        reject(err);
      });
    }).catch((err) => {
      reject(err);
    })
  });
}

/**
 * 判断用户是否登录
 */
function checkLogin() {
  return new Promise(function(resolve, reject) {
    if (wx.getStorageSync('userInfo') && wx.getStorageSync('token')) {
      checkSession().then(() => {
        resolve(true);
      }).catch(() => {
        reject(false);
      });
    } else {
      reject(false);
    }
  });
}

module.exports = {
  loginByWeixin,
  checkLogin,
};