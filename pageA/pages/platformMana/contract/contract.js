// pages/platformMana/hotelManalist/hotelManalist.js
var util = require("../../../../utils/util")
var api = require("../../../../config/api")
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tab: [{
        name: "酒店合同"
      },
      {
        name: "合伙人合同"
      },
      {
        name: "供应商合同"
      },
    ],
    active: '酒店合同',
    contractlist: [

    ],
    partner:[],
    supplier:[],
    pageSize: '',
    status: '',
    list: '',
    page: 1,
    userLevelId:''
  },
  current: function (x) {
    // console.log(x)
    var that = this
   
    if(x.currentTarget.dataset.name=='酒店合同'){
      that.setData({
        active: x.currentTarget.dataset.name
      })
      that.list();
    }
    if(x.currentTarget.dataset.name=='合伙人合同'){
      that.setData({
        active: x.currentTarget.dataset.name
      })
      that.partner();
    }
    if(x.currentTarget.dataset.name=='供应商合同'){
      that.setData({
        active: x.currentTarget.dataset.name
      })
      that.supplier();
    }
  },
  copy: function(e) {
    console.log(e)
    util.copy(e.target.dataset.copy)
  },
  check: function (res) {
    // console.log(res)
    var status = res.currentTarget.dataset.status
    var id = res.currentTarget.dataset.id
    var contractlist = this.data.contractlist
    var that = this
    if (status != 3) {
      wx.navigateTo({
        url: '/pageA/pages/platformMana/check/check?id=' + id + '&contract_type='+ '酒店合同',
      })
    } else {
      util.showErrorToast('合同已驳回')
    }
  },
  chakan:function (res) {
    // console.log(res)
    var that = this
    var status = res.currentTarget.dataset.status
    var id = res.currentTarget.dataset.id
    var active= that.data.active
    // console.log('123')
    // console.log(active)
      wx.navigateTo({
        url: '/pageA/pages/platformMana/check/check?id=' + id + '&contract_type='+ active+ '&type='+ 1,
      })
  },
  chakan1:function(res){
    var that = this
    var status = res.currentTarget.dataset.status
    var id = res.currentTarget.dataset.id
    var active =  res.currentTarget.dataset.active
  
      wx.navigateTo({
        url: '/pageA/pages/platformMana/check/check?id=' + id + '&contract_type='+ active+ '&type='+ 1+ '&chakan='+ 1,
      })
  },
  check_partner: function (res) {
    // console.log(res)
    var status = res.currentTarget.dataset.status
    var id = res.currentTarget.dataset.id
    var that = this
    if (status != 3) {
      wx.navigateTo({
        url: '/pageA/pages/platformMana/check/check?id=' + id+ '&contract_type='+ that.data.active,
      })
    } else {
      util.showErrorToast('合同已驳回')
    }
  },
  check_supplier: function (res) {
    // console.log(res)
    var status = res.currentTarget.dataset.status
    var id = res.currentTarget.dataset.id
    var that = this
    if (status != 3) {
      wx.navigateTo({
        url: '/pageA/pages/platformMana/check/check?id=' + id+ '&contract_type='+ that.data.active,
      })
    } else {
      util.showErrorToast('合同已驳回')
    }
  },
  delete: function (e) {
    console.log(e)
    var that=this
    var active=that.data.active
    var that=this
    var url=''
    var id=e.currentTarget.dataset.id
    if(that.data.active=='酒店合同'){
      url=api.c_delete
    }else if(that.data.active=='合伙人合同'){
      url=api.partner_delete
    }else if(that.data.active=='供应商合同'){
      url=api.supplier_delete
    }
    util.request1(url, {
      id:id,
    },
       "POST").then(function (res) {
       if(res.code==0){
            wx.showModal({
              title: '删除成功',
              content: '此合同已经被删除',
              success (res) {
                if(active=='酒店合同'){
                  that.list();
                }else if(active=='合伙人合同'){
                  that.partner();
                }else if(active=='供应商合同'){
                  that.supplier();
                }
              }
            })
        }else{
          util.showErrorToast('请求异常')
        }
      })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    let that=this
    that.setData({
      active:options.contract_type
    })
    if(options.contract_type=='酒店合同'){
      that.list();
    }else if(options.contract_type=='合伙人合同'){
      that.partner();
    }else if(options.contract_type=='供应商合同'){
      that.supplier();
    }
  },
  switch1Change: function (e) {

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    //获取用户的登录信息
    var hasLogin=wx.getStorageSync('hasLogin')
    if (hasLogin) {
      let userInfo = wx.getStorageSync('userInfo');
      // let username1=wx.getStorageSync('username');
      // let userLevelId=wx.getStorageSync('userLevelId');
      that.setData({
        userInfo: userInfo,
        userLevelId:userInfo.userLevelId,
        username1:userInfo.username,
        hasLogin: true
      });
    }else{
      wx.navigateTo({
        url: '/pages/auth/login/login',
      })
    }
   that.list();
  },
  list:function(){
    var that = this
    wx.request({
      url: api.c_hotelContract,
      data: {
        page: 1,
        pageSize: 10
      },
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        // console.log(res)
        if (res.data.code == 0) {
          that.setData({
            contractlist: res.data.data
          })
        } else {
          util.showErrorToast('请求异常')
        }

      },
      fail: function (res) {
        console.log(res)
      }
    })
  },
  partner:function(){
    var that = this
    wx.request({
      url: api.partner_selectByIdAndAll,
      data: {
        page: 1,
        pageSize: 10
      },
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        // console.log(res)
        if (res.data.code == 0) {
          that.setData({
            partner: res.data.data
          })
        } else {
          util.showErrorToast('请求异常')
        }

      },
      fail: function (res) {
        console.log(res)
      }
    })
  },
  supplier:function(){
    var that = this
    wx.request({
      url: api.supplier_selectByIdAndAll,
      data: {
        page: 1,
        pageSize: 10
      },
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        // console.log(res)
        if (res.data.code == 0) {
          that.setData({
            supplier: res.data.data
          })
        } else {
          util.showErrorToast('请求异常')
        }

      },
      fail: function (res) {
        console.log(res)
      }
    })
  },
  newaccount:function(e){
    console.log(e)
    var that=this
    var active=e.currentTarget.dataset.active
    var ContraNo=e.currentTarget.dataset.status
    var suppliercompany=e.currentTarget.dataset.suppliercompany
    var supplierPhone=e.currentTarget.dataset.supplierphone
    var hotelname=e.currentTarget.dataset.hotelname
    var hotelcontactphone=e.currentTarget.dataset.hotelcontactphone
    if(active=='酒店合同'){
        wx.navigateTo({
          url: '/pageA/pages/platformMana/hotelMana/hotelMana?type=' + '0'+'&ContraNo='+ContraNo+'&hotelname='+hotelname+'&hotelcontactphone='+hotelcontactphone,//酒店
        })
    }else if(active=='供应商合同'){
      wx.navigateTo({
        url: '/pageA/pages/platformMana/hotelMana/hotelMana?type=' + '2'+'&ContraNo='+ContraNo + '&suppliercompany=' + suppliercompany+ '&supplierPhone=' + supplierPhone,//供应商
      })
    }
  
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

/**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onShow();
    setTimeout(function(){
      wx.stopPullDownRefresh({
        complete: (res) => {
          wx.showToast({
            title: '刷新成功',
            icon:'none',
          })
        },
      })
    },200)
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    console.log(that.data.contractlist)
    var url=''
    if(that.data.active=='酒店合同'){
      url=api.c_hotelContract
      var listall1=that.data.contractlist
    }else if(that.data.active=='合伙人合同'){
      url=api.partner_selectByIdAndAll
      var listall2=that.data.partner
    }else if(that.data.active=='供应商合同'){
      url=api.supplier_selectByIdAndAll
      var listall3=that.data.supplier
    }
    var page = that.data.page

    page++;
    that.setData({
      page: page
    })
    // 显示加载图标
    wx.showLoading({
      title: '玩命加载中',
    })
    wx.request({
      url: url,
      data: {
        page: page,
        pageSize: 10
      },
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        wx.hideLoading();
        if (res.data.code == 0) {
         var list=res.data.data
         
         if (list == '') {
           wx.showToast({
             title: '暂无更多',
             icon: 'none',
           })
           return
         } else {
           if(that.data.active=='酒店合同'){
            var goods = list.concat(listall1) // 为一进入页面请求完数据定义的集合
            that.setData({
              contractlist: goods,
            });
           }else if(that.data.active=='合伙人合同'){
            var goods = list.concat(listall2) // 为一进入页面请求完数据定义的集合
            that.setData({
              partner: goods,
            });
           }else if(that.data.active=='供应商合同'){
            var goods = list.concat(listall3) // 为一进入页面请求完数据定义的集合
            that.setData({
              supplier: goods,
            });
           }
        
        
         }
        } else {
          util.showErrorToast('请求异常')
        }
     
  
      },
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  addhotel: function () {
    wx.navigateTo({
      url: '/pageA/pages/platformMana/hotelMana/hotelMana?type=' + '0',
    })
  }
})