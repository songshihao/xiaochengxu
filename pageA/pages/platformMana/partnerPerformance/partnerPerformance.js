// pages/platformMana/hotelManalist/hotelManalist.js
var api = require('../../../../config/api.js');
var util=require('../../../../utils/util')
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hotelManalist:[],
    pageSize:'',
    status:'',
    id:'',
    zhuntai:'',
    page:1,
    userLevelId:2,
    objectArray: [
      {
        id: 2,
        name: '普通合伙人'
      },
      {
        id: 3,
        name: '区县合伙人'
      },
      {
        id: 5,
        name: '城市合伙人'
      },
    ],
    index: 0,
    startdate: '',
    enddate: '',
    hotelCount:'',
    hotelSum: '',
    partnerCount: '',
    partnerSum: '',
    supplierCount: '',
  },
  qingqiu:function(){
    var that=this
      wx.request({
        url: api.statistics,
        data:{
          userId:'',
          startTime:'',
          endTime:''
        },
        method:'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded',
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
        success: function(res) {
          console.log(res)
          if(res.data.code==0){
            that.setData({
              hotelCount:res.data.hotelCount,
              hotelSum: res.data.hotelSum,
              partnerCount: res.data.partnerCount,
              partnerSum: res.data.partnerSum,
              supplierCount: res.data.supplierCount
            })
          }
        },
        fail:function(res){
          console.log(res)
        }
      })
 
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.qingqiu();
    var date = util.formatTime6(new Date())
    var date1 = util.formatTime7(new Date())
    this.setData({
      startdate: date[0],
      enddate: date1[0]
    })
    
  }, 
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that=this
    wx.request({
     url: app.globalData.url80 +"user/checkAccount",
     data: {
       userLevelId: that.data.userLevelId,
       page: 1,
       pageSize:20
     },
     method: 'POST',
     header: {
       'content-type': 'application/x-www-form-urlencoded',
       'X-Dts-Token': wx.getStorageSync('token')
     },
     success: function (res) {
      //  console.log(res)
       that.setData({
         hotelManalist:res.data.data,
       })
       wx.stopPullDownRefresh();
     },
     fail:function(res){
       console.log(res)
     }
    })
  },

/**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    // console.log(that.data.hotelManalist)
    var page = that.data.page
    var hotelManalist = that.data.hotelManalist
    page++;
    that.setData({
    page: page
    })
    // 显示加载图标
    wx.showLoading({
    title: '玩命加载中',
    })
    wx.request({
      url: app.globalData.url80 +"user/checkAccount",
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Dts-Token': wx.getStorageSync('token')
      },
     data:{
      userLevelId: that.data.userLevelId,
      page: page,
      pageSize:20
     },
    success: function (res) {
      console.log(res)
      wx.hideLoading();
      var list =res.data.data
    if (list == '') {
    wx.showToast({
    title: '暂无更多',
    icon: 'none',
    })
    } else {
    var lists = list.concat(hotelManalist)     //grade  为一进入页面请求完数据定义的集合
    that.setData({
      hotelManalist : lists
    });
    }
    },
  })
 },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  addhotel:function(){
    wx.navigateTo({
      url: '/pageA/pages/platformMana/hotelMana/hotelMana?type='+'4',
    })
  }
})