// pages/myContract/myContract.js
var util = require('../../../../utils/util.js');
var api = require('../../../../config/api.js');
var user = require('../../../../utils/user.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    taocan:[],
    userLevelId:'',
    id:'',
    status:'',
    contract_type:'',
    type:'',
    show:0,
    reject_reason:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var id=options.id
    var contract_type=options.contract_type
    var type=options.type
    var that = this
    that.setData({
      id:id,
      contract_type:contract_type,
      type:type
    })
    if(contract_type=='酒店合同'){
    util.request1(api.c_hotelContract, {
      id:id,
      page:1,
      pageSize:1
    },
       "POST").then(function (res) {
      console.log(res)
      if(res.code==0){
        // packagelist
        var str=res.data[0].packagelist
        var packagelist = JSON.parse(str); 
        console.log(packagelist)

        that.setData({
          list:res.data,
          packagelist:packagelist  
        })
      }
    })   
  }else if(contract_type=='合伙人合同'){
    util.request1(api.partner_selectByIdAndAll, {
      id:id,
      page:1,
      pageSize:1
    },
       "POST").then(function (res) {
      console.log(res)
      if(res.code==0){
        // packagelist
        // var str=res.data[0].packagelist
        // var packagelist = JSON.parse(str); 
        // console.log(packagelist)
        that.setData({
          list:res.data        
        })
      }
    }) 
  }else if(contract_type=='供应商合同'){
    util.request1(api.supplier_selectByIdAndAll, {
      id:id,
      page:1,
      pageSize:1
    },
       "POST").then(function (res) {
      console.log(res)
      if(res.code==0){
        // packagelist
        // var str=res.data[0].packagelist
        // var packagelist = JSON.parse(str); 
        // console.log(packagelist)
        that.setData({
          list:res.data        
        })
      }
    }) 
  }
  },
  audit:function(e){
    var that=this
    if(that.data.userLevelId!=8){
      that.setData({
        status:1
      })
    }else{
      that.setData({
        status:2
      })
    }
    that.jiekou();
  },
  reject:function(e){
    var that=this
      that.setData({
        status:3,
        show:1
      })
  },
  submit:function(){
    this.jiekou();
  },
  hide:function(){
    this.setData({
      show:0
    })
  },
  jiekou:function(){
    var that=this
    var url=''
    if(that.data.contract_type=='酒店合同'){
      url=api.audit
    }else if(that.data.contract_type=='供应商合同'){
      url=api.supplier_audit
    }

      util.request1(url, {
        id:that.data.id,
        status:that.data.status,
        approvalComments:that.data.reject_reason
      },
         "POST").then(function (res) {
          console.log(res)
          if(res.code==0){
            if(that.data.status==1){
              wx.showModal({
                title: '成功',
                content: '已通过初审',
                success (res) {
                    wx.navigateTo({
                      url: '/pageA/pages/platformMana/contract/contract?contract_type='+that.data.contract_type,
                    })
                }
              })
            }else if(that.data.status==2){
              wx.showModal({
                title: '成功',
                content: '已通过终审',
                success (res) {
                    wx.navigateTo({
                      url: '/pageA/pages/platformMana/contract/contract?contract_type='+that.data.contract_type,
                    })
                }
              })
            }else if(that.data.status==3){
              wx.showModal({
                title: '驳回成功',
                content: '合同已经被驳回',
                success (res) {
                    wx.navigateTo({
                      url: '/pageA/pages/platformMana/contract/contract?contract_type='+that.data.contract_type,
                    })
                }
              })
            }
      
          }else{
            util.showErrorToast('请求异常')
          }
        })
     if(that.data.contract_type=='合伙人合同'){
    util.request1(api.partner_audit, {
      id:that.data.id,
      status:that.data.status,
      approvalComments:that.data.reject_reason
    },
       "POST").then(function (res) {
        console.log(res)
        if(res.code==0){
          if(that.data.status==1){
            wx.showModal({
              title: '成功',
              content: '已通过初审',
              success (res) {
                  wx.navigateTo({
                    url: '/pageA/pages/platformMana/contract/contract?contract_type='+that.data.contract_type,
                  })
              }
            })
          }else if(that.data.status==2){
            wx.showModal({
              title: '成功',
              content: '已通过终审',
              success (res) {
                  wx.navigateTo({
                    url: '/pageA/pages/platformMana/contract/contract?contract_type='+that.data.contract_type,
                  })
              }
            })
          }else if(that.data.status==3){
            wx.showModal({
              title: '驳回成功',
              content: '合同已经被驳回',
              success (res) {
                  wx.navigateTo({
                    url: '/pageA/pages/platformMana/contract/contract?contract_type='+that.data.contract_type,
                  })
              }
            })
          }
    
        }else{
          util.showErrorToast('请求异常')
        }
      })
    }
  },
  delect:function(e){
    var that=this
    var url=''
    if(that.data.contract_type=='酒店合同'){
      url=api.c_delete
    }else if(that.data.contract_type=='合伙人合同'){
      url=api.partner_delete
    }else if(that.data.contract_type=='供应商合同'){
      url=api.supplier_delete
    }
    util.request1(url, {
      id:that.data.id,
    },
       "POST").then(function (res) {
       if(res.code==0){
            wx.showModal({
              title: '删除成功',
              content: '此合同已经被删除',
              success (res) {
                  wx.navigateTo({
                    url: '/pageA/pages/platformMana/contract/contract',
                  })
              }
            })
        }else{
          util.showErrorToast('请求异常')
        }
          
      })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  reject_reason(e){
  this.setData({
    reject_reason:e.detail.value
  })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that=this
    that.setData({
      userLevelId:wx.getStorageSync('userLevelId')
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  
  
})