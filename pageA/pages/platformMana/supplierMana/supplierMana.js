// pages/platformMana/hotelManalist/hotelManalist.js
var api = require('../../../../config/api.js');
var util=require('../../../../utils/util')
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hotelManalist:[],
    pageSize:'',
    status:'',
    id:'',
    zhuntai:'',
    page:1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
   wx.request({
    url: app.globalData.url80 +"user/checkAccount",
    data: {
      userLevelId: 6,
      page: 1,
      pageSize:20
    },
    method: 'POST',
    header: {
      'content-type': 'application/x-www-form-urlencoded',
      'X-Dts-Token': wx.getStorageSync('token')
    },
    success: function (res) {
      console.log(res)
      that.setData({
        hotelManalist:res.data.data,
      })

    },
    fail:function(res){
      console.log(res)
    }
   })
    
  },
  userId:function(e){
    console.log(e)
    var that=this
    var id=e.currentTarget.dataset.id
    var status=e.currentTarget.dataset.status
    wx.request({
      url: app.globalData.url80 +"user/freezeAccount",
      data: {
       userId:id
      },
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'X-Dts-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        that.onLoad()
      },
      fail:function(res){
        console.log(res)
      }
     })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that=this
    wx.request({
     url: app.globalData.url80 +"user/checkAccount",
     data: {
       userLevelId: 6,
       page: 1,
       pageSize:20
     },
     method: 'POST',
     header: {
       'content-type': 'application/x-www-form-urlencoded',
       'X-Dts-Token': wx.getStorageSync('token')
     },
     success: function (res) {
       that.setData({
         hotelManalist:res.data.data
       })
       wx.stopPullDownRefresh()
     },
     fail:function(res){
       console.log(res)
     }
    })
  },


/**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    // console.log(that.data.hotelManalist)
    var page = that.data.page
    var hotelManalist = that.data.hotelManalist
    page++;
    that.setData({
    page: page
    })
    // 显示加载图标
    wx.showLoading({
    title: '玩命加载中',
    })
    wx.request({
      url: app.globalData.url80 +"user/checkAccount",
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Dts-Token': wx.getStorageSync('token')
      },
     data:{
      userLevelId: 6,
      page: page,
      pageSize:20
     },
    success: function (res) {
      // console.log(res)
      wx.hideLoading();
      var list=[]
      if(res.data.data.length>0){
        list =res.data.data
        if (list.length<=10) {
          wx.showToast({
          title: '暂无更多',
          icon: 'none',
          })
          var lists = hotelManalist.concat(list)     //  为一进入页面请求完数据定义的集合
          that.setData({
            hotelManalist : lists
          });
          }else{
            var lists = hotelManalist.concat(list)     //  为一进入页面请求完数据定义的集合
            that.setData({
              hotelManalist : lists
            });
          }
      }else if(res.data.data.length==0){
            return;
      }
      
 
    },
  })
 },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  addsupplier:function(){
    wx.navigateTo({
      url: '/pageA/pages/platformMana/hotelMana/hotelMana?type='+'2',
    })
  }
})