const QQMapWX = require('../../../../lib/qqmap-wx-jssdk.min.js');
// 实例化API核心类
var qqmapsdk = new QQMapWX({
  key: 'GVOBZ-APK6P-BOZDT-V5ZWX-MRI7T-6GF5H' // 必填
});

var api = require('../../../../config/api.js');
var util=require('../../../../utils/util')
var app = getApp();
Page({
  data: {
    username: '',
    password: '',
    nickname:'',
    confirmPassword: '',
    contraNo:'',
    suppliercompany:'',
    code: '',
    loginErrorCount: 0,
    imgUrl:'https://dl.reg.163.com/cp?pd=yanxuan_web&pkid=SkeBZeG',
    userLevelId:'',
    type:'',
    level:0,
    Partner_list:[
      {id:0,value:'等级选择',type:0},//type=1,传行政区三级
      {id:2,value:'普通合伙人',type:1},//type=1,传行政区三级
      {id:3,value:'区县总代合伙人',type:1},
      {id:4,value:'城市运营中心',type:2},//type=2,传行政区二级
      {id:5,value:'城市合伙人（分红)',type:2},
    ],
    Partner_listIndex:0,
    Array: [
      {
        id: 0,
        name: '等级选择'
      },
      {id:2,name:'普通合伙人',type:1},//type=1,传行政区三级
      {id:3,name:'区县总代合伙人',type:1},
      {id:4,name:'城市运营中心',type:2},//type=2,传行政区二级
      {id:5,name:'城市合伙人（分红)',type:2},
    ],
    partnerLevel:'',
    type1:'',
    index: 0,
    company: [
    ],
    companyNamechage:'',
    companyNameindex: 0,
    companyID:'',
    imggg:'',
    smusername:'',
    municipality:'',
    citylist: [], //获取所有的城市列表
    province: [],
    Partner_area:0,
    P_code:'',
    P_code1:'',
    P_code2:'',
    partnerArea1:'',
    partnerArea2:'',
    partnerArea3:'',
    cityname: [],
    cityname1:[],
    cityname_area:0,
    county: [],
    county1:[],
    county_area:0,
    franchise:'',//加盟金
    C_password:0,
    C_password1:0
  },
  bindPickerChange: function(e) {
    var that=this
    var index=e.detail.value
    var ty=that.data.Array[index].type
    var partnerLevel=that.data.Array[index].id
    that.setData({
      type1:ty,
      Partner_listIndex:e.detail.value,
      partnerLevel:partnerLevel
    })

  },
  bindcompanyChange: function(e) {
    var index=e.detail.value
    var company=this.data.company[index].id
    this.setData({
      companyNameindex: e.detail.value,
      companyID:company,
      companyNamechage:1
    })
  },
  onLoad: function (options) {
    console.log(options)
    // 页面初始化 options为页面跳转所带来的参数
    // 页面渲染完成
    var that=this
    that.setData({
      type:options.type,
      contraNo:options.ContraNo,
      // suppliercompany:options.suppliercompany,
      // hotelname:options.hotelname
    })
    if(options.type==0){//酒店
      that.setData({
        level:7,
        nickname:options.hotelname,
        username:options.hotelcontactphone
      })
    }else if(options.type==2){ //供应商
      that.setData({
        level:6,
        nickname:options.suppliercompany,
        username:options.supplierPhone
      })
    }
           //调用获取城市列表接口
           qqmapsdk.getCityList({
            success: function (res) { //成功后的回调
              // console.log(res.result)
              that.setData({
                citylist: res.result,
                province: res.result[0],
                cityname: res.result[1],
                county: res.result[2],
              });
            },
            fail: function (error) {
              console.error(error);
            },
          });

  },
  onReady: function () {

  },
  onShow: function () {
    // 页面显示
    var that=this
    util.request(api.companyList, null).then(function (res) {
      if(res.errno==0){
        that.setData({
          company:res.data.companyList
        })
      }else{
        util.showErrorToast('服务器错误')
      }
      
    })
 
  },
  onHide: function () {
    // 页面隐藏

  },
  onUnload: function () {
    // 页面关闭

  },
  startRegister: function () {
    // console.log('123456')
    var that = this;
    if (that.data.password.length < 3 || that.data.username.length < 3) {
      wx.showModal({
        title: '错误信息',
        content: '用户名和密码不得少于3位',
        showCancel: false
      });
      return false;
    }
    if (that.data.password != that.data.confirmPassword) {
      wx.showModal({
        title: '错误信息',
        content: '确认密码不一致',
        showCancel: false
      });
      return false;
    }
    var data={}
    var url=''
    if(that.data.level==7){
      url="api/user/register",
      data={
        nickName:that.data.nickname,
        username: that.data.username,
        password: that.data.password,
        level:that.data.level,
        contraNo:that.data.contraNo
      }
    }else if(that.data.level==6){
      url="api/user/supplier/reg",
      data={
        nickName:that.data.nickname,
        username: that.data.username,
        password: that.data.password,
        level:that.data.level,
        contraNo:that.data.contraNo,
        companyId:that.data.companyID
      }
    }
    wx.request({
      // url: api.Register1,
      url: app.globalData.url83 +url,
      data: data,
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function (res) {
        if (res.data.code == 0) {
          wx.showModal({
            title: '注册成功',
            content: '点击确定返回上一页,取消继续注册',
            success (res) {
            if (res.confirm) {
              wx.switchTab({
                url: '/pages/dts_index/index',
              })
            } else if (res.cancel) {
              that.setData({
                nickname:'',
                username:'',
                password: '',
                confirmPassword:'',
                contraNo:''
              })
            }
          }
        }); 
        }else if(res.data.code==401){
          wx.showToast({
            title: '您的合同已经绑定账户请登入',
            icon:'none'
          })
        }else if(res.data.errno==404){
          util.showErrorToast(res.data.errmsg)
        }else if(res.data.code==405){
          util.showErrorToast(res.data.errmsg)
        }
        if (res.data.code == 200) {
          that.setData({
            'loginErrorCount': 0
          });
          wx.setStorage({
            key: "token",
            data: res.data.data.token,
            success: function () {
              wx.switchTab({
                url: '/pages/dts_index/index'
              });
            }
          });
      }
      },
      fail:function(res){
        console.log(res)
      }
    });
  },
  startRegister1: function () {
      var that=this
      var P_code=''
      var P_code1=this.data.P_code
      var P_code2=this.data.P_code1
      var P_code3=this.data.P_code2
      if(P_code3!=''){
        P_code=P_code3
        // return;
      }else if(P_code3==''&&P_code2!=''){
        P_code=P_code2
        // return;
      }else if(P_code3==''&&P_code2==''&&P_code1!=''){
        P_code=P_code1
      }else if(P_code3==''&&P_code2==''&&P_code1==''){
        util.showErrorToast('请选择加盟地区')
        return;
      }
      var address=that.data.partnerArea1+that.data.partnerArea2+that.data.partnerArea3
      console.log(P_code)
      wx.request({
        // url: api.Register1,
        url: app.globalData.url80 +'user/modificationLevel',
        data: {
          username:that.data.smusername,
          // username:'jc-8otimym103gr',
          level:that.data.partnerLevel,
          area:P_code,
          address:address,
          amount:that.data.franchise
        },
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded',
          'X-Dts-Token': wx.getStorageSync('token')
        },
        success: function (res) {
          console.log(res)
          if (res.data.errno == 0) {
            wx.showModal({
              title: '等级修改成功',
              success (res) {
              if (res.confirm) {
                wx.switchTab({
                  url: '/pages/dts_index/index',
                })
              } else if (res.cancel) {
              console.log('用户点击取消')
              that.onShow();
              }
            }      
            });
          }else if(res.statusCode!=200){
            util.showErrorToast('服务器请求异常')
          }
        },
        fail:function(res){
          console.log(res)
        }
      });
    },
    bindFranchiseInput(e){
      this.setData({
        franchise: e.detail.value
      });
    },
  bindNicknameInput: function (e) {
    console.log(e)
    this.setData({
      nickname: e.detail.value
    });
  },
  bindUsernameInput: function (e) {
    this.setData({
      username: e.detail.value,
      smusername: e.detail.value
    });
  },
  bindPasswordInput: function (e) {
    this.setData({
      password: e.detail.value
    });
  },
  bindConfirmPasswordInput: function (e) {
    this.setData({
      confirmPassword: e.detail.value
    });
  },
  bindCodeInput: function (e) {
    this.setData({
      code: e.detail.value
    });
  },
  bindcontraNoInput:function(e){
    this.setData({
      contraNo: e.detail.value
    });
  },
  clearInput: function (e) {
    console.log(e)
    switch (e.currentTarget.id) {
      case 'clear-nickname':
        this.setData({
          nickname: ''
        });
        break;
      case 'clear-username':
        this.setData({
          username: ''
        });
        break;
      case 'clear-password':
        this.setData({
          password: ''
        });
        break;
      case 'clear-franchise':
        this.setData({
          franchise: ''
        });
        break;
      case 'clear-confirm-password':
        this.setData({
          confirmPassword: ''
        });
        break;
      case 'clear-contraNo':
          this.setData({
            contraNo: ''
          });
        break;   
      case 'clear-code':
        this.setData({
          code: ''
        });
        break;
    }
  },
  refreshImg:function(e){
    var that = this;
    console.log(that.data.imgUrl)
    that.setData({
      imgUrl: that.data.imgUrl + '&random=' + Math.random() / 9999
    })
    console.log(that.data.imgUrl)
  },
  uptouxiang:function(){
    wx.chooseImage({
      success (res) {
        const tempFilePaths = res.tempFilePaths
        wx.uploadFile({
          url: 'https://example.weixin.qq.com/upload', //仅为示例，非真实的接口地址
          filePath: tempFilePaths[0],
          name: 'file',
          formData: {
            'user': 'test'
          },
          success (res){
            const data = res.data
            //do something
          }
        })
      }
    })
  },
  chooseImage(e) {
    var that=this
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success (res) {
        console.log(res)
        that.setData({
          imggg:res.tempFilePaths[0]
        })
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths
      }
    })
  },
  smusername:function(){
      // 允许从相机和相册扫码
      var that=this
    wx.scanCode({
      success (res) {
        console.log(res)
        that.setData({
          smusername:res.result,
          username:res.result
        })
      }
    })

    // 只允许从相机扫码
    // wx.scanCode({
    //   onlyFromCamera: true,
    //   success (res) {
    //     console.log(res)
        
    //   }
    // })
  },
  Partner_area:function(e){
    var that=this
    var index=e.detail.value
    var P_code=this.data.province[index].id
    var cityname=this.data.cityname
    var municipality=this.data.province[index].fullname
    var partnerArea=this.data.province[index].fullname
    var b=[]
    var list=[]
 
    for(var i=0;i<cityname.length;i++){
      var a= cityname[i]
      b= String(a.id).substring(0,2);
      var bianhao=b+'0000'
      if( b+'0000' == P_code){
        // console.log(i)
        list.push(cityname[i])
      }
    }
    if(that.data.type1==2){
    if(municipality=="北京市"||municipality=="天津市"||municipality=="上海市"||municipality=="重庆市"){
        that.setData({
          municipality:'zxs'
        })
      }
    }
    console.log(municipality)
    if(list!=[]){
      that.setData({
        Partner_area:index,
        P_code: P_code,
        P_code1:'',
        partnerArea1:partnerArea,
        cityname1:list
      })
    }else{
      
    }
  },
  cityname_area:function(e){
    var that=this
    var index=e.detail.value
    var P_code=this.data.cityname1[index].id
    var county=this.data.county
    var partnerArea=this.data.cityname1[index].fullname
    var b=[]
    var list=[]
    for(var i=0;i<county.length;i++){
      var a= county[i]
      b= String(a.id).substring(0,4);
      if( b+'00' == P_code){
        // console.log(i)
        list.push(county[i])
      }
    }
    this.setData({
      cityname_area:index,
      P_code1: P_code,
      partnerArea2:partnerArea,
      county1:list
    })
  },
  county_area:function(e){
    var that=this
    var index=e.detail.value
    var P_code=this.data.county1[index].id
    var partnerArea=this.data.county1[index].fullname
    this.setData({
      cityname_area:index,
      P_code2: P_code,
      partnerArea3:partnerArea
    })
  },
  C_password:function(e){
    // console.log(e)
    var type=e.currentTarget.dataset.password
    if(type==0){
      this.setData({
        C_password:1
      })
    }else if(type==1){
      this.setData({
        C_password:0
      })
    }
  
  },
  C_password1:function(e){
    // console.log(e)
    var type=e.currentTarget.dataset.password
    if(type==0){
      this.setData({
        C_password1:1
      })
    }else if(type==1){
      this.setData({
        C_password1:0
      })
    }
  
  }
})