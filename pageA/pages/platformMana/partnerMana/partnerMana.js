// pages/platformMana/hotelManalist/hotelManalist.js
var api = require('../../../../config/api.js');
var util=require('../../../../utils/util')
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hotelManalist:[],
    pageSize:'',
    status:'',
    id:'',
    zhuntai:'',
    page:1,
    userLevelId:2,
    dengji:'',
    objectArray: [
      {
        id: 2,
        name: '普通合伙人'
      },
      {
        id: 3,
        name: '区县合伙人'
      },
      {
        id: 5,
        name: '城市合伙人'
      },
    ],
    index: 0,
  },
  bindPickerChange: function(e) {
    var index= e.detail.value
    var list=this.data.objectArray;
    var userLevelId=list[index].id
    this.setData({
      index: index,
      userLevelId:userLevelId
    })
    this.qingqiu();
  },
  qingqiu:function(){
    var that=this
    wx.request({
     url:api.selectByMap,
     data: {
       userLevelId: that.data.userLevelId,
       page: 1,
       pageSize:20
     },
     method: 'POST',
     header: {
       'content-type': 'application/x-www-form-urlencoded',
       'X-Dts-Token': wx.getStorageSync('token')
     },
     success: function (res) {
       console.log(res)
       that.setData({
         hotelManalist:res.data.data,
       })
 
     },
     fail:function(res){
       console.log(res)
     }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
   wx.request({
    url:api.selectByMap,
    data: {
      userLevelId: that.data.userLevelId,
      page: 1,
      pageSize:20
    },
    method: 'POST',
    header: {
      'content-type': 'application/x-www-form-urlencoded',
      'X-Dts-Token': wx.getStorageSync('token')
    },
    success: function (res) {
      console.log(res)
      that.setData({
        hotelManalist:res.data.data,
      })

    },
    fail:function(res){
      console.log(res)
    }
   })
    
  },
  userId:function(e){
    console.log(e)
    var that=this
    var id=e.currentTarget.dataset.id
    var status=e.currentTarget.dataset.status
    wx.request({
      url: app.globalData.url80 +"user/freezeAccount",
      data: {
       userId:id
      },
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded',
        'X-Dts-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        that.onLoad()
      },
      fail:function(res){
        console.log(res)
      }
     })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   var userLevelId= wx.getStorageSync('userLevelId')
   this.setData({
     dengji:userLevelId
   })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that=this
    wx.request({
      url:api.selectByMap,
     data: {
       userLevelId: that.data.userLevelId,
       page: 1,
       pageSize:20
     },
     method: 'POST',
     header: {
       'content-type': 'application/x-www-form-urlencoded',
       'X-Dts-Token': wx.getStorageSync('token')
     },
     success: function (res) {
      //  console.log(res)
      wx.showToast({
        title: '刷新成功',
        icon:'none'
      })
       that.setData({
         hotelManalist:res.data.data,
       })
       wx.stopPullDownRefresh();
     },
     fail:function(res){
       console.log(res)
     }
    })
  },

/**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    // console.log(that.data.hotelManalist)
    var page = that.data.page
    var hotelManalist = that.data.hotelManalist
    page++;
    that.setData({
    page: page
    })
    // 显示加载图标
    wx.showLoading({
    title: '玩命加载中',
    })
    wx.request({
      url:api.selectByMap,
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Dts-Token': wx.getStorageSync('token')
      },
     data:{
      userLevelId: that.data.userLevelId,
      page: page,
      pageSize:20
     },
    success: function (res) {
      console.log(res)
      wx.hideLoading();
      var list =res.data.data
    if (list == '') {
    wx.showToast({
    title: '暂无更多',
    icon: 'none',
    })
    } else {
    var lists = list.concat(hotelManalist)     //grade  为一进入页面请求完数据定义的集合
    that.setData({
      hotelManalist : lists
    });
    }
    },
  })
 },
 
 delete:function(e){
  console.log(e)
  let that=this
  let id=e.currentTarget.dataset.id
  wx.showModal({
    title: '提示',
    content: '确定要删除该合伙人等级',
    success (res) {
    if (res.confirm) {
      wx.showLoading()
    wx.request({
      url: api.Partnerdelete,
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded",
        'X-Dts-Token': wx.getStorageSync('token')
      },
     data:{
       id:id
     },
    success: function (res) {
     console.log(res)
     wx.hideLoading();
     if(res.data.errno==0){
     wx.showToast({
       title: '删除成功',
       icon:'none'
     })
     }else{
      wx.showToast({
        title: '删除失败',
        icon:'none'
      })
     }
    },
    fail:function(res){
     console.log(res)
     wx.showToast({
      title: '请求服务器失败',
      icon:'none'
    })
    }
    })
    } else if (res.cancel) {
    that.onLoad();
    }
  }
  })
  
 },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  addhotel:function(){
    wx.navigateTo({
      url: '/pageA/pages/platformMana/hotelMana/hotelMana?type='+'4',
    })
  },
  performance:function(res){
    wx.navigateTo({
      url: '/pageA/pages/platformMana/partnerMana/performance/performance',
    })
  }
})