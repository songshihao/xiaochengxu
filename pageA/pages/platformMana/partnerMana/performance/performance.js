// pages/platformMana/hotelManalist/hotelManalist.js
var api = require('../../../../../config/api.js');
var util=require('../../../../../utils/util')
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    pageSize:'',
    status:'',
    id:'',
    zhuntai:'',
    page:1,
    userLevelId:2,
    objectArray: [
      {
        id: 2,
        name: '普通合伙人'
      },
      {
        id: 3,
        name: '区县合伙人'
      },
      {
        id: 5,
        name: '城市合伙人'
      },
    ],
    index: 0,
  },
  bindPickerChange: function(e) {
    var index= e.detail.value
    var list=this.data.objectArray;
    var userLevelId=list[index].id
    this.setData({
      index: index,
      userLevelId:userLevelId
    })
    this.qingqiu();
  },
  qingqiu:function(){
    var that=this
      wx.request({
        url: api.statistics,
        data:{
          startTime:'',
          endTime:'',
          page:1,
          pageSize:20
        },
        method:'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded',
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
        success: function(res) {
          if(res.data.code==0){
            that.setData({
              list:res.data.resMap
            })
          }
 
     },
     fail:function(res){
       console.log(res)
     }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
    that.qingqiu()
   
  },
 
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that=this
    that.qingqiu();
    setTimeout(function(){
      wx.stopPullDownRefresh();
    },500)
   
  },

/**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    // console.log(that.data.hotelManalist)
    var page = that.data.page
    var alllist = that.data.list
    page++;
    that.setData({
    page: page
    })
    // 显示加载图标
    wx.showLoading({
    title: '玩命加载中',
    })
    wx.request({
      url: api.statistics,
        data:{
          startTime:'',
          endTime:'',
          page:page,
          pageSize:20
        },
        method:'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded',
          'X-Nideshop-Token': wx.getStorageSync('token')
        },
    success: function (res) {
      wx.hideLoading();
      if(res.data.code==0){
        var list =res.data.resMap
        if (list == '') {
          wx.showToast({
          title: '暂无更多',
          icon: 'none',
          })
          } else {
          var lists = list.concat(alllist)
          that.setData({
            list : lists
          });
          }
      }
    },
  })
 },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  addhotel:function(){
    wx.navigateTo({
      url: '/pageA/pages/platformMana/hotelMana/hotelMana?type='+'4',
    })
  }
})