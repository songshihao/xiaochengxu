var app = getApp();
var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');

Page({
 
  /**
   * 页面的初始数据
   */
  data: {
    from:'',
    latitude: '',
    longitude: '',
    hotel_information:{},
    msg:[
      {
        appearancePicUrl: "http://cdn.jiachang8.com/Fn5jAwwQVSgT8Oau0uZK5IEoEG8r",
        callout: {
          bgColor: "#fff",
          borderRadius: 2,
          color: "#FC7905",
          content: "¥438起",
          display: "ALWAYS",
          padding: 5,
          textAlign: "center"
        },
        hotelId: 148,
        hotelLabel: "商务出行,天天特价,平台推荐,会议酒店",
        iconPath: "http://cdn.jiachang8.com/nfr2xh3mxm00zlfmjfkf.png ",
        id: 0,
        latitude: 31.34631,
        longitude: 121.25579,
        name: "美豪丽致酒店(上海嘉定新城中心店)",
        price: 438,
        score: 5
      }
    ],
    mapWidth:'',
    mapHeight:'',
    color:false,
    markerId:'',
    startTime: '',
    endTime: '',
    from:'',
    next:false,
    screenHeight:''
  },
  toaddress:function(e){
    // console.log(e)
    var that=this
    var id =e.markerId
    var msg=that.data.msg
    var markerId=that.data.markerId
        msg[id].callout.bgColor="#FC7905"
        msg[id].callout.color="#ffffff"
        if(markerId!=''){
          msg[markerId].callout.bgColor="#ffffff"
          msg[markerId].callout.color="#FC7905"
        }
    that.setData({
      color:true,
      markerId:id,
      msg:msg
    })
  },
 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let that=this
    var sy = wx.getSystemInfoSync(),
        mapWidth = sy.windowWidth*2,
        mapHeight = sy.windowHeight*2;
        that.setData({
      mapWidth:mapWidth,
      mapHeight:mapHeight
    })
    wx.getStorage({
      key: 'location',
      success:function(res){
        that.setData({
          latitude: res.lat,
          longitude: res.lng,
          from:res.data.lat +','+res.data.lng
        })
        that.hotelmap();
      }
    })

    that.setData({
      screenHeight:wx.getStorageSync('screenHeight'),
      screenWidth:wx.getStorageSync('screenWidth')
    })
  },
  location:function(e){
    // console.log(e)
    var that=this
    that.setData({
      latitude:e.detail.latitude,
      longitude:e.detail.longitude,
      from:e.detail.latitude + ',' + e.detail.longitude
    })
    that.hotelmap();
  },
  hotelmap:function(){
    let that=this
    wx.request({
      url: api.hotelMap,
      method: "POST",
      header: {
        'content-type': "application/x-www-form-urlencoded"
      },
      data:{
        location:that.data.from
      },
       success:function(res){
        that.setData({
          msg:res.data.data
        })
        console.log(that.data.msg)
       },
       fail:function(res){
        console.log(res)
       }
    })
  },
  onShow:function(){
    var _this=this
    let getDate = wx.getStorageSync("ROOM_SOURCE_DATE");
    if(getDate){
      _this.setData({
        startTime: getDate.checkInDate,
        endTime: getDate.checkOutDate,
      })
    }else{
      wx.getStorage({
        key: 'timedata',
        success:function(res){
          var startTime_a=res.data[0].startTimeq
          var endTime_a =res.data[0].endTimeq
          var startTime_y = startTime_a.replace('年', '-');
          var startTime_m = startTime_y.replace('月', '-');
          var startTime1 = startTime_m.replace('日', '');
  
          var startTime_y = endTime_a.replace('年', '-');
          var startTime_m = startTime_y.replace('月', '-');
          var endTime1 = startTime_m.replace('日', '');
  
          _this.setData({
            startTime:startTime1,
            endTime:endTime1,
          })
        }
      })
    }
  },
  hotel:function(e){
    console.log(e)
    var hotelid=e.currentTarget.dataset.hotelid
    var hotelname=e.currentTarget.dataset.hotelname
    var startDate=this.data.startTime
    var endDate=this.data.endTime
    console.log(hotelname)
    if(hotelid!=''){
      wx.navigateTo({
        url: '/pages/ucenter/hotelxq/hotelxq?id=' + hotelid +'&startDate='+startDate +'&endDate='+endDate +'&hotelname='+ hotelname,
      })
    }
  },
  last:function(){
    var that=this
    var markerId=that.data.markerId
    markerId--;
    that.setData({
      markerId:markerId,
      next:false
    })
  },
  next:function(){
    var that=this
    var markerId=that.data.markerId
    var msg=that.data.msg
    markerId++;
    console.log(msg.length)
    var max=msg.length - 1
    if(markerId<=max){
      that.setData({
        markerId:markerId
      })
    }else{
      that.setData({
        next:true
      })
    }
  },
  clickcontrol(e) {
    console.log(e)
    let mpCtx = wx.createMapContext("myMap");
    mpCtx.moveToLocation();

    var that=this
      wx.getLocation({
        type: 'wgs84', //返回可以用于wx.openLocation的经纬度
        success: function (res) { //因为这里得到的是你当前位置的经纬度
          console.log(res)
          // var latitude = res.latitude
          // var longitude = res.longitude
          that.setData({
            latitude : res.latitude,
            longitude : res.longitude,
            from:res.latitude + ',' +res.longitude
          })
          that.hotelmap();
          // wx.openLocation({ //所以这里会显示你当前的位置
          //   latitude: latitude1,
          //   longitude: longitude1,
          // })
        }
      })
    },
  regionchange(e) {
      console.log(e)
      var that=this
      that.setData({
        latitude:e.detail.latitude,
        longitude:e.detail.longitude,
        from:e.detail.latitude + ',' + e.detail.longitude
      })
      wx.request({
        url: api.hotelMap,
        method: "POST",
        header: {
          'content-type': "application/x-www-form-urlencoded"
        },
        data:{
          location:that.data.from
        },
         success:function(res){
           console.log(res)
         if(res.data.data){
          var mas=res.data.data
         }else{
            var mas=[]
          }
          var item=[{
            iconPath: "http://cdn.jiachang8.com/woswqqp18f3rek5p8cy2.png",
            latitude: e.detail.latitude,
            longitude: e.detail.longitude,
            width:30,
            height:30
          }]

          var msglist=mas.concat(item)
          console.log(msglist)
            that.setData({
              msg:msglist
            })
         },
         fail:function(res){
          console.log(res)
         }
      })
},
})