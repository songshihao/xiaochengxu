var util = require('./utils/util.js');
var api = require('./config/api.js');
var user = require('./utils/user.js');

  
App({
  
  globalData: {
    hasLogin: false,
    userInfo: {
      nickName: 'Hi,游客',
      userName: '点击去登录',
      avatarUrl: 'https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/20180727/150547696d798c.png'
  },
  token: '',
  userCoupon: 'NO_USE_COUPON',//默认不适用优惠券
  courseCouponCode: {},//购买课程的时候优惠券信息
  // url83:'http://192.168.0.243:8383/reservation/',
  // url80:'http://192.168.0.243:6080/demo/wx/',
  // url83:'http://192.168.0.103:8383/reservation/',
  // url80:'http://192.168.0.103:6080/demo/wx/',
  // url83:'http://192.168.0.57:8383/reservation/',
  // url80:'http://192.168.0.57:6080/demo/wx/',
  
  url83:'https://jczh.jiachang8.com/reservation/',
  url80:'https://jczh.jiachang8.com/jczh/wx/wx/',
  goTop:'https://www.jiachang8.com/mpimage/images/goTop.png?x=1',
  statusBarHeight:wx.getStorageSync('statusBarHeight'),
  titleBarHeight :wx.getStorageSync('titleBarHeight'),
    //发票信息 （新建的
  xinfapiao: {
      fapiaotaitou: "",
      anshuirenshibiehao: "",
      fapiaoneirong: "",
      kaihuhang: "",
      zhanghao: "",
      dizhi: "",
      dianhua: "",
      cljDs: '',//长连接定时
  },
  statusBarHeight:0,
  toBar:44,
  bbh:1,
  globalData_token:'',
  hasUpdate:''
  },
  onLaunch: function() {
    let _that=this
    const updateManager = wx.getUpdateManager();
    wx.getUpdateManager().onUpdateReady(function() {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success: function(res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })
    this.autoUpdate()
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
    wx.getSystemInfo({
        success (res) {
          console.log(res)
          wx.setStorageSync('screenHeight', res.screenHeight);
          wx.setStorageSync('screenWidth', res.screenWidth)
          _that.globalData.statusBarHeight=res.statusBarHeight;
          if(res.platform=="ios"){
             _that.globalData.toBar=44;
          }else if(res.platform=="android"){
             _that.globalData.toBar=48;
          }else{
             _that.globalData.toBar=44;
          }
        }
    })
    var hdata = wx.getMenuButtonBoundingClientRect();
    wx.setStorageSync('hdatahieht', hdata.height);
    wx.setStorageSync('hdatatop', hdata.top)
  },
  onShow: function(options) {
    user.checkLogin().then(res => {
      this.globalData.hasLogin = true;
    }).catch(() => {
      this.globalData.hasLogin = false;
    });
  },
   autoUpdate: function () {
    var self = this
    // 获取小程序更新机制兼容
    const version = wx.getSystemInfoSync().SDKVersion
    console.log('版本号：' + version)
    if (wx.canIUse('getUpdateManager')) {
      const updateManager = wx.getUpdateManager()
      //1. 检查小程序是否有新版本发布
      updateManager.onCheckForUpdate(function (res) {
        // 请求完新版本信息的回调
        if (res.hasUpdate) {
          //2. 小程序有新版本，则静默下载新版本，做好更新准备
          self.globalData.hasUpdate=1;

          console.log( self.globalData.hasUpdate)
          updateManager.onUpdateReady(function () {
            console.log(new Date())
            wx.showModal({
              title: '更新提示',
              content: '新版本已经准备好，是否重启应用？',
              success: function (res) {
                if (res.confirm) {
                  //3. 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                  updateManager.applyUpdate()
                } else if (res.cancel) {
                  //如果需要强制更新，则给出二次弹窗，如果不需要，则这里的代码都可以删掉了
                  wx.showModal({
                    title: '温馨提示~',
                    content: '本次版本更新涉及到新的功能添加，旧版本无法正常访问的哦~',
                    success: function (res) {
                      self.autoUpdate()
                      return;
                      //第二次提示后，强制更新                      
                      if (res.confirm) {
                        // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                        updateManager.applyUpdate()
                      } else if (res.cancel) {
                        //重新回到版本更新提示
                        self.autoUpdate()
                      }
                    }
                  })
                }
              }
            })
          })
          updateManager.onUpdateFailed(function () {
            // 新的版本下载失败
            wx.showModal({
              title: '已经有新版本了哟~',
              content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~',
            })
          })
        }
      })
    } 
    // else {
    //   wx.showModal({
    //     title: '提示',
    //     content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
    //   })
    // }
  },
})